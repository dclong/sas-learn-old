
PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
	 title f=Arial c=red h=4 "Discriminaiton using variable X2";
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2 X7;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
	 title f=Arial c=red h=4 "Discrimination analysis using variable X2 and X7";
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2 X7 X4;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
	 title f=arial c=red h=4 "Discrimination analysis using variables x2, x4 and x7";
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2 X7 X4 X8;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2 X7 X4 X8 X3;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2 X7 X4 X8 X3 X6;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
run;

PROC DISCRIM DATA=loans POOL =Yes OUTSTAT=outdat.rule1;
     CLASS type;
     VAR x2 x7;
     PRIORS "GOOD"=0.8 "BAD"=0.2;
     FORMAT TYPE T.;
run;

proc discrim data=outdat.rule1 testdata=set2 testlist;
     class type;
     var x2 x7;
     testclass type;
     FORMAT TYPE T.;
run;

/* Test for homogeneous covariance matrices.
   In this case the program decides to use
   quadratic discriminant analysis        */

PROC DISCRIM DATA=roos POOL =Yes OUTSTAT=outdat.roos1;
  	CLASS type;
  	VAR x1 x3 x7 x9 x14 x15;
  	format type troo.;
run;

proc discrim data=outdat.roos1 testdata=oldroos testlist;
  	class type;
  	var x1 x3 x7 x9 x14 x15;
  	testclass type;
  	FORMAT TYPE troo.;
run;

PROC DISCRIM DATA=LOANS POOL=TEST ANOVA CROSSLIST MANOVA METHOD=NORMAL POSTERR
               SIMPLE SLPOOL=.01 OUTSTAT=DISCRIM1;
     CLASS TYPE;
     VAR X1-X8;
     ID ID;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
	 title f="Arial" c=red h=4 "Discrimination Analysis";
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE METHOD=NORMAL;
     CLASS TYPE;
     VAR X1-X8;
     ID ID;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
	 title f="Arial" c=red h=4 "Discrimination Analysis Using Linear Rule";
run;

PROC DISCRIM DATA=grain POOL=test CROSSVALIDATE METHOD=NORMAL outstat=model2;
     CLASS TYPE;
     VAR X1 x3 x4 x7 x10;
     FORMAT TYPE T.;
	 title f=arial c=red h=4 "Discrimination analysis using slected variables";
run;

PROC DISCRIM DATA=grain POOL=test CROSSVALIDATE METHOD=NORMAL outstat=model2;
     CLASS TYPE;
     VAR X1 x3 x4 x6 x7 x10;
     FORMAT TYPE T.;
	 title f=arial c=red h=4 "Discrimination analysis using slected variables";
run;

PROC DISCRIM DATA=grain POOL=yes CROSSVALIDATE METHOD=NORMAL outstat=model2;
     CLASS TYPE;
     VAR X1 x3 x4 x7 x10;
     FORMAT TYPE T.;
	 title f=arial c=red h=4 "Discrimination analysis using slected variables";
run;

proc discrim data=owls pool=test wcorr pcorr;
  	class type;
  	var km91 km118 km140 km160 km177 km241 km338 ;
run;

proc discrim data=grain pool=test anova crosslist manova method=normal 
              posterr simple slpool=0.01 outstat=dis1;
	 class type;
	 var x1-x11;
	 format type t.;
run;

PROC DISCRIM DATA=grain POOL=YES CROSSVALIDATE METHOD=NORMAL outstat=model1;
     CLASS TYPE;
     VAR X1-X11;
     FORMAT TYPE T.;
run;

PROC DISCRIM DATA=set1 POOL=test SLPOOL=.01 
     WCOV PCOV;  
     CLASS group;  
     VAR X1-X4;
run;

PROC DISCRIM DATA=set1 POOL=TEST SLPOOL=.001 WCOV PCOV CROSSLIST
                WCORR PCORR Manova testdata=set2 testlist;
     CLASS Admit;
     FORMAT TYPE ADMITFMT.;
     VAR GPA GMAT;
run;

PROC DISCRIM DATA=SET1 POOL=TEST SLPOOL=.01 
     WCOV PCOV;
     CLASS TEMP;
     VAR X1-X2;
run;
