
PROC UNIVARIATE DATA=bnotes NORMAL;
     VAR X1-X6;  
run;

PROC UNIVARIATE DATA=set1 PLOT NORMAL; 
     BY Admit;
     VAR GPA GMAT;
run;


PROC UNIVARIATE DATA=set1 NORMAL;
     BY group;
     VAR x1 x2 x3 x4;
run;

PROC UNIVARIATE DATA=set1 NORMAL;
     VAR X1-X4;
RUN;

proc univariate data=grain normal; 
     by type;
	 var x1-x11;
	 title f=arial c=red h=4 "Test for normality";
run;

PROC UNIVARIATE DATA=LOANS NORMAL;
     BY TYPE;
     VAR X1-X8;
     FORMAT TYPE T.;
RUN;

proc univariate data=set2 normal;
     var residual;
run;

proc univariate data=set2;
     var residual;
	 qqplot;
run;

proc univariate data=set2;
     probplot residual/normal(mu=0 sigma=1 color=red);
run;

proc univariate data=set1 normal;
	by agent;
  	var time;
run;

proc univariate plot normal;
  	var resid;
  	qqplot / normal;
  	title 'Normal probability plot of residuals';
run;
/* The second method, using a WHERE command to extract the subset */
/*   'on the fly' */

proc univariate data = tomato;
  	where group =  1;
  	var yield;
  	title 'Tomato yield summary for observations in group 1';
run;

proc univariate;
  	by group;
  	histogram yield;
  	title 'High resolution histograms for each group';
run;

proc univariate;
  	by group;
  	histogram yield;
  	title 'High resolution histograms for each group';
run;
