

proc corresp data=set1 out=results cp rp dimens=2 profile=both short;
  	var c1-c15;
  	id work;
run;


proc corresp data=france out=results cp rp dimens=3 profile=both short  ;
     var      varw free huma sche sala secu comp inte near atmo soci auto like
           othe none outd noa node grad smal medi larg;
     supplementary node grad smal medi larg;
     id  work;
run;

proc corresp data=set1 out=results cp rp dimens=3 profile=both short  ;
     var well mild moderate impaired;
     id parents;
run;
