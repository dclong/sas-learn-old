
PROC FORMAT;
     VALUE ADMITFMT 1 = 'Admit'
                    2 = 'Do Not Admit'
                    3 = 'Borderline';
run;

proc format; 
	value length  1='1 min'
                  2='2 min'
			      3='3 min'
			      4='5 min';
run;

proc format;
	value run 1 = 'Early'
           	  2 = 'Late';
  	value sex 1 = 'Female'
              2 = 'Male';
  	value gear 1 = 'Hook'
               2 = 'Net';
run;
