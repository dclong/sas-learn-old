
proc insight data=set1;
 	scatter x1 x2 x3*x1 x2 x3;
run;

proc standard data=set1 out=set2 mean=0 std=1 ;
   	var x1-x5;
run;

title ;

proc autoreg;
  	model y =  x /dw = 1 dwprob;
run;
/* fit four loess smooths, with 4 diff amounts of smoothing */   
proc loess;
  	model y =  year /smooth = (0.3 0.6 1 2) dfmethod =exact ;  
  	ods output outputstatistics = fit;
  	title 'Loess nonparametric regression';
run;

