
PROC RANK DATA=bnotes NORMAL=BLOM OUT=bnotes;
     VAR X1-X6; 
     RANKS Q1-Q6;
run;

PROC RANK DATA=set1 NORMAL=BLOM OUT=set1;
     VAR X1-X4; 
     RANKS Q1-Q4;
RUN;

proc rank data=set2 normal=blom out=set2;
      var residual; 
      ranks q;
run;
