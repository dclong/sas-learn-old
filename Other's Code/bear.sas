data bear;
     
  infile 'bear.txt';
  input   age    length    sex $ weight chest headlen headwid month neck;

  if sex =  '1' then nsex =  0;
   else nsex = 1;
  
  lage = log(age);
  llength = log(length);
  lweight = log(weight);
  lchest = log(chest);
  lheadlen = log(headlen);
  lheadwid = log(headwid);
  lneck = log(neck);
  
proc reg aic sbc cp mse;
  model lweight = lage llength nsex lchest lheadlen lheadwid lneck
    / selection = cp best = 5 b;
  model lweight = lage llength nsex lchest lheadlen lheadwid lneck
    / selection = stepwise slstay = 0.20 slentry = 0.20;
    
  
run;
     