options formdlim = '-';

data wool;
     
  infile 'wool.txt' missover;
  
  input bale @;
  do i =  1 to 4;
    input clean @;
    if clean ne . then output;
    end;
    
    
proc varcomp method = type1;
  class bale;
  model clean = bale ;
  title 'Method of moments estimates, all data ';

proc varcomp method = ml;
  class bale;
  model clean = bale ;
  title 'ML estimates, all data ';
  
proc plot;
  plot clean*bale;
             
proc mixed data = wool;
  where clean < 62;
   
  class bale;
  model clean = ;
  random bale;
  title 'REML estimates from proc mixed, without outlier';
          

  
  