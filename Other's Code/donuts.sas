
/* This code is posted as donuts.sas in the SAS 
   folder on the course web page  */ 

options nocenter nodate nonumber linesize=64;


/*  Enter the data for the doughnut experiment on pages 218-219
    in Snedecor and Cochran.  In this case each observation is
    on a separate line   */

data set1;
  infile 'c:\st500\data\donuts.dat';
  input oil y;
  label y = (oil absorbed -150) grams;
  run;


proc print data=set1; 
  title " ";  run;

/*  Run the MEANS procedure in SAS to compute sample
    means and variances for each fat  */

proc sort data=set1; by oil;

proc means data=set1 n mean std var stderr min max;
  by oil;
  var y;
  run;


/*  Run the GLM procedure in SAS to compute the ANOVA table
    and compare pairs of means.  In special cases like this
    where each treatment group has the same number of observations,
    these computations could also be done with the ANOVA
    procedure.  You can also do the computaions with the
    INSIGHT procedure  */

proc glm data=set1;
  class oil;
  model y = oil / p;
  estimate 'o4-(o1+o2+o3)/3' oil -1 -1 -1 3 / divisor=3;
  estimate 'o2-(o1+o3)/2' oil -0.5 1 -0.5 0;
  estimate 'o1-o3' oil 1 0 -1 0 ;
  contrast 'o4-(o1+o2+o3)/3' oil -1 -1 -1 3 ;
  contrast 'o2-(o1+o3)/2' oil -0.5 1 -0.5 0;
  contrast 'o1-o3' oil 1 0 -1 0 ;
  means oil /alpha=.05 bon lsd scheffe tukey snk;
  output out=set2 residual=r predicted=yhat;
  run;

/*  Compute diagnostic checks of the model using residuals */

proc univariate data=set2 normal plot;
  var r;

proc rank data=set2 normal=blom out=set2;
  var r;
  ranks q;

proc plot data=set2;
  plot r*(q yhat)='*' / hpos=60;
   run;


/*  Make a normal probability plot with better resolution
    using SASgraph                                        */


goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss h=2.3)
      ORDER = -3 to 3 by 1
      value=(f=triplex h=2.3)
      length= 5.5 in;

  axis2 label=(f=swiss h=2.3 a=90 r=0)
      order = -20 to 25 by 5
      value=(f=swiss h=2.3)
      length = 5.0 in;

  axis3 label=(f=swiss h=2.3)
      order = 0 to 5 by 1
      value=(f=swiss h=2.3)
      length = 5 in;

  axis4 label=(f=swiss h=2.3 a=90 r=0)
      order = -10 to 50 by 10
      value=(f=swiss h=2.3)
      length = 4.5 in;

  SYMBOL1 V=CIRCLE H=2.5 ;

 PROC GPLOT DATA=SET2;
   PLOT y*oil /  vaxis=axis4 haxis=axis3;
   TITLE1  H=3.0 F=swiss "Fat Absorbtion";
   TITLE2  H=2.2 F=swiss "Doughnut Cooking Oils";
       LABEL  y = 'Fat Absorption-150g';
	   LABEL  oil = 'Type of Cooking Oil';
   RUN;


  PROC GPLOT DATA=SET2;
   PLOT r*q / vaxis=axis2 haxis=axis1;
   TITLE1  H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
   TITLE2  H=2.2 F=swiss "Residuals Analysis for Doughnut Cooking Oils";
       LABEL q='Standard Normal Expectations';
       LABEL  r = 'Ordered Residuals';
   RUN;

   PROC GPLOT DATA=SET2;
   PLOT r*oil / vaxis=axis2 haxis=axis3;
   TITLE1  H=3.0 F=swiss "Residual Plot";
   TITLE2  H=2.2 F=swiss "Analysis of Doughnut Cooking Oils";
       LABEL  r = 'Residuals';
	   LABEL  oil = 'Type of Cooking Oil';
   RUN;


   proc npar1way data=set1 wilcoxon;
     class oil; var y;
	 exact wilcoxon / mc n=10000;
	 run;
