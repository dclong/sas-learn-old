
/*  A SAS progrsm for analyzing a Latin Square
    This program is posted in the file

                  latinsq.sas
*/

data set1;
  input driver car additive $ mpg;
  cards;
1 1 A 21
1 2 B 26
1 3 D 20
1 4 C 25
2 1 D 23
2 2 C 26
2 3 A 20
2 4 B 27
3 1 B 15
3 2 D 13
3 3 C 16
3 4 A 16
4 1 C 17
4 2 A 15
4 3 B 20
4 4 D 20
run;


/* Compute an ANOVA table where cars and
   drivers are treated as fixed effects */

proc glm data =set1;
  class car driver additive;
  model mpg = driver car additive / p;
  lsmeans additive / stderr tdiff pdiff;
  means additive / tukey;
  lsmeans driver / stderr tdiff pdiff;
  means driver / tukey;
  run;


/* Compute an ANOVA table where cars
   are treated as random effects */

proc glm data =set1;
  class car driver additive;
  model mpg = driver car additive / p;
  random  car / q test;
  lsmeans additive / stderr tdiff pdiff;
  means additive / tukey;
  lsmeans driver / tdiff pdiff;
  means driver / tukey;
  run;


/* Compute an ANOVA table where cars and
   drivers are treated as random effects */

proc glm data =set1;
  class car driver additive;
  model mpg = driver car additive / p;
  random  car driver / q test;
  run;
