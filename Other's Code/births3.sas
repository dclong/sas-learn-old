options noovp;      
/* enables overlay plots on vincent.  not necessary on pc's */
     
data births;
  infile 'birth.txt';
  input year denmark netherland canada usa;

  y =  netherland;
  x =  year - 1949;
  
/* calculate Durbin-Watson statistic in proc reg */
/*  by adding the option dw to the model statement */
/*  the dwprob option (not in the lab notes) gives the p-value */
/*    in addition to the DW statistic */

proc reg;
  model y =  x /dwprob;
  title 'regression with Durbin-Watson statistic';
           
/* fit a regression model using an AR(1) error structure */
/*   using proc mixed */
/* there are other procedures that do this (e.g. autoreg */
/*   in the Econometrics / Time Series collection of procs) */
/* you are welcome to use autoreg if you are familiar with it */
/* we will use mixed for other models in the next few weeks */
/* so this is an introduction to it */

proc autoreg;
  model y =  x /dw = 1 dwprob;
run;
          

proc mixed;
  model y =  x /solution ddfm = kr;
  repeated /subject = intercept type = ar(1);
  title 'regression with ar(1) error ';
run;
         
