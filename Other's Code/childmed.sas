
/*  SAS code to analyze data from a 
    two-way factorial experiment */

data set1;
input dosage admin y;
datalines;
0.5 1 0.414
0.5 1 0.312
0.5 2 0.537
0.5 2 0.451
0.5 3 0.572
0.5 3 0.554
1.0 1 0.541
1.0 1 0.423 
1.0 2 0.513
1.0 2 0.580
1.0 3 0.622
1.0 3 0.597
1.5 1 0.592
1.5 1 0.575
1.5 2 0.595
1.5 2 0.573
1.5 3 0.613
1.5 3 0.650
2.0 1 0.672
2.0 1 0.610 
2.0 2 0.709
2.0 2 0.623
2.0 3 0.695
2.0 3 0.751
run;

/* Construct a profile plot */


 goptions cback=white colors=(black)
    targetdevice=ps rotate=landscape;

proc sort data=set1; by dosage admin;
  run;

proc means data=set1 noprint; 
  by dosage admin;
  var y;
  output out=means mean=my;
  run;

axis1 label=(f=swiss h=2.5)  ORDER = 0.5 to 2.0 by 0.5
      offset=(2 cm  2 cm)
      value=(f=swiss h=2.0)  w=3.0  length= 6.5 in;

axis2 label=(f=swiss h=2.0)  order = 0.3 to 0.8 by 0.1
      value=(f=swiss h=2.0) w= 3.0  length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.0 w=3 l=1 i=join ;
       SYMBOL2 V=DIAMOND H=2.0 w=3 l=3 i=join ;
       SYMBOL3 V=square H=2.0 w=3 l=9 i=join ;

 PROC GPLOT DATA=means;
   PLOT my*dosage=admin / vaxis=axis2 haxis=axis1;
   TITLE1  H=3.0 F=swiss "Concentration in the Liver";
       LABEL my='Sample Mean';
       LABEL dosage = 'Dosage (mg)';
   RUN;


 proc glm data=set1;
  class admin dosage;
  model y = admin dosage admin*dosage /
             solution p clm alpha=.05;
  output out=setr r=resid p=yhat;
  lsmeans admin*dosage / stderr pdiff;
  means admin dosage / tukey;
  contrast 'd-lin' dosage -3 -1  1 3;
  contrast 'd-quad' dosage -1 1 1 -1;
  contrast 'd-cubic' dosage 1 -3 3 -1;
  contrast 'a1-a2' admin 1 -1 0;
  contrast '((a1+a2/2)-a3' admin .5 .5 -1;
  contrast '(a1-a2)*(d-lin)' admin*dosage
                      -3 -1 1 3 3 1 -1 -3 0 0 0 0;
  contrast '(a1-a2)*(d-quad)' admin*dosage
                      -1 1 1 -1 1 -1 -1 1 0 0 0 0;
  contrast '(a1-a2)*(d-cubic)' admin*dosage
                      1 -3 3 -1 -1 3 -3 1 0 0 0 0;
  contrast '(.5(a1+a2)-a3)*(d-lin)' admin*dosage
          -1.5 -0.5 0.5 1.5 -1.5 -0.5 0.5 1.5 
                      3 1 -1 -3;
  contrast '(.5(a1+a2)-a3)*(d-quad)' admin*dosage
          -.5 .5 .5 -.5 -.5 .5 .5 -.5 
                        1 -1 -1 1;
  contrast '(.5(a1+a2)-a3)*(d-cubic)' admin*dosage
          0.5 -1.5 1.5 -0.5  0.5 -1.5 1.5 -0.5 
                      -1 3 -3 1;
  estimate 'd-lin' dosage -3 -1  1 3;					  
  estimate 'd-quad' dosage -1 1 1 -1;
  estimate 'd-cubic' dosage 1 -3 3 -1;
  estimate 'a1-a2' admin 1 -1 0;
  estimate '((a1+a2/2)-a3' admin .5 .5 -1;
  estimate '(a1-a2)*(d-lin)' admin*dosage
              -3 -1 1 3 3 1 -1 -3 0 0 0 0;
  estimate '(a1-a2)*(d-quad)' admin*dosage
              -1 1 1 -1 1 -1 -1 1 0 0 0 0;
  estimate '(a1-a2)*(d-cubic)' admin*dosage
               1 -3 3 -1 -1 3 -3 1 0 0 0 0;
  estimate '(.5(a1+a2)-a3)*(d-lin)' admin*dosage
              -1.5 -0.5 0.5 1.5 -1.5 -0.5 0.5 1.5 
                      3 1 -1 -3;
  estimate '(.5(a1+a2)-a3)*(d-quad)' admin*dosage
                -.5 .5 .5 -.5 -.5 .5 .5 -.5 
                        1 -1 -1 1;
  estimate '(.5(a1+a2)-a3)*(d-cubic)' admin*dosage
               0.5 -1.5 1.5 -0.5  0.5 -1.5 1.5 -0.5 
                      -1 3 -3 1;
  run;
