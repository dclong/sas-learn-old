

/*  Analysis of creative writing data
    from the lecture notes  */

data set1;
  input trt y;
  datalines;
  1 12.0
  1 12.0
  1 12.9
  1 13.6
  1 16.6
  1 17.2
  1 17.5
  1 18.2
  1 19.1
  1 19.3
  1 19.8
  1 20.3
  1 20.5
  1 20.6
  1 21.3
  1 21.6
  1 22.1
  1 22.3
  1 22.6
  1 23.1
  1 24.0
  1 24.3
  1 26.7
  1 29.7
  2  5.0
  2  5.4
  2  6.1
  2 10.9
  2 11.8
  2 12.0
  2 12.3
  2 14.8
  2 15.0
  2 16.8
  2 17.2
  2 17.2
  2 17.4
  2 17.5
  2 18.5
  2 18.7
  2 18.7
  2 19.2
  2 19.5
  2 20.7
  2 21.2
  2 22.1
  2 24.0
  run;

options nocenter linesize=64;

/* Assign labels to the levels of the
   treatment variable  */
 
proc format; 
  value trt 1='intrinsic'
            2='extrinsic';


/* Sort the data file by the levels
   of the treatment variable  */

proc sort data=set1; by trt; run;


/* Compute summeary statistics for each 
   treatment group  */

proc univariate data=set1 normal;  by trt;  run;


/* Construct box plots.  The format statement
   assigns labels to the levels of the treatment 
   variable used to label the plot.  The insetgroup
   statement displays a box containing summary
   statistics at the top of the plot (this is optional) */

proc boxplot data=set1;
  plot y*trt / boxstyle=schematic;
  insetgroup n mean q1 q2 q3 / font=swiss  pos=topoff;
  format trt trt.;
  title 'Creative Writing Results';
  run;


  /*  Use the MULTTEST proceusre to perform a 
      permutation test using 10000 new random
      assignments of subjects to the two treatment 
      groups  */

  proc multtest data=set1 permutation nsample=10000 
                 seed=36607 pvals outsamp=pmt; 
	  class trt;  
	  contrast "intr vs extr" 1 -1;
      test mean(y);  
   run; 

/*  The outsamp=pmt command in the previous procedure
    creates an output file that contains the data for
    all 10000 random permutations.  The following prints
    the first 50 lins of that file.  Do not try to 
    print the entire file since it contains 470000 lines 
    and it will be too large for your output window */ 

   proc print data=pmt(obs=50); 
   run;

/* The following code computes the t-values for the
   10000 permutations and creates a histrogram using
   the GCHART procedure in SAS  */

   proc means data=pmt noprint; by _sample_ _class_;
     var y;
	 output out=stats n=n mean=mean css=css;
     run;

   data stats1; set stats; if(_class_=1); 
     n1=n; mean1=mean; css1=css;
     keep _sample_ n1 mean1 css1; run;

  data stats2; set stats; if(_class_=2); 
     n2=n; mean2=mean; css2=css;
     keep _sample_ n2 mean2 css2; run;

  data stats3; merge stats1 stats2; by _sample_; 
     t = (mean1-mean2)/sqrt(((css1+css2)/(n1+n2-2))*((1/n1)+(1/n2)));
     run;

  proc print data=stats3(obs=20);  run;

  goptions reset=global rotate=landscape targetdevice=ps;

  axis1 label = (h=2 r=0 a=90 f=swiss 'Percent')
        length = 4.8 in
        value = (h=2.0 f=swiss);

  axis2 label = (h=2 f=swiss 't-statistics')
        length = 7.0 in
		order = (-5 to 5 by 1)
        value = (h=2.0 f=swiss);

  pattern1 value=empty;

  proc gchart data=stats3;
     vbar t / type=percent space=0 frame
              raxis = axis1 gaxis=axis2;
     title h=2.5 f=swiss "t Values for 10000 Random Permutations";
     run;


/*  Use the TTEST procedure to perform t-tests on
	the original data  */

   proc ttest data=set1;
     class trt; 
	 format trt trt.;
	 run;


/* Compute normal probability plots 
   for the indvidual samples.  Tests
   for normality are provided by 
   previous UNIVARIATE output  */ 
   
   
PROC RANK DATA=set1 NORMAL=BLOM OUT=set2;
  by trt; VAR Y; RANKS Q; run;


/*  Use SASGRAPH to make high quality graphs */

goptions rotate=landscape targetdevice=ps hsize=6in vsize=6.7in;

 axis1 label=(f=swiss r=0 a=90 h=2.2 'Scores')
   ORDER = 0 to 35 by 5
   value=(f=swiss h=2.0)
   length= 4.0 in;

 axis2 label=(f=swiss h=2.2 'Normal Quantiles')
      order = -3.0 to 3.0 by 1
      value=(f=swiss h=2.0)
      length = 4.5in;
      

 symbol1 V=circle H=2.0;
   
   
data set3; set set2; if(trt=1); run;

PROC GPLOT DATA=set3; 
      PLOT Y*Q / vaxis=axis1 haxis=axis2;
      TITLE  H=3.0 f=swiss "Creative Writing Study";
	  Title2  h=2.5 f=swiss "Intrinsic Values";
      Title3  h=2.5 f=swiss "Normal probability plot";
   RUN;

data set4; set set2; if(trt=2); run;

PROC GPLOT DATA=set4; 
      PLOT Y*Q / vaxis=axis1 haxis=axis2;
      TITLE  H=3. f=swiss "Creative Writing Study";
	  Title2  h=2.5 f=swiss "Extrinsic Values";
      Title3  h=2.5 f=swiss "Normal probability plot";
   RUN;


/* Construct a normal probability plot
   for the combined set of residuals */

proc glm data=set1;
  class trt;
  model y = trt / noint solution;
  output out=resid residual=r;
  run;

proc univariate data=resid normal;
  var r; run;

PROC RANK DATA=resid NORMAL=BLOM OUT=resid;
  VAR r; RANKS q; run;


/*  Use SASGRAPH to make high quality graphs */

goptions rotate=landscape targetdevice=ps;

 axis1 label=(f=swiss r=0 a=90 h=2.2 'Residuals')
   ORDER = -15 to 15 by 5
   value=(f=swiss h=2.0)
   length= 4.0 in;

 axis2 label=(f=swiss h=2.2 'Normal Quantiles')
      order = -3.0 to 3.0 by 1
      value=(f=swiss h=2.0)
      length = 4.5 in;

 symbol1 V=circle H=2.0;
   
   
PROC GPLOT DATA=resid; 
      PLOT r*q / vaxis=axis1 haxis=axis2;
      TITLE  H=3.0 f=swiss "Creative Writing Study";
	  Title2  h=2.5 f=swiss "Residuals";
      Title3  h=2.5 f=swiss "Normal probability plot";
   RUN;


/* Perform the Wilcoxon Sum Rank test */

proc npar1way data=set1 wilcoxon;
  class trt;
  var y;
  run;
