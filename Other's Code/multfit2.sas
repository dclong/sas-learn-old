
/* Use the FREQ procedure in SAS
   to test a null hypothesis for
   a multinomial distribution.  
   This code is posted as
        multfit2.sas          */

data set1;  
  input type $9. y;
  datalines;
  white  155
  yellow 40
  green  10
  run;

 proc print data=set1; run;

/* Note that the probabilities in the 
   null hypothesis are listed in the
   order (green, white, yellow) because
   SAS alphabetically orders the values
   of the type variable.  */  

 proc freq data=set1;
   table type / testp = (.0625 .75 .1875);
   weight y;
   run;