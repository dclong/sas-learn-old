options formdlim = '-';

data rats;
  infile 'ratweight.txt';
  input amount $ type $ @;
  
  both = trim(amount) || '/' || type;
  
  do i =  1 to 10;
    input gain @;
    output;
    end;
  
proc print data = rats (obs = 20);
  title 'Rat weight data set ';
       
proc sort;
  by amount type;
  
proc glm;
  class amount type;
  model gain = amount type amount*type;

  lsmeans amount*type / slice = amount;
  title 'Slicing by amount';
    
proc glm;
  by amount;
  class type;
  model gain = type;
  title 'Separate analyses by amount';
  
run;
     