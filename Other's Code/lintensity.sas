

/*  This program analyzes data from the study of the
    effects of light intensity on skin elasticity 
    described in problem 1 on assignment 4 in 2008. 
    The data are posted as lintensity.dat in the data 
    folder of the course web page.  The code is posted
    as lintensity.sas in the SAS folder of the course 
    web page.  Cahnge the infile statement to read the
    data file from the directory where you store it. */


data set1;
  infile 'c:\stat500\data\lintensity.dat';
  input intensity elasticity;
  run;

/*  Print the data  */

proc print data=set1;
  run;

/*  Compute a dot plot  */

goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss h=2.5 a=90 )
      order = 0 to 10 by 1
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.5)
      value=(f=swiss h=2.3)
      order = 1 to 4 by 1
	  offset=(0.3in, 0.3in)
      length = 7 in;

  symbol v=circle h=2 I=none w=2;

proc gplot data=set1;
      plot elasticity*intensity / vaxis=axis1 haxis=axis2;
      title  h=3 F=swiss 'Dot Plot for Skin Elasticity';
      label  elasticity = 'Elasticity';
      label  intensity = 'Light Intensity';
   run;


proc boxplot data=set1;
  plot elasticity*intensity / boxstyle=schematic 
                         vaxis=axis1 haxis=axis2;
  title h=3 "Box Plots for Skin Intensity";
  label  elasticity = 'Elasticity';
  label  intensity = 'Light Intensity';
  run;


/* Compute separate normal probability plots 
   for each level of light intensity */

proc sort data=set1; by intensity; run;

proc rank data=set1 normal=blom out=set2; 
  by intensity;
  var elasticity; ranks q;
  run;

  proc print data=set2; run;

goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss h=2.3 angle=90)
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      order = -2.0 to 3.0 by 1
      length = 5 in;

  symbol v=circle h=2 I=none w=2;
  
proc gplot data=set2; by intensity;
      plot elasticity*q / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Normal Probability Plot';
      label  elasticity = 'Elasticity';
      label  q = 'Standard Normal Quantiles';

   run;

/*  Compute summary statistics and tests of normality */

proc sort data=set1; by intensity; run;

proc univariate data=set1 normal;
  by intensity;
  var elasticity;
  run;


/*  Compute the ANOVA table, list the means and 
    compute t-tests to compare all six pairs of 
    light intensity levels. Also output residuals
    to a temporary file called set2.    */

proc glm data=set1;
  class intensity;
  model elasticity = intensity / p clm;
  contrast "linear trend" intensity -3 -1 1 3;
  estimate "linear trend" intensity -3 -1 1 3;
  contrast "quadratic trend" intensity -1 1 1 -1;
  estimate "quadratic trend" intensity -1 1 1 -1;
  contrast "cubic trend" intensity -1 3 -3 1;
  estimate "cubic trend" intensity -1 3 -3 1;
  lsmeans intensity / stderr pdiff tdiff;
  means intensity / tukey scheffe snk;
  output out=set3 residual=r predicted=pred;
  run;


/*  Construct box plots of residuals */

proc boxplot data=set3;
  plot r*intensity / boxstyle=schematic 
                     vaxis=axis1 haxis=axis2;
  title h=3 "Box Plots for Residuals";
  label  r = 'Residuals';
  label  intensity = 'Light Intensity';
  run;


/*  Construct a normal probability plot and dot plot
    for the residuals and show the estimated means
    and observations on the same plot */

/* First compute the standard normal quantiles 
   needed for the normal probability plot */ 

proc rank data=set3 normal=blom out=set3;
  var r; ranks q;
  run;

/* Compute tests of normality for the 
   combined set of residuals  */

proc univariate data=set3 normal;
  var r;
  run;

/* Set up the axis and plotting symbols for
   the various plots  */

goptions targetdevice=pscolor rotate=landscape;

  axis1 label=(f=swiss h=2.3 angle=90 ' Residuals ')
      order = -4 to 4 by 2
      value=(f=swiss h=2.0)
      length= 5 in;

  axis2 label=(f=swiss h=2.3 ' Standard Normal Quantiles ' )
      value=(f=swiss h=2.0)
      order = -3.0 to 3.0 by 1
      length = 6.5 in;

  axis3 label=(f=swiss h=2.3 ' Light Intensity ' )
      value=(f=swiss h=2.0)
      order = 1 to 4 by 1
	  offset=(0.3in, 0.3in)
      length = 6.5 in;

  axis4 label=(f=swiss h=2.3 angle=90 ' Skin Elasticity ')
      order = 0 to 10 by 1
      value=(f=swiss h=2.0)
      length= 5 in;

  symbol1 c=black v=circle h=2 i=none;
  symbol2 c=black v=dot h=2 i=none;
  symbol3 c=darkblue v=none i=join w=3;
  symbol4 c=bgr v=K f=special i=none h=3; 

proc gplot data=set3;
      plot r*q=4 / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Normal Probability Plot';
      plot r*intensity=2 / vaxis=axis1 haxis=axis3;
      title1  h=3. F=swiss 'Residual Dot Plot';
      plot pred*intensity=3 elasticity*intensity=1 / 
               overlay vaxis=axis4 haxis=axis3;
      title1  h=3. F=swiss 'Trend in the Estimated Means';

   run;


/* Compute the Brown-Forsythe Test */

proc means data=set3 median; by intensity;
   var r; 
   output out=rstats median=median;
   run;

proc print data=rstats; run;

data set4; merge set3 rstats; by intensity; 
   d=abs(r-median);
   run;

proc print data=set4; run;

proc glm data=set4;
  class intensity;
  model d = intensity;
  run;

/* plot log(s) against log(mean) */

data means; set means;
  logmean=log(mean);
  logs=log(s);
  run;

 axis1 label=(f=swiss a=90 h=2.3)
      value=(f=swiss h=2.1)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.1)
      length = 5 in;

  symbol v=dot h=2 i=none w=2;

proc gplot data=means;
      plot logs*logmean / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Log(s) versus Log(mean)';
      label  logs = 'Log(s)';
      label  logmean = 'Log(mean)';
   run;

/* Compute the Kruskal-Wallace Test */

proc npar1way data=set1 wilcoxon;
  class intensity;
  var elasticity;  run;
