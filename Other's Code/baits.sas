


/*  Program to analyze data from the fly bait study.
    This program is posted in the SAS folder of the 
    course webpage as baits.sas  */


data set1;
  infile 'c:/stat500/sas/baits.dat';
  input location bait y;
  y = log(y);
  label y = 'Number of captured flies';
run;

proc glm data=set1;
  class location bait;
  model y = location bait / p clm;
  output out=set3 residual=r predicted=yhat;
  lsmeans bait / stderr pdiff;
  means bait / tukey snk;
  run;


/* Construct residual plots */

proc rank data=set3 normal=blom out=set3;
  var r; ranks q; run;

proc univariate data=set3 normal;
  var r;
  run;


/*  Construct a normal probability plot for the residuals
    and compute the Shapiro-Wilk test for normaility */

goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss a=90 h=2.3)
      order = -1 to 1 by .5
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      order = -3.0 to 3.0 by 1
      length = 5 in;

  symbol v=circle h=2 I=none w=2;

proc gplot data=set3;
      plot r*q / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Normal Probability Plot';
      label  r = 'Ordered Residuals';
      label  q = 'Standard Normal Quantiles';

   run;


 axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      order = 1 to 5 by 1
      length = 5 in;

proc gplot data=set3;
      plot r*yhat / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Residual Plot';
      label  r = 'Residuals';
      label  yhat = 'Estimated Mean Capture Counts';

   run;

/* Compute Tukey's one df test for
   additivity.  First compute the squares
   of the estimated means */

data set3; set set3;
  yhat2 = yhat*yhat;
  run;


/* Now fit a model with additive effects
   of treatments and blocks and an extra
   term corresponding to a regression on
   the squares of the estimated means.
   This extra term must appear last in the
   model statement.  The response variable
   is the residuals from the original model
   Tukeys one df test for additivity is
   the F-test that tests that the coefficient
   for the extra term is zero.     */

proc glm data=set3;
  class location bait;
  model r = location bait yhat2;
  output out=set4 residual=r2;
  run;
