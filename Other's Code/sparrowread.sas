/* Various ways to read the sparrow data set */

/* to skip the first line (usu. header), */
/*   add firstobs = 2 to infile command */

data sparrow;
 infile 'sparrow.txt' firstobs = 2;
 input humerus fate $;
run;
     
/* to read the file directly from the web page */
/* associate the URL with a file handle (filename statement) */
/* then read from the file handle (note no quotes in infile) */
/* could also use first obs if desired */

filename sparrow url "http://www.public.iastate.edu/~pdixon/stat500/data/sparrow.txt";

data sparrow;
  infile sparrow;
  input humerus fate $;
run;
     
/* to read a file from an excel spreadsheet (.xls = excel 2003 format) */
/* version 9.2 may know how to read .xlsx (excel 2007 format) */
/*   but I haven't had a chance to check */

proc import datafile = "sparrow.xls" out = sparrow;
run;
          


      
 