
/* Program to analyze data for problem 1
   on assignment 10 in 2006.             */


data set1;
  input tube $ temp glass y;
datalines;
 A    100 1   580
 A    125 1  1090
 A    150 1  1392
 A    100 2   579
 A    125 2  1070
 A    150 2  1328
 A    100 3   599
 A    125 3  1066
 A    150 3   899
 B    100 1   568
 B    125 1  1077
 B    150 1  1380
 B    100 2   530
 B    125 2  1035
 B    150 2  1312
 B    100 3   546
 B    125 3  1045
 B    150 3   867
 C    100 1   570
 C    125 1  1085
 C    150 1  1386
 C    100 2   530
 C    125 2  1030
 C    150 2  1299
 C    100 3   575
 C    125 3  1053
 C    150 3   904
 D    100 1   562
 D    125 1  1070
 D    150 1  1376
 D    100 2   525
 D    125 2  1000
 D    150 2  1300
 D    100 3   556
 D    125 3  1041
 D    150 3   860
run;


proc glm data=set1;
  class tube glass temp;
  model y = tube glass temp glass*temp / solution clm;
  lsmeans glass / stderr pdiff;
  lsmeans temp / pdiff stderr;
  lsmeans glass*temp / stderr pdiff;
  run;

proc sort data=set1; by glass temp;
proc means data=set1 noprint; by glass temp;
  var y;
  output out=means mean=my;
  run;


 goptions cback=white colors=(black)
    targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=2.5 )
      ORDER = 100 to 150 by 25 
      offset=(1cm, 1cm)
      value=(f=swiss h=2.0)
      w=3.0 length= 6.5 in;

axis2 label=(f=swiss h=2.0 a=90)
      order = 500 to 1400 by 100
      value=(f=swiss h=2.0)
      w= 3.0 length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.0 w=3 l=1 i=join ;
       SYMBOL2 V=square H=2.0 w=3 l=3 i=join ;
       SYMBOL3 V=diamond H=2.0 w=3 l=17 i=join ;


  PROC GPLOT DATA=means;
    PLOT my*temp=glass / vaxis=axis2 haxis=axis1;
    title1  H=3.0 F=swiss "Oscilloscope Light Ouput";
       LABEL my='Light Output';
       LABEL temp = 'Temperature (degrees C)';
   RUN;
