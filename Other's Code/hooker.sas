
/*  This program is posted as
    hooker.sas on the course web page */

/*  First enter the data */
filename hooker url "http://streaming.stat.iastate.edu/~stat500/Data/hooker.dat";

DATA set1;
  INFILE hooker;
  INPUT location X Y;
  LOGY = LOG(Y);
  LABEL X = BOILING POINT (deg. F)
        Y = ATM. PRESSURE (in. Hg)
        LOGY = LOG(ATM. PRESSURE);
run;

/*  Print the data using specified column headings */

proc sort data=set1; 
     by x;
run;

PROC PRINT DATA=SET1 UNIFORM SPLIT='*';
     VAR X Y LOGY;
     LABEL X = 'BOILING POINT*OF WATER*(degrees F)'
           Y = 'ATMOSPHERIC*PRESSURE*(inches Hg)'
           LOGY = 'NATURAL LOG OF*ATMOSPHERIC*PRESSURE';
     TITLE 'HOOKER DATA';
run;

/* Plot the data */

goptions targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=2.5)
   ORDER = 180 to 215 by 5
   value=(f=swiss h=2.0)
   length= 6.5 in;

axis2 label=(f=swiss h=2.0  a=90)
      order = 2.7 to 3.4 by .1
      value=(f=swiss h=2.0)
      length = 4.0 in;

axis3 label=(f=swiss h=2.0  a=90)
      order = 15 to 30 by 5
      value=(f=swiss h=2.0)
      length = 4.0 in;

   symbol1 V=circle H=2.0 i=r;
 
PROC GPLOT DATA=set1;
PLOT Y*X /  vaxis=axis3 haxis=axis1;
PLOT LOGY*X / vaxis=axis2 haxis=axis1;
TITLE1  H=2.0 F=swiss ;
TITLE2  H=3.5 F=swiss "Hooker Data";
       LABEL X='Boiling point of water (degrees F)';
       LABEL LOGY = 'Log Pressure';
	   LABEL Y = 'Pressure';
   RUN;



/*  Compute least squares estimates of regression coefficients
    and output the residuals, and predicted values to a work
    file that can be used by other SAS procedures.  */

PROC REG DATA=SET1 ;
  MODEL LOGY = X / P CLM CLI;
  OUTPUT OUT=SET2 P=YHAT R=RESIDUAL STUDENT=STDRES
         STDP=STDP;
  run;

