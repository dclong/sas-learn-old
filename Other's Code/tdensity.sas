
/* program to generate plots of density
   functions for the family of t-distributions */

data set1;
  do i = 1 to 1001;
    x = 5 - (1001 - i)/100;
    f2 = ((1+x*x/2)**(-3/2))/(sqrt(2)*gamma(0.5)*gamma(1)/gamma(1.5));
    f5 = ((1+x*x/5)**(-6/2))/(sqrt(5)*gamma(0.5)*gamma(5/2)/gamma(3));
    f10 = ((1+x*x/10)**(-11/2))/(sqrt(10)*gamma(0.5)*gamma(5)/gamma(5.5));
    f20 = ((1+x*x/20)**(-21/2))/(sqrt(20)*gamma(0.5)*gamma(10)/gamma(10.5));
    fn = exp(-x*x/2)/sqrt(2*3.1416);
    output;
    end;

goptions  cback=white colors=black  targetdevice=ps;

  axis1 label = (h=3 r=0 a=90 f=swiss 'f(t)')
        length = 5 in
        color = black width=3.0 style=1
        order = 0 to .4 by .05;

  axis2 label = (h=2.5 f=swiss 't')
        length = 6.0 in
        color=black width=3.0 style=1
        order = -5 to 5 by 1;

  symbol1 v=none i=spline l=1 h=2 c=black width=3;
  symbol2 v=none i=spline l=3 h=2 c=black width=3;
  symbol3 v=none i=spline l=9 h=2 c=black width=3;

proc gplot data=set1;
  plot (fn f2 f10)*x / overlay vaxis=axis1 haxis=axis2;
  title1 h=4;
  title2 h=3.5 f=triplex c=black 'Density functions for the t-distribution';
  run;
