options noovp;      
/* enables overlay plots on vincent.  not necessary on pc's */
     

data brain;
  infile 'brain.txt';
  input gest brain body litter species $;
  
  loggest = log(gest);
  logbrain = log(brain);
  logbody = log(body);
  loglitter = log(litter);

  if gest ne .;
      
/* multiple regression can be done in proc reg or proc glm; */
/*  proc reg provides many diagnostics (next week) */
/*    and a more general test statement */

proc reg;
   model logbrain = logbody loglitter loggest;
test1: test loglitter = 0;
test2: test loglitter = 0, loggest = 0;
test3: test loglitter+loggest = 1;
   title 'Regression of log brain size on log body and log litter';
   title2' With test statements';
   
run;
         
/* to use proc glm, just omit the regression variables from the class stmt */

proc glm;
   model logbrain = logbody loglitter loggest;
   contrast 'test1' loglitter 1;
   contrast 'test2' loglitter 1, loggest 1;
   estimate 'test3' loglitter 1 loggest 1;
   
   
   title 'Regression of log brain size on log body, log litter and log gest';
   output out = resids r = resid p = yhat;
   
run;

proc glm;
   model logbrain = logbody;
   title 'Regression of log brain size on log body only';

/* plot of residual vs. previous residual is not appropriate here, */
/*   but here's how to do it anyway. */
/*   this is appropriate if data collected over time */

/* the only hard part is combining a residual and its previous resid */
/*   into one line of data */

data resids2;
  set resids;

  lagresid =  lag(resid);     /* the lag function returns the value */
                              /*  from the previous observation */

/* once you've done that you can print, plot, calculate a correlation */
/*  or do anything you wish */

proc print data = resids2(obs = 10);
     
proc plot;
   plot lagresid*resid;
              
/* The following demonstrates the partial regression plot */

  
proc reg data = brain;
   model logbrain = logbody loglitter ;
   output out = resids r = bresid;
   
proc reg;
  model loggest = logbody loglitter;
  output out = resid2 r = gresid;
  
/*  the second proc reg uses the resids data set, so resids2 contains */
/*    both residuals */
                   
proc reg;
  model bresid = gresid;
  output out = resid3 p = yhat;
  
proc plot;
  plot bresid*gresid yhat*gresid = '*' /overlay;
run;
