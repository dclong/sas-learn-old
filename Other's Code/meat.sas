/* read the real data into the meat data set */

data meat;
 input time ph;
 logtime =  log(time);
 
cards;
1 7.02
1 6.93
2 6.42
2 6.51
4 6.07
4 5.99
6 5.59
6 5.80
8 5.51
8 5.36
;

run;

/* want to make predictions at additional time values */
/*   read those time values into a new data set, meat2 */
/*   the . means that the pH value is missing in these observations */

data meat2;
  input time ph;
  logtime = log(time);
datalines;
1   .
1.5 .
2   .
2.5 .
3   .
3.5 .
4   .
4.5 .
5   .
;

/* create a new data set called all by concatenating the two data sets */

data all;
  set meat meat2;

/* demonstrate that the missing pH values do not change the regression info */
/*   do a regression on the data in meat */

/* the estimate statement gives the predicted value and the s.e. (prediction */

proc glm data = meat;
  model ph = logtime;
  estimate 'ph at 5 hr' intercept 1 logtime 1.6094;
  estimate 'ph at 2 hr' intercept 1 logtime 0.6931;
  
  title 'regression on observed data';
                
/* and again on the data in all.  The output statement produces a data set */
/*   called set1, with lots of additional variables.  */
/*   you can use or omit keywords and variable names as needed */

proc glm data = all;
  model ph=logtime;
  output out=set1 
     residual=r               /* residuals */
     predicted=yhat           /* predicted values */
     stdp = sepred            /* s.e. of predicted mean (the line) */
     stdi = stobs             /* s.e. on predicted observation */
     lclm = lcipred           /* lower 95% c.i. limit for pred. mean */
     uclm = ucipred           /* upper 95% c.i. limit for pred. mean */
     lcl = lciobs             /* lower 95% c.i. limit for pred. obs */
     ucl = uciobs             /* upper 95% c.i. limit for pred. obs */
     ;                        /* and here, finally, is the end of the output */
  title 'regression on all data';
  
/* all the information from the output command goes into a data set */
/*  print it to see the stuff you need */

proc print;
  title 'output from all obs';
  
/* can also plot stuff in many ways */
/*  the following plots three pieces of information together: */
/*    the predicted line and the upper and lower bounds of the */
/*    95% c.i. for predicted observations */
/* the where statement restricts the plot to those obs. where ph is missing */
/*   this restricts the plot to those points in the meat2 data set */
/*   (i.e. the additional regularly spaced time points) */
 
proc plot;
  where ph = .;
  plot yhat*logtime = '*' uciobs*logtime = 'u' lciobs*logtime = 'l' /overlay;
run;
     
    
       

