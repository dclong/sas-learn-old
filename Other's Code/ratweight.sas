data rats;
  infile 'ratweight.txt';
  input amount $ type $ @;

/* create a variable encoding both amount and type */
/*  || is the string concatenate operator */
/*  SAS stores character variables as fixed length objects */
/*   8 characters is the default, */
/*   shorter strings are padded with trailing spaces, */
/*   trim() removes those trailing spaces   */

  both = trim(amount) || '/' || type;
  
  do i =  1 to 10;
    input gain @;
    output;
    end;
run;
       
/* an alternative way to read these data */
/* without knowing there are 10 obs per treatment */
/*  this approach works with unequal numbers per treatment */

/* the missover option is crucial.  By default, if SAS is asked */
/*  to read beyond the end of a line, it goes to the next line */
/*  missover tells SAS to return a missing value  instead */

data rat2;
  infile 'ratweight.txt' missover;
  input amount $ type $ @ ;
  
  both = trim(amount) || '/' || type;
  
  input gain @;
  
  do until (gain =  .);
    output;
    input gain @;
    end;
run;

/* the order of input and output are reversed from the first data step */
/*  this is because we read an observation before entering the loop */
/*  we stay in the loop so long as we have a valid value */
/* we leave the loop as soon as we get a missing value (end of line) */

/* Here's one way to store a SAS data set as a text file */
/* the if statement adds a header before the first data line */

data _null_;
  file 'ratweight2.txt';
     
  set rats;
  if _n_ =  1 then put 'amount type gain';
  put amount type gain;
run;
       
/* proc export; is another way to output all sorts of formats */
/*  including .csv, .xls, and many database formats */

proc sort;
  by amount type;
  
proc glm;
  class amount type;
  model gain = amount type amount*type;
     /* could also write amount | type */
     
  lsmeans amount amount*type /stderr;
  
  estimate 'beef-pork' type 1 0 -1;
  estimate 'high-low' amount 1 -1;
  
  estimate 'high-low in beef' amount 1 -1 amount*type 1 0 0 -1 0 0;
  estimate 'beef-pork in low' type 1 0 -1 amount*type 0 0 0 1 0 -1;
  estimate 'high-low in beef vs high-low in pork' amount*type 1 0 -1 -1 0 1;
  
  contrast 'av of beef,pork - cereal' type 1 -2 1;
  contrast 'beef-pork' type 1 0 -1;
  title '2 way factorial ANOVA';
run;
       
proc glm;
  class both;
  model gain = both;
  
  lsmeans both /stderr;
  
  contrast 'amount' both 1 1 1 -1 -1 -1;
  contrast 'type'  both 1 -1 0 1 -1 0,
                   both 1 0 -1 1 0 -1;
  contrast 'amount*type' both 1 -1 0 -1 1 0,
                         both 1 0 -1 -1 0 1;
 
  estimate 'high-low in beef' both 1 0 0 -1 0 0;
  title '1-way anova approach to factorial ANOVA';
run;




