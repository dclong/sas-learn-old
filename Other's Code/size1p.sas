

/*  This program computes sample sizes needed to 
    obtain a test of a hypothesis about a single 
    proportion with a specified power value.  
    It also computes the number of observations 
    needed to obtain a confidence interval with 
    a specified accuracy. This program is stored 
    in the file

                         size1p.sas        */

options nonumber nodate linesize=64;

proc iml;
  start samples;
  p0 = .7;           /* Enter the proportion
                         corrsponding to the
                         null hypothesis    */

  pa = {.6 .5 .4 .3};     /* Enter alternatives */

  power = {.8 .9 .95 .99};  /* Enter power values */

  alpha = .05;         /* Enter Type I error level */

  za = probit(1-alpha/2);
  za1 = probit(1-alpha);
  nb = ncol(power);
  np = ncol(pa);
  size = j(1,np);
  size1 = j(1,np);

  print ,,,,,, 'Sample sizes for tests of a single proportion';

  do i1 = 1 to np;  /* Cycle across alternatives */
     p2 = pa[1,i1];


  do i2 = 1 to nb;  /*  Cycle across power levels*/
     zb = probit(power[1,i2]);
     size[1,i2] = ((za*sqrt(p0*(1-p0))
           +zb*sqrt(p2*(1-p2)))**2)/((p0-p2)**2);
     size1[1,i2] = ((za1*sqrt(p0*(1-p0))
           +zb*sqrt(p2*(1-p2)))**2)/((p0-p2)**2);
  end;

  print,,,,,,, p0 p2 alpha power;
  size = int(size) + j(1,np);
  print 'Sample sizes (2-sided test):' size;

  size = int(size1) + j(1,np);
  print 'Sample sizes (1-sided test):' size;
  end;


/*  Compute sample size needed to obtain a
    confidence interval with a specified
    margin of error    */

  p = {.5 .4 .3 .2 .1 .01};  /* Enter possible
                                values of the
                                true proportion */

  level = 0.95;      /* Enter confidence level */

                     /* Enter desired margin of
                        error  */

  me = { 0.035};

                     /* Compute needed sample sizes */

  p = t(p);
  alpha2  = (1-level)/2;
  np = nrow(p);
  n = ((probit(1-alpha2)/me)**2)#p#(j(np,1)-p);
  n = int(n) + j(np,1);

  percent = level*100;

  print ,,,,,, 'Sample sizes for ' percent 'percent confidence interval:';
  print p n;


finish;
run samples;
