
/*  This SAS code for making inferences about
    a correlation coefficient is posted as
       fwheat.sas on the course web page */ 

/*  First enter the data */

data set1;
  input year x y;
  label   x = yield in plot 1
          y = yield in plot 2;
  d=x-y;
datalines;
1873 35.81 22.75 
1874 38.19 39.56
1875 30.50 26.63 
1876 33.31 25.50
1877 40.12 19.12
1878 37.19 32.19
1879 21.94 17.25
1880 34.06 34.31
1881 35.44 26.13
1882 31.81 34.75
1883 43.38 36.31
1884 40.44 37.75
run;


/* Print the data file */

proc print data=set1;
run;


/* Compute the t test and summary 
   statistics for the differences */

proc univariate data=set1 normal plot;
  var d;
  run;


/* Compute correlation */

proc corr data=set1 Fisher(biasadj=no); 
  var x y; run;


/* Plot the data */

goptions targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=3.0)
      ORDER = 15 to 45 by 5
      value=(f=triplex h=2.5)
      length= 5in;

axis2 label=(f=swiss h=3.0 a=90)
      order = 15 to 45 by 5
      value=(f=swiss h=2.5)
      length = 5in;

SYMBOL1 V=CIRCLE H=2.5 ;
    
PROC GPLOT DATA=SET1;
      plot y*x / vaxis=axis2 haxis=axis1;
  TITLE1  H=2.0 F=swiss " ";
  TITLE2  H=3.0 F=swiss "Fisher Broadbalk Wheat Data";
       LABEL y='Yield in second plot';
       LABEL x = 'Yield in first plot';
   RUN;

