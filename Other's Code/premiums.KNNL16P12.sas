

/*  This program analyzes the premium distribution
    data from problem 16.12 at the end of Chapter 16
    in KNNL. The data are posted as premiums.KNNL16p12.dat 
    in the data folder of the course web page, and this 
    code is posted as premiums.KNNL16p12.sas in the data 
    folder of the course web page*/

/*  First enter the data  */

data set1;
  infile 'c:/stat500/data/premiums.KNNL16p12.dat';
  input time agent case;
  run;


/*  Print the data  */

proc print data=set1;
  run;

/*  Compute a dot plot and a box plot  */

goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss h=2.5 a=90 )
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.5)
      value=(f=swiss h=2.3)
      order = 1 to 5 by 1
	  offset=(0.3in, 0.3in)
      length = 7 in;

  symbol v=circle h=2 I=none w=2;

proc gplot data=set1;
      plot time*agent / vaxis=axis1 haxis=axis2;
      title  h=3 F=swiss 'Dot Plot for Agent Transaction Times';
      label  agent = 'Agent';
      label  time = 'Transaction Time (in days)';
   run;

proc boxplot data=set1;
  plot time*agent / boxstyle=schematic
				    vaxis=axis1 haxis=axis2;
  title  h=3 F=swiss 'Dot Plot for Agent Transaction Times';
      label  agent = 'Agent';
      label  time = 'Transaction Time (in days)';
  run;



/* Compute separate normal probability plots 
   for each level of light intensity */



/*  Compute summary statistics and box plots */

proc sort data=set1; by agent; run;

proc univariate data=set1 normal;
  by agent;
  var time;
  run;



/*  Compute the ANOVA table, apply the Tukey HSD
    procedure to determine which agents differ
    with respect to mean transaction time.  Also
    list the means and compute t-tests for all
    possible pairs of means.  Output residuals
    to a file.    */

proc glm data=set1;
  class agent;
  model time = agent / p clm;
  contrast "men vs women" agent -3 2 -3 2 2;
  estimate "men vs women" agent -3 2 -3 2 2;
  contrast "exp vs inexp" agent -2 -2 3 3 -2;
  estimate "exp vs inexp" agent -2 -2 3 3 -2;
  lsmeans agent / stderr pdiff tdiff;
  means agent / tukey scheffe snk;
  output out=set2 residual=r predicted=meantime;
  run;


/*  Construct a normal probability plot for the residuals
    and compute the Shapiro-Wilk test for normaility */


proc rank data=set2 normal=blom out=set2;
  var r; ranks q;
  run;


proc univariate data=set2 normal;
  var r;
  run;


goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss h=2.3 a=90)
      order = -10 to 10 by 5
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      order = -3.0 to 3.0 by 1
      length = 5 in;

  symbol v=circle h=2 I=none w=2;

proc gplot data=set2;
      plot r*q / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Normal Probability Plot';
      label  r = 'Residuals';
      label  q = 'q';

   run;


/* Compute the Brown-Forsythe Test */

proc means data=set1 mean stddev median; by agent;
   var time; 
   output out=means mean=mean stddev=s median=median;
   run;

proc print data=means; run;

data set3; merge set1 means; by agent; 
   d=abs(time-median);
   run;

proc print data=set3; run;

proc glm data=set3;
  class agent;
  model d = agent ;
  run;

/* plot log(s) against log(mean) */

data means; set means;
  logmean=log(mean);
  logs=log(s);
  run;

 axis1 label=(f=swiss a=90 h=2.3)
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      length = 5 in;

  symbol v=dot h=2 I=none w=2;

proc gplot data=means;
      plot logs*logmean / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Log(s) versus Log(mean)';
      label  logs = 'Log(s)';
      label  logmean = 'Log(mean)';
   run;


proc npar1way data=set1 wilcoxon;
  class agent;
  var time;  run;

