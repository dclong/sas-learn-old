data f;
  input p df dfe;
  q =  finv(p,df,dfe);
cards;
0.2 14 238
0.5 14 238
;
proc print;
  title 'Quantiles of F distributions';
run;
          

