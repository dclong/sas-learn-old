
/*  This program uses the cup loss data 
    considered in class to illustrate how 
    to use the GLM procedure in SAS to 
    compute a lack-of-fit test. It is posted
    as cuploss.sas on the course web page */


/*  First enter the data */

DATA SET1;
  INPUT X Y;
  LABEL X = Bottle loss
        Y = Cup loss;
datalines;
2.1 2.6
2.6 2.8
2.7 2.9
2.7 3.6
2.7 3.6
3.0 3.1
3.0 3.1
3.0 3.4
3.1 3.9
3.1 3.1
3.2 4.1
3.3 3.6
3.6 4.0
3.8 3.6
run;


/*  Print the data using column headings */

PROC PRINT DATA=SET1 UNIFORM SPLIT='*';
  VAR X Y;
  LABEL X = 'Bottle*Loss'
        Y = 'Cup*Loss';
  TITLE 'Bottle Loss Data';
  run;


/*  Plot the response against the explanatory variable  */


goptions cback=white colors=(black) rotate=portrait
     targetdevice=ps;

axis1 label=(f=swiss h=2.2)
      ORDER = 2.0 to 4.0 by .5
      value=(f=swiss h=2.0)
      length= 5.5 in;

axis2 label=(f=swiss h=2.2 a=90)
      order = 2.0 to 4.5 by .5
      value=(f=swiss h=2.0)
      length = 5.0 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 w=4 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT y*x / vaxis=axis2 haxis=axis1;
    TITLE1  H=5.0 F=swiss ;
    TITLE2  H=4.0 F=swiss "Cup Loss Data";
       LABEL x='Bottle loss';
       LABEL  Y = 'Cup loss';
   RUN;


/*  Compute least squares estimates of 
    regression coefficients and output 
    residuals, and predicted values  */

PROC REG DATA=SET1 ;
  MODEL Y = X / P CLM CLI;
  OUTPUT OUT=SET2 P=YHAT R=RESIDUAL;
  run;

/* Plot the estimated line */

goptions cback=white colors=(black) rotate=portrait
     targetdevice=ps;

axis1 label=(f=swiss h=2.2)
      ORDER = 2.0 to 4.0 by .5
      value=(f=swiss h=2.0)
      length= 5.5 in;

axis2 label=(f=swiss h=2.2 a=90)
      order = 2.0 to 4.5 by .5
      value=(f=swiss h=2.0)
      length = 5.0 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 w=4 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT y*x yhat*x / overlay vaxis=axis2 haxis=axis1;
    TITLE1  H=5.0 F=swiss ;
    TITLE2  H=4.0 F=swiss "Cup Loss Data";
       LABEL x='Bottle loss';
       LABEL  Y = 'Cup loss';
   RUN;


/*  Plot residuals against bottle  
    loss to check for homogeneity of 
    variances and other patterns   */


axis1 label=(f=swiss h=2.2)
      ORDER = 2.0 to 4.0 by 0.5
      value=(f=swiss h=2.0)
      length= 5.5 in;

axis2 label=(f=swiss h=2.2 a=90)
      order = -1 to 1 by .5
      value=(f=swiss h=2.0)
      length = 5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*x  /  vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss ;
TITLE2  H=3.0 F=swiss "Cup Loss Data";
title3  h=3.0 f=swiss "Residual Plot";
       LABEL yhat='Predicted values';
       LABEL residual = 'Residuals';
   RUN;


/*  Compute normal probability plot 
    for residuals */

PROC RANK DATA=SET2 NORMAL=BLOM OUT=SET2;
  VAR RESIDUAL;
  RANKS Q;

axis1 label=(f=swiss h=2.2)
      ORDER = -2.0 to 2.0 by 1
      value=(f=swiss h=2.0)
      length= 5.5 in;

axis2 label=(f=swiss h=2.2 a=90)
      order = -1.0 to 1.0 by 0.5
      value=(f=swiss h=2.0)
      length = 5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;
       SYMBOL3 V=NONE  H=2 L=3 I=SPLINE;
       SYMBOL4 V=NONE  H=2 L=3 I=SPLINE;

   PROC GPLOT DATA=SET2;
      PLOT residual*q  / vaxis=axis2 haxis=axis1;
     TITLE1  H=4.0 F=swiss ;
     TITLE2  H=3.0 F=swiss "Cup Loss Data";
     title3  h=3.0 f=swiss "Normal probability plot";
       LABEL q='q';
       LABEL residual = 'Residuals';
   RUN;


PROC UNIVARIATE DATA=SET2 NORMAL PLOT;
  VAR RESIDUAL;
  TITLE 'TEST OF NORMALITY';
  run;

/* perform a lack-of-fit test */

data set1; set set1;
  bottle = x;
  run;

proc print data=set1;  run;

proc glm data=set1;
  class bottle;
  model y = bottle;
  run;

proc glm data=set1;
  class bottle;
  model y = x bottle;
  run;






