/* determine probability of less than specified value */
/*  for a t distribution with the given d.f. */
/* this is the inverse computation to that done by tquant.sas */

data tprob;
  input t df;
  p = probt(t,df);
  
cards;
2.00 10
2.00 20
;

proc print;
  title 'Probabilities of observed < T from a t distribution';
   
run;
     