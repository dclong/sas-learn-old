


/* Program to generate plots of density
   functions for central chi-square distributions
   This code is posted as chisq.densities.sas */

data set1;
    do  i= 1 to 1000;
    x = i/100;
    f1 = (x**((1-2)/2))*exp(-x/2)/((2**(1/2))*gamma(1/2));
    f3 = (x**((3-2)/2))*exp(-x/2)/((2**(3/2))*gamma(3/2));
    f5 = (x**((5-2)/2))*exp(-x/2)/((2**(5/2))*gamma(5/2));
    f7 = (x**((7-2)/2))*exp(-x/2)/((2**(7/2))*gamma(7/2));
    output;
    end;

goptions  cback=white colors=black targetdevice=ps;

  axis1 label = (h=3 r=0 a=90 f=swiss 'density')
        length = 6 in
        color = black width=6.0 style=1
        order = 0 to 0.5 by .1;

  axis2 label = (h=3 f=swiss 'quantile')
        length = 6.5 in
        color=black width=6.0 style=1
        order = 0 to 10 by 1;

  symbol1 v=none i=spline l=1 h=2 c=black width=4;
  symbol2 v=none i=spline l=3 h=2 c=black width=4;
  symbol3 v=none i=spline l=9 h=2 c=black width=4;
  symbol4 v=none i=spline l=16 h=2 c=black width=4;

proc gplot data=set1;
  plot (f1 f3 f5 f7)*x / overlay vaxis=axis1 haxis=axis2;
  title1 h=4;
  title2 h=3.5 f=swiss c=black 'Central Chi-Square Densities';
  run;
