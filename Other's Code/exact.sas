
/* This program is stored in the file

             exact.sas             */


DATA SET1;
  INPUT ROW COL COUNT;
  CARDS;
1 1 9
1 2 1
2 1 5
2 2 5
run;

PROC FREQ DATA=SET1;
  TABLES ROW*COL / CHISQ EXACT;
  WEIGHT COUNT;
RUN;


DATA SET2;
   INPUT ROW COL X;
   CARDS;
1 1 3
1 2 6
1 3 11
2 1 8
2 2 4
2 3 8
3 1 10
3 2 5
3 3 5
RUN;

PROC FREQ DATA=SET2;
  TABLES ROW*COL / EXACT CHISQ;
  WEIGHT X;
  RUN;
