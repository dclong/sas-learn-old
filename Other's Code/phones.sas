
options nodate nonumber nocenter linesize=64;

/*  This program is posted as phones.sas
    in the SAS folder on the course web page

    The data are posted as phones.dat in the
	data folder on the course web page

    Note that the data file contains an extra 
    lines with information on explanatory variables 
    for making a prediction, but a missing value 
    appears for the response variable      */

/* Establish a library reference */

libname plots 'c:\documents and settings\kkoehler.IASTATE\my documents\courses\st500\sas\';


/*  Enter the data */

data set1;
  infile 'c:/documents and settings/kkoehler.IASTATE/my documents/courses/st500/sas/phones.dat';
  input country $15. x1 x2 x3 y;
run;

proc print data=set1;
run;


/*  Plot y against each x */

goptions targetdevice=ps rotate=portrait;

  axis1 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      length= 5.5 in;

  axis2 label=(f=swiss h=2.3 a=90 r=0)
      order = 20 to 90 by 10
      value=(f=swiss h=2.3)
      length = 5.5 in;
  
  SYMBOL1 V=CIRCLE H=2.5 ;

 PROC GPLOT DATA=set1;
   PLOT y*(x1 x2 x3) /  vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss "Incidence of Heart Disease";
   TITLE3  H=2.2 F=swiss "(55-59 year old males)";
       LABEL  y = "100 log(deaths per 100000 males)";
	   LABEL  x1 = 'Telephones per 1000 persons';
	   LABEL  x2 = 'Percent fat calories';
	   Label  x3 = 'Percent animal protein calories';
   RUN;


/* Correlations and summary statistics */


proc corr data=set1;
  var y x1 x2 x3;
  run;


/*  Regression of y on x1 */

proc reg data=set1;
  model y = x1 / p covb clm cli;
  run;


/*  Regression of y on x2  */

proc reg data=set1;
  model y = x2 / p covb clm cli;
  run;


/*  Regression of y on x3  */

proc reg data=set1;
  model y = x3 / p covb clm cli;
  run;



/*  Regression of y on all three explaantory variables  */

proc reg data=set1 outest=parms covout;
  model y = x1 x2 x3 /p covb clm cli ss1 ss2;
  output out=setall residual=r predicted=yhat stdp=stdmean
       l95m=lmeam u95m=umean stdi=stdpred l95=lpred u95=upred;
  run;

/*  Print the file containing the parameter estimates  */

proc print data=parms;
  run;

/*  Print the file containing residuals and predicted values  */

proc print data=setall;
  run;


/*  Plot residuals against explantory variable
    to check for homogeneity of variance and
    patterns in the residuals that would 
    suggest that the model is inadequate  */

data setall; set setall;
  x12=x1*x2;
  zero=0;
  run;



axis1 label=(f=swiss h=2.5)
   value=(f=swiss h=2.0)
   length= 5.5 in;

axis2 label=(f=swiss h=2.0)
      value=(f=swiss h=2.0)
      length = 5.5 in;

   symbol1 c=black V=circle H=2.0 i=none;
   symbol2 v=none w=3 l=1 i=splines;
   symbol3 v=none w=2 l=20 i=splines;

PROC GPLOT DATA=setall;
   PLOT r*x1 zero*x1 / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus X1";
       LABEL  y = "Residuals";
	   LABEL  x1 = 'Telephones per 1000 persons';
	   LABEL  x2 = 'Percent fat calories';
	   Label  x3 = 'Percent animal protein calories';
   RUN;

PROC GPLOT DATA=setall;
   PLOT r*x2 zero*x2 / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus X2";
       LABEL  y = "Residuals";
	   LABEL  x1 = 'Telephones per 1000 persons';
	   LABEL  x2 = 'Percent fat calories';
	   Label  x3 = 'Percent animal protein calories';
   RUN;

PROC GPLOT DATA=setall;
   PLOT r*x3 zero*x3 / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus X3";
       LABEL  y = "Residuals";
	   LABEL  x1 = 'Telephones per 1000 persons';
	   LABEL  x2 = 'Percent fat calories';
	   Label  x3 = 'Percent animal protein calories';
   RUN;

PROC GPLOT DATA=setall;
   PLOT r*yhat zero*yhat / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus Predicted Values";
       LABEL  r = "Residuals";
	   LABEL  yhat = 'Predicted Values';

   RUN;


PROC GPLOT DATA=setall;
   PLOT r*x3 zero*x3 / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus X3";
       LABEL  y = "Residuals";
	   LABEL  x1 = 'Telephones per 1000 persons';
	   LABEL  x2 = 'Percent fat calories';
	   Label  x3 = 'Percent animal protein calories';
   RUN;

PROC GPLOT DATA=setall;
   PLOT r*x12 zero*x12 / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus Predicted Values";
       LABEL  r = "Residuals";
	   LABEL  yhat = 'Predicted Values';

   RUN;


/*  Compute normal probability plot 
    for residuals */

PROC RANK DATA=setall NORMAL=BLOM OUT=setall;
  VAR r; RANKS q; run;

axis1 label=(f=swiss h=2.5)
   value=(f=swiss h=2.0)
   length= 5.5 in;

axis2 label=(f=swiss h=2.0)
      value=(f=swiss h=2.0)
      length = 5.5 in;

   symbol1 V=circle H=2.0 i=none;

PROC GPLOT DATA=setall;
   PLOT r*q /  vaxis=axis2 haxis=axis1;
TITLE  H=8.0 F=swiss ;
TITLE2  H=3.5 F=swiss ;
TITLE3  H=3.5 F=swiss "Normal Probability Plot for Residuals";
       LABEL q='Expected N(0,1) Values';
       LABEL  r = 'Residuals';
   RUN;


PROC UNIVARIATE DATA=setall NORMAL;
  VAR r;
  TITLE 'TEST OF NORMALITY';
  TITLE2 ' ';
  TITLE3 ' ';
  run;



proc reg data=set1 outest=parms covout;
  model y = x1 x2 x3 / partial;
  title ' ';
run;

proc reg data=set1 outest=parms covout;
  model y = x1 x2 x3 / vif ;
  plot y*(x1 x2 x3) / hplots=2 vplots=2;
  plot residual.*(x1 x2 x3 predicted. )/ hplots=1 vplots=4;
  run;

proc reg data=set1 outest=parms covout;
  model y = x1 x2 x3 / selection=rsquare cp aic bic start=0 stop=3 best=5;
  run;
