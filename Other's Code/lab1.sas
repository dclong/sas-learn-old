
/* This is the SAS code we developed in the
   first lab.  SAS has two main features: 
   (1) the DATA step that enters data and
   manipulate data files, and (2) the 
   procedures (like PROC UNIVARIATE) that
   compute summary statistics, fit models, 
   and create graphs and data displays.  */  


/* Make a data file called set1.  This is 
   a temporary data file that will be lost
   when the SAS session os termiated. 

   The inout ststement tells SAS to read
   two values from each line of data.  The
   names of the variables are trt and rain.
   A dollar sign is used after trt to 
   indicate that it is an alphanumeric 
   variable and not a numeric variable. 
   The list of data is inserted after the
   datalines keyword. Do not end a line of 
   data with a semicolon.  The first line 
   with a semicolon, after the datalines 
   keyword will terminate data entry, and 
   that line will not be included in 
   the data set that is created.*/

data set1;
  input trt $ rain;
  datalines;
  control 68.5
  control 47.3
  control 41.1
  control 36.6
  control 29.0
  control 28.6
  control 26.3
  control 26.1
  run;

  /* Use the PRINT procedure to print 
     the data file in the output window */

  proc print data=set1;
    var trt rain;
	run;


  /* Use the UNIVARIATE procedure to 
	 compute summary statistics such as
	 the sample mean, sample median,
	 sample quartiles, innerquartile range,
	 sample standard deviation, and 
	 standard error for the mean.  The "normal"
	 option specified in the proc statement
	 produces tests of the null hypothesis 
	 that the data were sampled from a population
	 with a normal distribution.*/

  proc univariate data=set1 normal;
    var rain;
	run;


 /* Construct a normal probability plot.  
	First use the rank proceudre to compute
	approximations to the expected values
	of the ordered sample of size n from
	a standard normal distribution. These
	values are saved to a new file called 
	"quantiles" along with the original 
	data set.*/

  proc rank data=set1 normal=blom out=quantiles;
    var rain;
	ranks normal;
	run;

	proc print data=quantiles;
	run;

/* Use the PLOT procedure to make a
   scatter plot. */

	proc plot data=quantiles;
	  plot rain*normal;  run;

/* Use the GPLO procedure to create a
   a plot with higher resolution and better
   labesl */

   goptions rotate=landscape targetdevice=pscolor
            hsize=8 in  vsize=6 in;

   axis1 color=tomato 
         label = (c=blue h=1.5 a=90 "Rainfall");
   axis2 color=tomato 
         label = (c=violet h=2.0 "Normal Quantiles");

   symbol1 c=steelblue v=dot i=none;

   proc gplot data=quantiles;
     plot rain*normal=1 / vaxis=axis1 haxis=axis2 frame;
	 title "Normal probability Plot" h=4.0;  run;

/* The previous code will open up a graphics
   containing the plot.  Use the edit button
   on the graphics widow to get into a graph
   editing window to make changes to the
   graph.  Then use the export option under the 
   file button to save the edited graph in
   a specific format.   */


/* Enter a second data set for rainfall
   from seeded clouds.  Here we put four 
   values per line and use the @@ option
   to properly read the data. A variable 
   called trt is also created. */

  data set2;
    input rain @@;
	trt='Seeded';
	datalines;
	255.0 242.5 200.7 198.6
	129.6 119.0 118.3 115.3
	run;

	proc print data=set2; run;

/* Combined the two data files.  Set2
	will be appended below set1.  */

	data set3; set set1 set2;  run;
	proc print data=set3; run;

/* Use the BY statement to make separate
	runs of the UNIVARIATE procedure for
	control clouds and seeded clouds. 
	First sort the data with respect to 
	the variable that indicated the control
	and seeded groups.*/

    proc sort data=set3; by trt; run;
	proc univariate data=set3 normal;
	  by trt;
	  var rain;
	  run;

/* Create side by side box plots  */

proc boxplot data=set3;
     plot rain*trt / boxstyle=schematicid;
	 title "Boxplots";
run;

proc boxplot data=set3;
     plot rain;
run;
/* Enter the larger set of data for the cloud
   seeding experiment.  These data are read
   from a file stored on  the C drive.  You 
   can copy data from the data folder on the
   course web page to a directory of your
   choice. Then use a data step to read the
   stired files and create a temporary SAS 
   data set called "set4".  There is no datalines
   statement when you read data from a file.*/ 


   data set4;
     infile "c:/stat500/data/cloudseeding.dat";
     input trt $ rain;
     run; 
     
