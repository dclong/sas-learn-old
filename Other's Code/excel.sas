/* various ways to read files from excel */

/* 1) store the file as .csv (comma delimited) */
/*    read using a data step  dsd option on infile crucial */
/*    may also need lrecl=XX  if many fields (replace XX by 512 or 1024) */
data datacsv;
  infile 'c:/philip/stat500/data/bacillus2.csv' dsd;
  input trt $ pre post;
run;

/* 2) store the file as .csv (comma delimited) */
/*    read using proc import */
proc import file='c:/philip/stat500/data/bacillus2.csv' out=fromcsv;
run;

/* 3) store the file as .xls (Excel workbook) */
/*    read using proc import */
proc import file='c:/philip/stat500/data/bacillus2.xls' out=fromxls;
run;

options formdlim='-';
proc print data=datacsv (obs=10);
  title 'First 10 obs of csv/data';
proc print data=fromcsv (obs=10);
  title 'First 10 obs of csv/import';
proc print data=fromxls (obs=10);
  title 'First 10 obs of xls/import';
run;
