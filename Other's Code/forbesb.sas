
/*  This program uses the Forbes data 
    considered in class to illustrate 
    the use of PROC REG in SAS to obtain
    least squares estimates for a simple 
    linear regression model.  It is posted
    as forbes.sas on the course web page */

/*  First enter the data */

DATA set1;
  INFILE 'c:/documents and settings/kkoehler.IASTATE/my documents/courses/st500/sas/forbes.dat';
  INPUT X P;
  Y = LOG(P);
  LABEL X = BOILING POINT (deg. F)
        P = ATM. PRESSURE (in. Hg)
        Y = LOG(ATM. PRESSURE);

/*  Print the data using specified column headings */

proc sort data=set1; by x;

PROC PRINT DATA=SET1 UNIFORM SPLIT='*';
  VAR X P Y;
  LABEL X = 'BOILING POINT*OF WATER*(degrees F)'
        P = 'ATMOSPHERIC*PRESSURE*(inches Hg)'
        Y = 'NATURAL LOG OF*ATMOSPHERIC*PRESSURE';
  TITLE 'FORBES DATA';
  run;

/* Plot the data */

goptions targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=2.5)
   ORDER = 190 to 215 by 5
   value=(f=swiss h=2.0)
   length= 6.5 in;

axis2 label=(f=swiss h=2.0)
      order = 3.0 to 3.5 by .1
      value=(f=swiss h=2.0)
      length = 4.0 in;

   symbol1 V=circle H=2.0 i=none;
 
PROC GPLOT DATA=set1;
   PLOT Y*x /  vaxis=axis2 haxis=axis1;
TITLE1  H=2.0 F=swiss ;
TITLE2  H=3.5 F=swiss "Forbes Data";
       LABEL x='Boiling point of water (degrees F)';
       LABEL  Y = 'Log Pressure';
   RUN;



/*  Compute least squares estimates of regression coefficients
    and output the residuals, and
    predicted values to a work file that can be used by other
    SAS procedures.  */

PROC REG DATA=SET1 ;
  MODEL Y = X / P CLM CLI;
  OUTPUT OUT=SET2 P=YHAT R=RESIDUAL STUDENT=STDRES
         STDP=STDP;
  run;


 /*  Add columns to the data file 
     corresponding to a horizontal line 
     at zero and the Scheffe bounds for 
     a 95% simultaneous confidence region 
     for the true regression line.  */

 data set2; set set2;
   yzero = 0;
   upper = yhat + sqrt(2*finv(.95,2,15))*stdp;
   lower = yhat - sqrt(2*finv(.95,2,15))*stdp;
   if(y = .) then delete;
   run;


/* Plot the fitted line with
   the observed data  */

goptions targetdevice=ps rotate=landscape colors=black cback=white;

axis1 label=(f=swiss h=2.5)
   ORDER = 190 to 215 by 5
   value=(f=swiss h=2.0)
   length= 6.5 in;

axis2 label=(f=swiss h=2.0)
      order = 3.0 to 3.5 by .1
      value=(f=swiss h=2.0)
      length = 4.0 in;

   symbol1 c=black V=circle H=2.0 i=none;
   symbol2 c=black v=none w=3 l=1 i=splines;

PROC GPLOT DATA=SET2;
   PLOT y*x yhat*x/ overlay vaxis=axis2 haxis=axis1;
TITLE1  H=2.0 F=swiss ;
TITLE2  H=3.5 F=swiss "Forbes Data";
       LABEL x='Boiling point of water (degrees F)';
       LABEL  Y = 'Log Pressure';
   RUN;


/*  Plot residuals against explantory variable
    to check for homogeneity of variance and
    patterns in the residuals that would 
    suggest that the model is inadequate  */

axis1 label=(f=swiss h=2.5)
   ORDER = 190 to 215 by 5
   value=(f=swiss h=2.0)
   length= 6.5 in;

axis2 label=(f=swiss h=2.0)
      order = -.01 to 0.04 by 0.01
      value=(f=swiss h=2.0)
      length = 4.0 in;

   symbol1 V=circle H=2.0 i=none;
   symbol2 v=none w=3 l=1 i=splines;
   symbol3 v=none w=2 l=20 i=splines;

PROC GPLOT DATA=SET2;
   PLOT residual*x yzero*x / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=2.0 F=swiss ;
TITLE2  H=3.5 F=swiss "Forbes Data Residuals";
       LABEL x='Boiling point of water (degrees F)';
       LABEL  Y = 'Log Pressure';
   RUN;

/*  Compute normal probability plot 
    for residuals */

PROC RANK DATA=set2 NORMAL=BLOM OUT=set2;
  VAR residual;
  RANKS q; run;

axis1 label=(f=swiss h=2.5)
   order = -3 to 3 by 1
   value=(f=swiss h=2.0)
   length= 6.5 in;

axis2 label=(f=swiss h=2.0)
      order = -0.01 to 0.04 by 0.01
      value=(f=swiss h=2.0)
      length = 4.0 in;

   symbol1 V=circle H=2.0 i=none;

PROC GPLOT DATA=SET2;
   PLOT residual*q /  vaxis=axis2 haxis=axis1;
TITLE1  H=2.0 F=swiss ;
TITLE2  H=3.5 F=swiss "Forbes Data Residuals";
       LABEL x='Boiling point of water (degrees F)';
       LABEL  Y = 'Log Pressure';
   RUN;


PROC UNIVARIATE DATA=set2 NORMAL;
  VAR residual;
  TITLE 'TEST OF NORMALITY';
  run;


/* Plot fitted line with Scheffe
   confidence bands  */

axis1 label=(f=swiss h=2.5)
   ORDER = 190 to 215 by 5
   value=(f=swiss h=2.0)
   length= 6.5 in;

axis2 label=(f=swiss h=2.0)
      order = 3.0 to 3.5 by .1
      value=(f=swiss h=2.0)
      length = 4.0 in;

   
   symbol1 v=none w=3 l=1 i=splines;
   symbol2 v=none w=2 l=20 i=splines;
   symbol3 v=none w=2 l=20 i=splines;

 PROC GPLOT DATA=SET2;
   PLOT yhat*x lower*x upper*x / 
         overlay vaxis=axis2 haxis=axis1;
TITLE1  H=2.0 F=swiss ;
TITLE2  H=3.2 F=swiss "Simultaneous 95% Confidence Bands";
TITLE3  h=3.2 F=SWISS "for the True Regression Line";
       LABEL x='Boiling point of water (degrees F)';
       LABEL  yhat = 'Log Pressure';
   RUN;

