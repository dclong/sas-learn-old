
/*  This program computes a paired t test for the
    rhesus monkey experiment considered in class */

/*  First enter the data and compute the differences */

data set1;
  input monkey y1 y2;
  d = y1-y2;
  label  y1 = CP level for severed nerve
         y2 = CP level for intact nerve
          d = difference (y1-y2);
cards;
 1 11.5 16.3
 2  3.6  4.8
 3 12.5 10.9
 4  6.3 14.2
 5 15.2 16.3
 6  8.1  9.9
 7 16.6 29.2
 8 13.2 22.4
run;


/* Print the data file */

proc print data=set1;
run;


/* Compute the t test and summary statistics for the differences */

proc univariate data=set1 normal plot;
  var d;
  run;


/* Make a better normal probability plot for the differences  */

proc rank data=set1 normal=blom out=set1;
  var d;
  ranks q;

proc plot data=set1;
  plot d*q='*' /hpos=60;
  run;

proc corr data=set1;
  var y1 y2;
  run;

/* Plot y1 against y2 */

proc plot data=set1;
  plot y1*y2 = '*' / hpos=60;
  run;



 goptions cback=white colors=(black) gunit=pct vpos=100 hpos=100
    targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=3.5)
      ORDER = 0 to 30 by 10
      value=(f=triplex h=3.0)
      length= 70 pct;

axis2 label=(f=swiss h=3.5)
      order = 0 to 20 by 10
      value=(f=swiss h=3.0)
      length = 60 pct;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=circle H=2.2 ;
       SYMBOL3 V=NONE  H=2 L=3 I=SPLINE;
       SYMBOL4 V=NONE  H=2 L=3 I=SPLINE;

   PROC GPLOT DATA=SET1;
      plot y1*y2 / vaxis=axis2 haxis=axis1;
TITLE1  H=5.0 F=swiss " ";
TITLE2  H=4.0 F=swiss "Creatine Phosphate Concentrations";
TITLE3  h=4.0 f=swiss "in Rhesus Monkeys";
       LABEL y1='Severed Nerves';
       LABEL y2 = 'Intack Nerves ';
   RUN;
