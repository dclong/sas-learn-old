/* the following options line saves paper - */
/*     SAS puts information together to print fewer pages */
options formdlim = '-';

/* read the sparrow data set */
/*   the second column is not numeric. 
/*   The $ tells SAS to read the PRECEDING variable as a character variable */

data sparrow;
 infile 'sparrow.txt';
 input humerus fate $;

 if humerus ne .;             /* omit the first observation (header line) */
    
 lhumerus =  log(humerus);    /* log() is the natural log */
 
 run;

/* proc ttest does 2 sample t tests */
/*  assuming equal variances */
/*  and the welch-satterthwaite unequal variance test */
/*  the class variable can have only two levels.  */
/*     missing is treated as a valid level.  */
/*     If the data set has a header line, you need to delete the first */
/*     observation to get ttest to run */

proc ttest;
  class fate;
  var humerus lhumerus;
  title 'T-test of humerus and log transformed length';
run;

