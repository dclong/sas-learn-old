
/*  This is a SAS/IML program to compute
    confidence intervals for a binomial
    sucess rate.  The program is stored 
    in the file

                 binci.sas           */


options nonumber nodate linesize=64;

proc iml;
  start binci;

  x = 1;       * Enter number of successes;
  n = 10;       * Enter total number of trials;
  a = .95;       * Enter confidence level;

  a2=1-((1-a)/2);
  v1=x;
  v2=n-x+1;
  v3=v1+1;
  v4=v2-1;
  invb1=1;
  if(v1 > 0) then invb1 = betainv(a2,v2,v1);
  invb2=1;
  if(v4 > 0) then invb2 = betainv(a2,v3,v4);
  pl= 1-invb1;
  pu = invb2;
  print 'Exact confidence intervals';
  print pl pu;

  z = probit(a2);
  p = v1/n;
  pl = p - z*sqrt(p*(1-p)/n);
  pu = p + z*sqrt(p*(1-p)/n);
  print 'Confidence intervals based on the',
     'large sample normal approximation';
  print pl pu;
finish;

run binci;
