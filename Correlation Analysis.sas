*******************Chuanlong Du*********************************;
/*
Functions:
This program test the null hypthesis that the correlation r equals 0
Parameters:
Parameter r is the correlation
Parameter n is the number of observations
*/
proc iml;
******************Body of the Function***************************;
     start CorrAnal(r,n);
     Tstat=r*sqrt(n-2)/sqrt(1-r**2);
	 df=n-2;
	 Pvalue=(1-probt(abs(Tstat),df))*2;
	 title f=arial c=red h=6 "Correlation Analysis";
	 print Tstat df Pvalue;
	 finish;
*******************Initial the Parameters**************************;
     r=0.942;
	 n=13;
********************Run the Function*******************************;
	 run CorrAnal(r,n);
quit;

