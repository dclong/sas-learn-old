/*
This program calculate the orthogonal polynomial contrasts
*/

proc iml;
 /* unequally spaced and unbalanced factor levels */ 
   levels = { 
   1,1,1,1,1,1,1, 
   4,4, 
   6,6,6, 
   10,10,10,10}; 
    
   /* data for y. Make sure the data is sorted 
      according to the factor levels */ 
   y = { 
   2.804823, 0.920085, 1.396577, -0.083318, 
   3.238294, 0.375768, 1.513658,            /* level 1 */ 
   3.913391, 3.405821,                      /* level 4 */ 
   6.031891, 5.262201,  5.749861,           /* level 6 */ 
   10.685005, 9.195842, 9.255719,  9.204497 /* level 10 */ 
   }; 
 
   a      = {1,4,6,10}; /* spacing */ 
   trials = {7,2,3,4};  /* sample sizes */ 
   maxDegree = 3; /* model with Intercept,a,a##2,a##3 */ 
 
   P = orpol(a,maxDegree,trials); 
 
   /* Test linear hypotheses: 
      How much variation is explained by the 
      i_th polynomial component after components 
      0..(i-1) have been taken into account? */ 
       
   /* the columns of L are the coefficients of the  
      orthogonal polynomial contrasts */ 
   L = diag(trials)*P; 
 
   /* form design matrix */ 
   x = design(levels); 
    
   /* compute b, the estimated parameters of the 
      linear model. b will be the mean of the y's 
      at each level. 
      b = ginv(x'*x) * x` * y 
      but since x is the output from DESIGN, then 
      x`*x = diag(trials) and so 
      ginv(x`*x) = diag(1/trials) */ 
   b = diag(1/trials)*x`*y; 
 
   /* (L`*b)[i] is the best linear unbiased estimated  
      (BLUE) of the corresponding orthogonal polynomial  
      contrast */ 
   blue = L`*b; 
 
   /* the variance of (L`*b) is  
      var(L`*b) = L`*ginv(x`*x)*L 
        =[P`*diag(trials)]*diag(1/trials)*[diag(trials)*P] 
        = P`*diag(trials)*P 
        = Identity         (by definition of P) 
       
      so therefore the standardized square of  
      (L`*b) is computed as  
      SS1[i] = (blue[i]*blue[i])/var(L`*b)[i,i])  
             = (blue[i])##2                            */ 
    
   SS1 = blue # blue;  
   rowNames = {'Intercept' 'Linear' 'Quadratic' 'Cubic'}; 
   print SS1[rowname=rowNames format=11.7 label="Type I SS"];
quit;
