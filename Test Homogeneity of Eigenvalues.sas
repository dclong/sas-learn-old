******************************Chuanlong Du****************************************;
/*
Functions:
This program test the homogeneity of the last several eigen values
Parameters:
Parameter corr is the correlation (covariance) matrix
Parameter n is the number of observations
Parameter r is the number of eigenvalues that are equal to each other
*/
proc iml;
********************Parameters Part***************************************;
*****correlation or covariance matrix is need;
     use corr;
	 read all into corr;
	 n=54;*number of observations;
	 r=2;*the number of eigenvalues that have the same value;
**********************Body Part*******************************************;
	 p=nrow(corr);*number of traits;
	 m=p-r;
	 title f=arial c=red h=6 "Test Homogeneity of Eigenvalues";
	 values=j(p,1,0);
	 vectors=j(p,p,0);
	 call eigen(values,vectors,corr);
	 print "The eigenvalues and eigenvectors of the correlation (covariance) matrix are:";
	 print values vectors;
	 values1=values[1:m,1];
	 values2=values[m+1:p,1];
	 LambdaMean=sum(values2)/r;
	 C=LambdaMean##2*sum((values1-LambdaMean)##(-2));
	 C=C+n-m-(2*r+1+2/r)/6;
	 C=-C;
	 Q=exp(sum(log(values2)))/LambdaMean##r;
	 ChisqStat=C*log(Q);
	 df=r*(r+1)/2-1;
	 Pvalue=1-probchi(ChisqStat,df);
	 print "The information of the approximate Chisquare test for homogeneity of eigenvalues is:";
	 print ChisqStat df Pvalue;
quit;
