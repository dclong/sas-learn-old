proc univariate data=set2;
     var residual;
	 qqplot;
run;

proc univariate data=set2;
     probplot residual/normal(mu=0 sigma=1 color=red);
run;
