data male;
     set set1;
	 where sex eq 1;
	 drop sex;
run;

proc print data=male;
run;

data female;
     set set1;
	 where sex eq 2;
	 drop sex;
run;

proc print data=female;
run;

proc iml;
********Define the Covariance Matrix Function********************************;
     start Cov(X);
     SumVector=X[+,];
	 N=nrow(X);
	 P=ncol(X);
	 MeanVector=SumVector/N;
	 Xcent=X-shape(MeanVector,N,P);
	 A=Xcent`*Xcent;
	 return(A/(N-1));
	 finish;
********Use the function above to calculate pooled covariance matrix**********;
     use male;
     read all into Xm;
     Sm=Cov(Xm);
	 Nm=nrow(Xm);
	 use female;
	 read all into Xf;
	 Sf=Cov(Xf);
	 Nf=nrow(Xf);
	 Spool=(Sm*(Nm-1)+Sf*(Nf-1))/(Nm+Nf-2);
	 print Spool;
quit;
	 


 
