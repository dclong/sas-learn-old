***************************Chuanlong Du************************************;
/*
This program deals with one sample with normal distribution.
1. Do the one-sample Hotelling Tsquare (F) test H0: C*Mu=d
2. Print information about the confidence ellipse
3. Construct simutaneous confidence intervals using Tsquare method
4. Construct simutaneous confidence intervals using Bonferroni method 
Parameters:
Parameter X is the observations matrix
Parameter mu is the mean vector in the null hypothesis
Parameter alpha is the significant level
Parameter C is a full row rank matrix used for H0:C*mu=d
Parameter d is a vector used for H0: C*mu=d
Parameter ConMat is used to construct confidence intervals,and it don't need 
to be full row rank
Waring: 
when you use this program, you should be aware of that the confidence 
ellipse is for C*Mu while the simutaneous confidence intervals are for ConMat*Mu. 
They are different!!! But When C is an identical matrix, ConMat*Mu is base on 
the confidence ellipse of Mu. 
*/
proc iml;
********************Parameters Part I**************************************;
     *Mu0 must be a row vector;
     use set3;
	 read all into X;
	 Mu0={500 50 30};
	 alpha=0.05;
	 C={};
	 d=C*Mu0`;
	 ConMat={};
********************Parameters Part II**************************************;
	 /*If X is given, there's no need to intial n, p and S. 
	 However, if X is not given, these parameters should be intialed*/
	 *calculate some necessary statistics;
	 n=nrow(x);
	 p=nrow(C);
	 sum=X[+,];
	 MeanVec=sum/n;
	 Xcentered=X-repeat(MeanVec,n,1);
	 A=Xcentered`*Xcentered;
	 S=A/(n-1);
***************************************************************************;
	 *do the Hotelling Tsquare (F) test for H0: ConMat*mu=d;
	 Snew=C*S*C`;
	 HTsquare=n*(C*MeanVec`-d)`*inv(Snew)*(C*MeanVec`-d);
	 df1=p;
	 df2=n-p;
	 HotellingF=HTsquare*df2/p/(n-1);
	 Pvalue=1-probf(HotellingF,df1,df2)
	 title f=arial c=red h=6 "One Sample with Normal Distribution";
	 print HTsquare HotllingF df1 df2 Pvalue;
****************************************************************************;
     *calculate the lengths and directions of the axes of the confidence ellipse*;
	 Svalues=j(1,p);*a vector used to store the eigen values;
	 Svectors=j(p,p);*a matrix used to store the eigen vectors;
	 call eigen(Svalues,Svectors,Snew);
	 constant=sqrt(p*(n-1)/n/(n-p)*finv(1-alpha,p,n-p));
	 AxisLen=sqrt(Svalues)*constant;
	 print "The length of the axes of the ellipse and their directions are:";
	 print AxisLen Svectors;
*****************************************************************************;
	 *construct the confidence region for the mean vector;
	 print "The confidence region (ellipse) for the C*Mu is:";
	 print "n(C*Mu-C*Xbar)'(C*S*C`)^(-1)(C*Mu-C*Xbar)<=" HTsquare;
*****************************************************************************;
	 p=ncol(X);
	 constant=sqrt(p*(n-1)/n/(n-p)*finv(1-alpha,p,n-p));
	 *construct simutaneous confidence intervals using Tsquare method;
	 **************Body of Function*********************************;
	 start Tconf(a);
	 Axbar=a*MeanVec`;
	 Width=constant*sqrt(a*S*a`);
	 UpperLimit=Axbar+Width;
	 LowerLimit=Axbar-width;
	 print "The Tsquare confidence interval for a'mu is:";
	 print Axbar LowerLimit UpperLimit;
	 finish;
	 *use the above function to construct Tsquare simutaneous intervals;
	 m=nrow(ConMat);
	 do i=1 to m;
	    run Tconf(ConMat[i,]);
	 end;
******************************************************************************;
	 *construct simutaneous confidence intervals using Bonferroni method;
	 start BonConf(a);
	 Axbar=a*MeanVec`;
	 Width=tinv(1-alpha/m/2,n-1)*sqrt(a*S*a`/n);
	 UpperLimit=Axbar+Width;
	 LowerLimit=Axbar-width;
	 print "The Bonferroni confidence interval is:";
	 print Axbar LowerLimit UpperLimit;
	 finish;
	 *use the above function to construct Bonferroni simutaneous intervals;
	 do i=1 to m;
	    run BonConf(ConMat[i,]);
	 end;
quit;
