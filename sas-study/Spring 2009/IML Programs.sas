
PROC IML;
     START NORMAL;
     USE newnotes;          *ENTER THE DATA;
     READ ALL  INTO X;
     N=NROW(X);             *NUMBER OF OBSERVATIONS IS N;
P=NCOL(X);             *COMPUTE NUMBER OF TRAITS = P;
SUM=X[+, ];            *COMPUTE TOTAL FOR EACH TRAIT;
A=X`*X-SUM`*SUM/N;     *COMPUTE CORRECTED CROSSPRODUCTS MATRIX;
S=A/(N-1);             *COMPUTE SAMPLE COVARIANCE MATRIX;
XBAR=SUM/N;            *COMPUTE TRANSPOSE OF SAMPLE MEAN VECTOR;
SCALE=INV(SQRT(DIAG(A)));
R=SCALE*A*SCALE;       *COMPUTE SAMPLE CORRELATION MATRIX;

PRINT,,,,," THE SAMPLE MEAN VECTOR";
PRINT XBAR;
PRINT,,,,," THE SAMPLE CORRELATION MATRIX";
PRINT R;

                       * Compute a chi-square probability plot;

E=X-(J(N,1)*XBAR);     *MATRIX OF DEVIATIONS FROM MEAN VECTOR;
D=VECDIAG(E*INV(S)*E`);    *SQUARED MAHALANOBIS DISTANCES;
RD = RANK(D);            * COMPUTE RANKS FOR THE D VALUES;
RD =(RD-.5)/N;
PD2=P/2;
Q=2*GAMINV(RD,PD2);  

/* COMPUTE CHI-SQUARE PLOTTING POSITIONS */
DD=D||Q;
CREATE CHISQ FROM DD ;   

/* OPEN A FILE TO TEMPORARILY STORE RESULTS */
APPEND FROM DD;

/* Compute the p-value for a correlation test
   of multivariate normality, using the points on 
   chi-square probability plot */

rpn = t(D)*Q-(sum(D)*sum(Q))/N;
rpn = rpn/sqrt((ssq(D)-(sum(D)**2)/N)*(ssq(Q)-(sum(Q)**2)/N));

/* Simultate the p-value for the 
   correlation test  */

ns=10000;
pvalue=0;
do i=1 to ns;
  do j1=1 to n;
  do j2=1 to p;
    x[j1,j2] = rannor(-100);
    end;
    end;

   SUMX=X[+, ];           
   A=t(X)*X-t(SUMX)*SUMX/N;     
   S=A/(N-1);             
   XBAR=SUMX/N;           
   E=X-(J(N,1)*XBAR);   
   DN=VECDIAG(E*INV(S)*t(E));   
   RN = RANK(DN);            
   RN =(RN-.5)/N;
   PD2=P/2;
   QN=2*GAMINV(RN,PD2);  
   rpnn = t(DN)*QN-(sum(DN)*sum(QN))/N;
   rpnn = rpnn/sqrt((ssq(DN)-(sum(DN)**2)/N)*(ssq(QN)-(sum(QN)**2)/N));
   if(rpn>rpnn) then pvalue=pvalue+1;   
   end;
   pvalue=pvalue/ns;

print,,,,,"Correlation Test of Normality";
print N P rpn pvalue;

FINISH;
RUN NORMAL;

PROC IML;
     START NORMAL;
     USE set2;            /* Enter the data */
     READ ALL  INTO X;
     N=NROW(X);             /* Number of observations is N */
     P=NCOL(X);             /* Number of traits is p */
     SUM=X[+, ];            /* Total for each trait */
     A=X`*X-SUM`*SUM/N;     /* Corrected crossproducts matrix */
     S=A/(N-1);             /* Sample covariance matrix */
     XBAR=SUM/N;            /* Sample mean vector */
     SCALE=INV(SQRT(DIAG(A)));
     R=SCALE*A*SCALE;       /* Sample correlation matrix */
     PTR=J(P,P); /* 1 matrix with dimension P*P*/
     TR=J(P,P);
     DF=N-2;
     DO I1=1 TO P;          /* T-tests for correlations */
         IP1=I1+1;
         DO I2=IP1 TO P;
               TR[I1,I2]=SQRT(N-2)#R[I1,I2]/SQRT(1-R[I1,I2]##2);
               TR[I2,I1]=TR[I1,I2];
               PTR[I1,I2]=(1-PROBT(ABS(TR[I1,I2]),DF))*2;
               PTR[I2,I1]=PTR[I1,I2];
         END;
     END;
     RINV=INV(R);           /* Partial Correlations */
     SCALER=INV(SQRT(DIAG(RINV)));
     PCORR=-SCALER*RINV*SCALER;
     DO I = 1 TO P;
          PCORR[I,I]=1.0;
     END;
     PTPCORR=J(P,P);
TPCORR=J(P,P);
DF=N-P;
DO I1=1 TO P;  /* T-tests for partial correlations */
IP1=I1+1;
  DO I2=IP1 TO P;
    TPCORR[I1,I2]=SQRT(N-P)#PCORR[I1,I2]/
                  SQRT(1-PCORR[I1,I2]##2);
    TPCORR[I2,I1]=TPCORR[I1,I2];
    PTPCORR[I1,I2]=
        (1-PROBT(ABS(TPCORR[I1,I2]),DF))*2;
    PTPCORR[I2,I1]=PTPCORR[I1,I2];
  END;
END;

PRINT,,,,," THE SAMPLE MEAN VECTOR";
PRINT XBAR;
PRINT,,,,," THE SAMPLE CORRELATION MATRIX";
PRINT R;
PRINT,,,,," VALUES OF T-TESTS FOR ZERO CORRELATIONS";
PRINT TR;
PRINT,,,,," P-VALUES FOR THE T-TESTS FOR ZERO CORRELATIONS";
PRINT PTR;
PRINT,,,,," THE MATRIX OF PARTIAL CORRELATIONS CONTROLLING FOR";
PRINT "       ALL VARIABLES NOT IN THE PAIR";
PRINT PCORR;
PRINT,,,,," VALUES OF T-TESTS FOR PARTIAL CORRELATIONS";
PRINT TPCORR;
PRINT,,,,," P-VALUES FOR T-TESTS FOR PARTIAL CORRELATIONS";
PRINT PTPCORR;

/* Compute plotting positions
   for a chi-square probability plot */

E=X-(J(N,1)*XBAR);       
D=VECDIAG(E*INV(S)*E`);  /* Squared Mah. distances */
RD = RANK(D);            /* Compute ranks  */
  RD=(RD-.5)/N;
PD2=P/2;
  Q=2*GAMINV(RD,PD2);    /* Plotting positions */
  DQ=D||Q;
  print DQ;
CREATE CHISQ FROM DQ ;   /* Open a file to store results */
APPEND FROM DQ;

                         /* Compute test statistic */
rpn = t(D)*Q - (sum(D)*sum(Q))/N;
rpn = rpn/sqrt((ssq(D)-(sum(D)**2)/N)
         *(ssq(Q)-(sum(Q)**2)/N));

/* Simultate a p-value for the correlation test  */

ns=10000;
pvalue=0;
do i=1 to ns;
  do j1=1 to n;
  do j2=1 to p;
    x[j1,j2] = rannor(-100);
    end;
    end;

   SUMX=X[+, ];           
   A=t(X)*X-t(SUMX)*SUMX/N;     
   S=A/(N-1);             
   XBAR=SUMX/N;           
   E=X-(J(N,1)*XBAR);   
   DN=VECDIAG(E*INV(S)*t(E));   
   RN = RANK(DN);            
   RN =(RN-.5)/N;
   PD2=P/2;
   QN=2*GAMINV(RN,PD2);  
   rpnn = t(DN)*QN-(sum(DN)*sum(QN))/N;
   rpnn = rpnn/sqrt((ssq(DN)-(sum(DN)**2)/N)
           *(ssq(QN)-(sum(QN)**2)/N));
   if(rpn>rpnn) then pvalue=pvalue+1;   
   end;
   pvalue=pvalue/ns;

print,,,,,"Correlation Test of Normality";
print N P rpn pvalue;

FINISH;

RUN NORMAL;

PROC IML;
	START LOGIST;
                                 * Set cutoff value.  If predicted;
                                 * probability exceeds cutoff value;
                                 * then observation is classified;
                                 * into group 0, otherwise it is;
                                 * classified into group 1;
  	CUT = 0.4;
                                 *Identify the input data set;
  	USE SET2;
                                 * Put the data into the matrix Y ;
                                 * It is assumed that the group/response;
                                 * is coded as a binary 0-1 variable in the;
                                 * first column.  The;
                                 * remaining columns contain the data ;
                                 * for the explanatory variables ;
                                 * A column of ones is added by this program ;
                                 * to provide an intercept term;
                                 * The logistic regression model is for the;
                                 * log-odds of membership in the group ;
                                 * coded 0 versus the group coded 1;
  	READ ALL INTO Y;
                                 * Compute the total number of observations;
  	N=NROW(Y);
  	NN=N;
  	K=NCOL(Y);
                                 * Put the columns that indicate the;
                                 * group/response into the matrix S;
  	S=J(N,1)-Y[ ,1];
                                 * Put the model matrix into A;
  	A=Y[ ,2:K];
  	A=J(N,1)||A;
  	AA=A;
                                   * Classification decisions are;
                                   * stored in yhat;
  	SS=S;
  	YHAT=S;
                                  * B contains initial parameter estimates;
 	USE SETP;
 	READ ALL INTO B;
  	BT=B;
  	PRINT,,," MODEL MATRIX IS" A ;
  	PRINT,,,"INITIAL PARAMTER VALUES" BT;
  	MAXIT=50;
  	HALVING=16;
  	CONVERGE=.000001;
                                   * Start iterations for jackknife;
                                   * estimation of misclassification;
  	N=NN-1;
  	B=BT`;
  	C=I(NN);
  	C=C[2:NN, ];
  	A=C*AA;
  	S=C*SS;
  	RUN NEWTON;
  	XA=AA[1, ];
  	XAB=(XA*B)><J(N,1,50);
  	XAB=XAB<>J(N,1,-50);
  	P = EXP(XAB);
  	P = P/(1+P);
  	IF(P > CUT) THEN 
		YHAT[1,1]=0; 
	ELSE 
		YHAT[1,1]=1;
  	NM=NN-1;
  	DO IJ = 2 TO NM;
    	IJM1=IJ-1;
    	IJP1=IJ+1;
    	CC=I(NN);
    	C=CC[1:IJM1, ]//CC[IJP1:NN, ];
    	A=C*AA;
    	S=C*SS;
    	B=BT`;
    	RUN NEWTON;
    	XA=AA[IJ, ];
    	XAB=(XA*B)><J(N,1,50);
    	XAB=XAB<>J(N,1,-50);
    	P = EXP(XAB);
    	P = P/(1+P);
    	IF(P > CUT) THEN 
			YHAT[IJ,1]=1; 
		ELSE 
			YHAT[IJ,1]=0;
  	END;
  	N=NN-1;
  	B=BT`;
  	C=I(NN);
  	C=C[1:N, ];
  	A=C*AA;
  	S=C*SS;
  	RUN NEWTON;
  	XA=AA[NN, ];
    XAB=(XA*B)><J(N,1,50);
    XAB=XAB<>J(N,1,-50);
  	P = EXP(XAB);
  	P = P/(1+P);
  	IF(P > CUT) THEN 
		YHAT[NN,1]=0; 
	ELSE 
		YHAT[NN,1]=1;
                                   * Output results to SET2;
  	Y = YHAT||SS||AA;
  	CREATE SET3 FROM Y;
  	APPEND FROM Y;
  	CLOSE SET3;
 	FINISH;

* -------------- Modified Newton-Raphson algortihm --------------------------;
*                                                                            ;
* User must supply initial values for parameters in B, and the FUN and       ;
* DERIV functions.  FUN evaluates the function to be maximized at the        ;
* current value of  B.  DERIV evaluates first and second partial             ;
* derivatives.  First partial derivatives are put int Q.  The       ;
* The negative of the matrix of second partial derivatives is put   ;
* into H                                                            ;
*----------------------------------------------------------------------------;
	START NEWTON;
  	CHECK=1;
  	DO ITER = 1 TO MAXIT WHILE(INT(CHECK/CONVERGE));
  		RUN FUN;
     	FOLD=F;
     	BOLD=B;
        B2=BOLD;
     	RUN DERIV;
     	HI=INV(H);
     	B=BOLD+HI*Q;
     	RUN FUN;
     	DO HITER = 1 TO HALVING WHILE(FOLD-F >= 0);
        	B=B2+2.*((.5)##HITER)*HI*Q;
        	RUN FUN;
        	B2=B;
     	END;
   		CHECK=((BOLD-B)`*(BOLD-B))/(B`*B);
   	END;
	FINISH;
*---------------USER SUPPLIED FUNCTION EVALUATION----------------------------
;
	START FUN;
  		RUN PROB;
  		F=S`*LOG(PP) +  (J(N,1)-S)`*LOG(J(N,1)-PP);
	FINISH;
*-------------COMPUTATION OF PREDICTED PROBABILITIES------------------------;
*                                                                           ;
	START PROB;
  	AB = A*B;
  	ABT= AB><J(N,1,20);
  	ABT= ABT<>J(N,1,-20);
  	PP = EXP(ABT)/(J(N,1)+EXP(ABT));
	FINISH;
*-------------USER SUPPLIED DERIVATIVE EVALUATION ---------------------------;
*                                                                            ;
* First partial derivatives are returned in Q and the negative of the        ;
* second partial derivatives are returned in the matrix  H.                  ;
*----------------------------------------------------------------------------;
	START DERIV;
  		RUN PROB;
  		Q = A`*(S-PP);
  		M = DIAG(PP)-PP*PP`;
  		H = A`*DIAG(M)*A;
	FINISH;

	RUN LOGIST;
quit;



/*  The IML procedure in SAS can perform 
    basic matrix computations as well as 
    a variety of other functions 

    We will use the "start" command to create
    a module named "new".  This module is 
    executed with the statement  "run new:" */ 


PROC IML;
	START new;
 
/* A matrix can be entered directly using
   the { } brackets.  A comma is used to 
   end each row in the matrix.  The following
   enters the 5x2 matrix on page 21 of the 
   notes.  Note that any command must be 
   ended with a semicolon. */

	X={42 4, 52 5, 88 7, 58 4, 60 5};

/* This matrix is printed with the 
   following command  */

	print X;

/* Compute the number of rows in X with the
   nrow function and compute the numbers of 
   columns with the ncol function.  */

	n=nrow(X);
	p=ncol(X);	
	print ,, "number of rows: " n;
	print ,, "number of columns: " p;

/* Compute column sums.  Square brackets
   are used to select rows or columns of
   a matrix.  The plus sign below is used
   to sum across rows. */

	csum=X[+, ];    

/* Compute a row vector of sample means */

	means=csum/n;

	print ,,, n,,, csum,,, means;

/*  Compute the sample covariance matrix
    on page 22 of the notes.  This requires
    matrix multiplication denoted by the *
    operator and the transpose of a matrix
    obtained with the t( ) function  */

	S=(X`*X-csum`*csum/n)/(n-1);    

/* Compute the correlation matrix on page 22 
   The DIAG( ) function creates a diagonal matrix.
   The SQRT( ) function computes the square root
               of each element in a matrix 
   The INV( ) function computes the inverse of a 
               matrix */

	scale=INV(SQRT(DIAG(S)));
	R=scale*S*scale;     
	print ,,, S,,, scale,,, R;

/* Define the vectors on page 49 and
   compute transpose, multiply by a 
   scalar, and add two vectors  */

	x={2, 1, -4};
	y={5, -2, 0};
	xt = t(x);
	w=6*x;
	z=x+y;
	d=x-y;
	print ,, x y xt w z d;

/* sum of squares and length (page 52)  */

	x={2, 1, -4, -2};
	ssx=ssq(x); 
	lengthx = sqrt(ssq(x));
	print x ssx lengthx;

/* inner product and cos of angle */

	x={2, 1, -4};
	y={5, -2, 0};
	Lx=sqrt(t(x)*x);
	Ly=sqrt(sum(y#y));
	xy=sum(x#y);
	cos=xy/(Lx*Ly);
	print x y Lx Ly xy cos;

/* examples on page 62 */

	X={2 1 -4, 5 7 0};
	XT = t(x);
	Z=6*X;
	W={2 -1, 0 3}+{2 1, 5 7};
	print X XT Z W;

/* matrix multiplication (page 64) */

	A={2 0 1, 5 1 3};
	B={1 4, -1 3, 0 2};
	AB = A*B;
	C={2 1 , 5 3};
	D={1 4, -1 3};
	CD=C*D;
	DC=D*C;

/* Elementwise multiplication uses the
   "#" operator  */

	W=C#D;

	print ,, A B AB  ,, C D CD DC W;

/* Create identity matrices (page 65) */

	I3 = I(3);
	D = I(2);

	print D I3;

/* create matrices in which all
   entries are the same  */

	A=J(2,3);
	B=J(2,3,0);
	C=J(4,2,-3);

	print ,,A B C;

/* Inverse matrix */

	A ={9 2, 2 4};
	AINV = inv(A);
	W=A*AINV;

	B= { 6 3 1, 3 9 2, 1 2 5};
	BINV = inv(B);

	print ,, A AINV W ,, B BINV ;

/* determinant of a matrix */

	DA = det(A);
	DB = det(B);

	print ,,"determinants: " DA DB;

/* Compute eigenvalues and eigenvectors.  In
   the following call to the eigen function to
   find the eigenvalues and eigenvectors of A,
   M names a diagonal matrix of eigenvalues and
   E names a matrix with eigenvectors as columns */

	A= { 6 3 1, 3 9 2, 1 2 5};
	call eigen(M,E,A);

	print ,,A M E;

	/* Show that E is an orthogonal matrix */

	ETE=t(E)*E;
	EET=E*t(E);

	print ,, ETE EET;

	/* Compute the inverse of A, a square root matrix,
	and an inverse square root matrix */

	Ainv= E*INV(diag(M))*t(E);
	Asqrt= E*sqrt(diag(M))*t(E);
	Asqrtinv = E*inv(sqrt(diag(M)))*t(E);

	print ,, Ainv Asqrt Asqrtinv;

/* Compute trace and determinant of A */

	TraceA=trace(A);
	DetA=det(A);

	print ,, TraceA DetA;

/* Compute covariance matrix for conditional
   normal distribution (pages 126-127)  */

	S={4 0 1 3, 0 4 1 1, 1 1 3 1, 3 1 1 9};
	r1 = {1 3};
	r2 = {2 4};
	S11=S[r1,r1];
	S22=S[r2, r2];
	S12=S[r1, r2];
	S21=S[r2, r1];
	V = S11-S12*inv(S22)*S21;

	print ,,, S ,,, S11 S22 S12 S21 ,,, V;

/* The "FINISH" statement ends the module */

	finish;

/* Now execute the module */
	run new;
quit;


************************************Chuanlong Du****************************************;
/*
Functions:
*This program test the null hypothesis that the correlation matrix is compound symmetric;
Parameters:
Parameter corr is the correlation matrix (covariance) matrix
Parameter n is the number of observations
*/
proc iml;
*******************Parameters Part******************************************************;
     use corr;
	 read all into corr;
	 n=33;*numbers of observations;
******************Body Part of Functions************************************************;
	 p=nrow(corr);
	 SumVector=corr[+,];
	 MeanVector=(SumVector-1)/(p-1);
	 OverallMean=(sum(corr)-p)/p/(p-1);
	 GammaHat=(p-1)##2*(1-(1-OverallMean)##2)/(p-(p-2)*(1-OverallMean)##2);
	 ChisqStat=(sum((corr-OverallMean)##2)-p*(1-OverallMean)##2)/2;
	 ChisqStat=ChisqStat-GammaHat*sum((MeanVector-OverallMean)##2);
	 ChisqStat=(n-1)/(1-OverallMean)##2*ChisqStat;
	 df=(p+1)*(p-2)/2;
	 Pvalue=1-probchi(ChisqStat,df);
	 title f=arial c=red h=6 "Test Compound Symmetric Structure of Correlation (Covariance) Matrix";
	 print "The value of the approximate chisquare statistic and its p-value are";
	 print ChisqStat df Pvalue;
quit;
