
goptions cback=white colors=(black) 
    targetdevice=ps rotate=portrait;

axis1 label=(f=swiss h=3.5 )
      ORDER = 0 to 100 by 20
      value=(f=triplex h=3.0)
      length= 5 in;

axis2 label=(f=swiss h=3.5 r=0 a=90)
      order = 0 to 100 by 20
      value=(f=swiss h=3.0)
      length = 5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=square H=2.2 ;
	   symbol3 V=dot h=2.2;


PROC GPLOT DATA=set1;
      PLOT x2*x1=group / vaxis=axis2 haxis=axis1;
      TITLE1  H=3.5 F=swiss "Plot of Aptitude Scores Against Mathematics Scores";
      LABEL x1='Aptitude ';
      LABEL x2 = 'Mathematics';
RUN;

goptions colors = (black black blue green);

goptions device=WIN target=ps rotate=portrait;

/* Specify features of the plot */

axis1 label=(f=swiss h=2.5  
      "Standard Normal Quantiles")
      ORDER = -2.5 to 2.5 by .5
      value=(f=triplex h=1.6)
      length= 5.5in;

axis2 label=(f=swiss h=2.5 a=90 r=0
       "Ordered Data ")
      value=(f=triplex h=2.0)
      length = 5.0in;

  SYMBOL1 V=CIRCLE H=2.0 ;

PROC GPLOT DATA=set1;
     PLOT X1*Q1 / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
     TITLE2  H=2.3 F=swiss "X1";
     footnote ls=1.0in;
RUN;

PROC GPLOT DATA=set1;
     PLOT X2*Q2 / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
     TITLE2  H=2.3 F=swiss "X2";
     footnote ls=1.0in;
RUN;

PROC GPLOT DATA=set1;
     PLOT X3*Q3 / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
     TITLE2  H=2.3 F=swiss "X3";
     footnote ls=1.0in;
RUN;

PROC GPLOT DATA=set1;
     PLOT X4*Q4 / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
     TITLE2  H=2.3 F=swiss "X4";
     footnote ls=1.0in;
RUN;

goptions cback=white colors=(black) device=WIN target=ps;

  axis1 label=(h=2.5 r=0 a=90 f=swiss)
        length = 5 in
        value=(h=0.25in f=swiss)
        color = black width=4.0;

  axis2 label = (h=2.5 f=swiss 'Standard Normal quantiles')
        value=(h=0.25in f=swiss)
        length = 5.5 in
        color=black width=4.0;

  symbol1 v=circle h=2 c=black width=2;

proc gplot data=bnotes; 
     PLOT X1*Q1 / vaxis=axis1 haxis=axis2;
     PLOT X2*Q2 / vaxis=axis1 haxis=axis2;
     PLOT X3*Q3 / vaxis=axis1 haxis=axis2;
     PLOT X4*Q4 / vaxis=axis1 haxis=axis2; 
     PLOT X6*Q6 / vaxis=axis1 haxis=axis2;  
     title ls=2.0in h=4 f=swiss  'Normal Q-Q Plot';
run;

 axis1 label = (h=2.5 r=0 a=90 f=swiss 'Ordered distances')
        value =(h=0.25in f=swiss)
        length = 5 in
        color = black width=4.0;

  axis2 label = (h=2.5 f=swiss 'Chi-square quantiles')
        length = 5.5 in
		value =(h=0.25in f=swiss)
        color=black width=4.0;

  symbol1 v=circle h=2 c=black width=2;

proc gplot data=CHISQ;
  	plot col1*col2 / vaxis=axis1 haxis=axis2;
  	title ls=2.0in h=4 f=swiss  'Chi-square Plot';
run;

axis2 label=(f=swiss h=2.5 a=90 r=0
       "Ordered Distances ")
      value=(f=triplex h=2.0)
      length = 5.0in;

axis1 label=(f=swiss h=2.5 
      "Chi-Square Quantiles")
      value=(f=triplex h=1.6)
      length= 5.5in;
 
 SYMBOL1 V=CIRCLE H=2.0 ;
 
PROC GPLOT DATA=CHISQ;
   	PLOT Col1*COL2 / vaxis=axis2 haxis=axis1;
	TITLE1 ls=1.5in H=3.0 F=swiss "CHI-SQUARE PLOT";
	Title2 H=2.3 F=swiss "Board Stiffness Data";
	footnote ls=1.0in;
RUN;

proc gplot data=ALoadings;
     plot VF2*VF1 PF2*pf1/overlay legend=legend1 haxis=axis1 vaxis=axis2;
	 goptions reset=all;
	 symbol1 v=circle c=red h=1.5 w=2;
	 symbol2 v=triangle c=blue h=1.5 w=2;
	 axis1 label=(f="Arial" c=magenta h=5pct "Factor1") c=cyan width=3 minor=none
               value=(f="Arial" c=red h=3pct);
     axis2 label=(f="Arial" c=magenta h=5pct "Factor2") c=cyan width=3 minor=none
               value=(f="Arial" c=red h=3pct); 
	 legend1 down=2 position=(top left inside) cshadow=black frame
	           value=(f="Arial" h=12pt t=1 "Varimax" t=2 "Promax")
			   label=(f="Arial" h=2 "Method");
	 title f="Arial" c=black h=18pt "Factor plot of Varimax and Promax";
run;

proc plot data=results vtoh=2;
   plot dim2*dim1='*' $ work  /box href=0 vref=0;
   plot dim3*dim1='*' $ work  /box href=0 vref=0;
   plot dim3*dim2='*' $ work  /box href=0 vref=0;
run;

PROC PLOT;
     PLOT CAN2*CAN1=CLUSTER/HPOS=56;
     PLOT CAN3*CAN1=CLUSTER/HPOS=56;
     TITLE2 "PLOT OF &N CLUSTERS FROM METHOD=&METHOD";
RUN;

goptions targetdevice=ps300 rotate=portrait;

axis1 label=(f=swiss h=2.5  
      "Differences in BOD Measurements")
      value=(f=triplex h=1.6)
      length= 5.5in;

axis2 label=(f=swiss h=2.5 a=90 r=0
       "Differences in SS Measurements ")
      value=(f=triplex h=2.0)
      length = 5.0in;

SYMBOL1 V=CIRCLE H=2.0 ;

PROC GPLOT DATA=set1;
     PLOT dBOD*dSS / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "Differences in Effluent Measurements";
     footnote ls=1.0in;
RUN;

goptions cback=white colors=(black) targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=3.5 )
      ORDER = 350 to 700 by 50
      value=(f=triplex h=3.0)
      length= 8.0 in;

axis2 label=(f=swiss h=3.5 r=0 a=90)
      order = 2 to 4 by 0.2
      value=(f=swiss h=3.0)
      length = 4.5 in;

      SYMBOL1 V=CIRCLE H=2.5 ;
      SYMBOL2 V=square H=2.2 ;
	  Symbol3 V=dot H=2.2;

PROC GPLOT DATA=SET1;
      PLOT GPA*GMAT=admit / vaxis=axis2 haxis=axis1;
      TITLE1  H=3.5 F=swiss "Business Graduate School Admissions Data";
RUN;

PROC PLOT;
   	PLOT CAN2*CAN1=CLUSTER/HPOS=56;
RUN;

proc g3d data = one ;
  	plot x1 * x2 = likelihood ;
run;

proc g3d data = one ;
  	plot x1 * x2 = likelihood / rotate = 45 ;
run;

proc g3d data = one ;
  	plot x1 * x2 = likelihood / rotate = 45 tilt = 20 ;
run;

axis1 order = (0 to 0.5 by 0.05) minor = none length = 70 pct
   		label = (f=swiss a=90 h=1.5 'Sample quantiles') ;

axis2 order = (-2.4 to 2.4 by 0.4) minor = none length = 70 pct
   		label = (f=swiss h=1.5 'Normal quantiles');

symbol1 v = dot c = green i = none h = 1 ;

proc gplot data = all ;
  plot radiation * q / vaxis = axis1 haxis = axis2 frame ;
  title 'Q-Q plot for microwave example' h=2 f=swiss ;
run ;

proc plot data=scores;
     plot prin1*prin2=newage/ hpos=56;
     plot prin1*prin3=newage/ hpos=56;
run;

proc plot data=pcorr;
     plot pcorr1*pcorr2=newage / hpos=56;
     plot pcorr1*pcorr3=newage / hpos=56;
run;

goptions cback=white colors=(black) 
    targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=2.5 )
      ORDER = -2.5 to 2.5 by 1.
      value=(f=triplex h=2.5)
      length= 5.5 in;

axis2 label=(f=swiss h=2.6 r=0 a=90)
      order = -2.5 to 2.5 by 1.
      value=(f=triplex h=2.5)
      length = 4.5 in;

SYMBOL1 V=circle H=2.5 l=1 w=3 i=none;
      
 proc gplot data=men2v;
   plot factor2*factor1 / vaxis=axis2 haxis=axis1;
   title1 ls=0.1in h=3.0 f=swiss "Varimax Factor Scores";
   title2 h=2.5 f=swiss "Male Track Records";
     label factor1='Distance Races ';
     label factor2 = 'Sprints';
   footnote ls=0.4in '  ';
   RUN;

 proc gplot data=men2v;
   plot factor2*factor1 / vaxis=axis2 haxis=axis1;
   title1 ls=0.1in h=3.0 f=swiss "Promax Factor Scores";
   title2 h=2.5 f=swiss "Male Track Records";
     label factor1='Distance Races ';
     label factor2 = 'Sprints';
   footnote ls=0.4in '  ';
   RUN;

   proc gplot data=women3v;
   plot factor2*factor1 / vaxis=axis2 haxis=axis1;
   title1 ls=0.1in h=3.0 f=swiss "Varimax Factor Scores";
   title2 h=2.5 f=swiss "Female Track Records";
     label factor1='Sprints ';
     label factor2 = 'Distance Races';
   footnote ls=0.4in '  ';
   RUN;

 proc gplot data=women3v;
   plot factor3*factor1 / vaxis=axis2 haxis=axis1;
   title1 ls=0.1in h=3.0 f=swiss "Varimax Factor Scores";
   title2 h=2.5 f=swiss "Female Track Records";
     label factor1='Sprints ';
   footnote ls=0.4in '  ';
   RUN;

   proc gplot data=women3v;
   plot factor3*factor2 / vaxis=axis2 haxis=axis1;
   title1 ls=0.1in h=3.0 f=swiss "Varimax Factor Scores";
   title2 h=2.5 f=swiss "Female Track Records";
     label factor2='Distance Races ';
   footnote ls=0.4in '  ';
   RUN;

proc gplot data=coor;
  	symbol1 v=point;
  	axis1 length=6 IN order=-0.60 to 1.00 by .2;
  	axis2 length=6 IN order=-0.40 to 0.40 by .1;
  	plot   y*x=1/haxis= axis1 vaxis= axis2 frame href=0 vref=0 annotate=coor;
run;

proc plot data=pcorr;
  	plot pcorr1*pcorr2=type / hpos=56;
  	plot pcorr1*pcorr3=type / hpos=56;
run;

proc plot data=results vtoh=2;
   	plot dim2*dim1='*' $ parents  /box href=0 vref=0;
   	plot dim3*dim1='*' $ parents  /box href=0 vref=0;
run;

proc gplot data=cplot;
  	symbol1 v=point;
  	axis1 length = 6 in order = -.3 to .3 by .1;
    axis2 length = 6 in order = -.3 to .3 by .1;
  	plot y*x = 1 / haxis=axis1 vaxis=axis2 frame href=0 vref=0 annotate=cplot;
  	title1 'Associations Between Mental Health Status' h=2.5 f=swiss;
  	title2 'and Socioeconomic Status' h=2.5 f=swiss;
run;

goptions cback=white colors=(black) 
    targetdevice=ps rotate=portrait;

axis1 label=(f=swiss h=3.5 )
      ORDER = 32 to 44 by 2
      value=(f=triplex h=3.0)
      length= 6 in;

axis2 label=(f=swiss h=3.5 r=0 a=90)
      order = 56 to 66 by 2
      value=(f=swiss h=3.0)
      length = 6 in;

SYMBOL1 V=CIRCLE H=2.5 ;
SYMBOL2 V=square H=2.2 ;

PROC GPLOT DATA=SET1;
      PLOT x2*x1=temp / vaxis=axis2 haxis=axis1;
      TITLE1  H=3.5 F=swiss "Strength of Steel Samples";
      TITLE2  H=3.0 F=swiss "Effect of Rolling temperature";
      LABEL x1='Yield Point ';
      LABEL x2 = 'Ultimate Strength';
RUN;

goptions cback=white colors=(black) targetdevice=ps rotate=portrait;

axis1 label=(f=swiss h=3.0 )
      ORDER = -3 to 3 by 1
      value=(f=triplex h=3.0)
      length= 5.5 in;

axis2 label=(f=swiss h=3.1 r=0 a=90)
      order = -3 to 3 by 1
      value=(f=triplex h=3.0)
      length = 5.5 in;

SYMBOL1 V=circle H=2.5 l=1 w=3 i=none;
      
proc gplot data=scorepc2;
     plot factor2*factor1 / vaxis=axis2 haxis=axis1;
     title1 ls=0.3in h=4.0 f=swiss "Factor Scores";
     title2 h=3.0 f=swiss "Stock Market Data";
     label factor1='Market Factor ';
     label factor2 = 'Chemical vs Oil Stocks';
     footnote ls=0.4in '  ';
RUN;

proc plot data=scores ;
     plot prin1*prin2 / hpos=56;
     plot prin1*(length width height)=id / hpos=56;
run;

proc plot data=pcorr;
     plot pcorr1*pcorr2=id / hpos=56;
run;

