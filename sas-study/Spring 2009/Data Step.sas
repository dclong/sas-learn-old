
data set2; 
     set set1;
     array a(i) x1-x6;
     do over a;
        x=a; 
        task=int((i-1)/3)+1; 
        cue=i; 
        if(i>3) then cue=i-3; 
        output;
     end;
     keep subject group task cue x ;  
run;

data _NULL_; 
      set set2;
      file "I:\Study\Spring 2009\STAT 501\Homework\HW3\broota892.dat";
      put (subject group task cue x) (4.0 4.0 4.0 4.0 6.0);
run;

DATA chbones(TYPE=CORR);
     INPUT _TYPE_ $ _NAME_ $ X1 X2 X3 X4 X5 X6;
     datalines;
  CORR  X1  1.000 0.505 0.569 0.602 0.621 0.603
  CORR  X2  0.505 1.000 0.422 0.467 0.482 0.450
  CORR  X3  0.569 0.422 1.000 0.926 0.877 0.878
  CORR  X4  0.602 0.467 0.926 1.000 0.874 0.894
  CORR  X5  0.621 0.482 0.877 0.874 1.000 0.937
  CORR  X6  0.603 0.450 0.878 0.894 0.937 1.000
     N   N    276   276   276   276   276   276
run;

data set1(TYPE=CORR);
  	infile "c:\stat501\data\Lawley.dat";
  	input x1-x6;
  	_TYPE_ ='CORR';
    IF (_N_ = 1) THEN _NAME_='X1';
    IF (_N_ = 2) THEN _NAME_='X2';
    IF (_N_ = 3) THEN _NAME_='X3';
    IF (_N_ = 4) THEN _NAME_='X4';
    IF (_N_ = 5) THEN _NAME_='X5';
    IF (_N_ = 6) THEN _NAME_='X6';
	IF (_N_ = 7) THEN DO; _NAME_='  '; _TYPE_='N   '; END;
run;

DATA huba(TYPE=CORR);
     infile huba;
     INPUT _TYPE_ $ _NAME_ $ x1-x13;
run;

data VLoading;
     set VarStat;
	 if _type_ eq "PATTERN";
	 if _name_ eq "Factor1" then _name_ = "VF1";
	 else _name_ ="VF2";
RUN;

data PLoading;
     set ProStat;
	 where _type_ eq "STRUCTUR";
	 if _name_ eq "Factor1" then _name_ = "PF1";
	 else _name_="PF2";
run;

data set3; 
     set set1;
     array A(I) North East South West;
     do over A;
        Y = A;
        side = I;
        output;
     end;
     keep tree side Y;
run;


data set1;
	infile "corresp.dat";
 	input work $ varw free huma sche sala secu comp inte near atmo soci
           		auto like othe none outd noa node grad smal medi larg;
run;

data x2;
 	set set1;
 	job='varw';y=varw;output;
 	job='free';y=free;output;
 	job='huma';y=huma;output;
 	job='sche';y=sche;output;
 	job='sala';y=sala;output;
 	job='secu';y=secu;output;
 	job='comp';y=comp;output;
 	job='inte';y=inte;output;
 	job='near';y=near;output;
 	job='atmo';y=atmo;output;
 	job='soci';y=soci;output;
 	job='auto';y=auto;output;
 	job='like';y=like;output;
 	job='othe';y=othe;output;
 	job='none';y=none;output;
 	job='outd';y=outd;output;
 	job='noa ';y=noa ;output;
 	keep work job y;
run;

data set1;
     infile czech;
     input hw1-hw9 wh1-wh9 hh1-hh9 ww1-ww9;
run;

data set3; 
     set set1;
     array A(I) x1-x4;
     do over A;
        x = A;
        treat = I;
        output;
     end;
     keep dog treat x;
run;

data set2;
     input ID Admit GPA GMAT;
     datalines;
     101 .  3.21 497
     102 .  3.78 625
run;

data set2;
     input ID $ Type X2 X7;
     datalines;
     new1 . 0.429 0.512
     new2 . 0.124 0.100
     new3 . -0.085 0.635
run;

libname outdat "c:\stat501\sas\";

DATA SET1;
  	INFILE 'c:/stat501/data/crime.dat';
  	INPUT ID $ RESULT AGE SEX EDUC EMOTION ETREAT LIVING ATREAT ALCADD
        		HEALTH FINANCE MARRIAGE PDRINK SIBS WORK WAGES JOBS DAGE
        				DFREQ STOP DRY DRUGS;
  	LABEL RESULT = 1/SUCCESS 2/DROPOUT
          EDUC = EDUCATION LEVEL
       	  EMOTION = EMOTIONAL PROBLEMS TEST SCORE
          ETREAT = PREVIOUS TREATMENT FOR EMOTIONAL PROBLEMS
          LIVING = LIVING ARRANGEMENT
          ATREAT = PREVIOUS TREATMENT FOT ALCOHOLISM
          ALCADD = ALCOHOLISM TEST SCORE
          HEALTH = HEALTH PROBLEMS
          FINANCE = FINANCIAL PROBLEMS
          MARRIAGE = MARITAL STATUS
          PDRINK = PARENTAL DRINKING
          SIBS = NUMBER OF SIBLINGS
          WORK = EMPLOYMENT STATUS
          WAGES = WAGES IN THOUSANDS
          JOBS = NUMBER OF JOBS IN LAST 5 YEARS
          DAGE = AGE WHEN DRINKING STARTED
          DFREQ = DAYS/WEEK DRINKING
          STOP = PREVIOUS ATTEMPT TO STOP
          DRY = LONGEST NON-DRINKING PERIOD (MONTHS)
          DRUGS = OTHER DRUG DEPENDENCIES;
/*  Recode classification variables as a set of binary variables */
  	IF MARRIAGE NE . THEN DO;
    	IF MARRIAGE=1 THEN M1=1; ELSE M1=0;
    	IF MARRIAGE=2 THEN M2=1; ELSE M2=0;
    	IF MARRIAGE=3 THEN M3=1; ELSE M3=0;
    END;
  	IF EDUC NE . THEN DO;
     	IF EDUC=1 THEN E1=1; ELSE E1=0;
     	IF EDUC=2 THEN E2=1; ELSE E2=0;
     	IF EDUC=3 THEN E3=1; ELSE E3=0;
    END;
  	IF LIVING NE . THEN DO;
     	IF LIVING=1 THEN L1=1; ELSE L1=0;
     	IF LIVING=2 THEN L2=1; ELSE L2=0;
     	IF LIVING=3 THEN L3=1; ELSE L3=0;
     	IF LIVING=4 THEN L4=1; ELSE L4=0;
    END;
RUN;

DATA SET2; 
	SET SET1;
  	RESULT=RESULT-1;
  	KEEP RESULT ETREAT ATREAT E3 WORK;
	IF(RESULT=. OR ETREAT=. OR ATREAT=. OR E3=. OR WORK=.) THEN DELETE;
run;

data set2;
     set set1;
     subject=_N_;
     array x(i) x1-x4;
     do over x;
        y=x;
	    test = i;
        output;
     end;
     keep group subject y test;
run;

data one;
  	do i = 1 to 100 ;
    	x1 = 0.2*(i - 1) - 10;
    	do j = 1 to 100 ;
	  		x2 = 0.2*(j - 1) - 10 ;
	  		kernel = exp(-((x1**2 / 3) + (x2**2 / 3))/2) ;
	  		constant = 1 / sqrt(2 * 3.14159 * 9) ;
	  		likelihood = constant * kernel ;
	  		output ;
		end ;
  	end ;
run;


data two;
  	n = 42 ;
  	do i = 1 to n ;
    	p = (i - 0.5) / n ;
		q = probit(p) ;
		output ;
  	end ;
run;

data coor;
  	set results;
  	if _type_='INERTIA'   then delete;
  	x   =dim1;
  	y   =dim2;
  	text=work;
  	label y='Factor 2'
          x='Factor 1';
  	xsys='2';
  	ysys='2';
  	size=1.5;
  	keep x y text xsys ysys size;
run;

DATA spearman(TYPE=CORR);
  	INPUT _TYPE_ $ _NAME_ $ C F E M D Mu;
    datalines;
	CORR  C  1.00 0.83 0.78 0.70 0.66 0.63
	CORR  F  0.83 1.00 0.67 0.67 0.65 0.57
	CORR  E  0.78 0.67 1.00 0.64 0.54 0.51
	CORR  M  0.70 0.67 0.64 1.00 0.45 0.51
	CORR  D  0.66 0.65 0.54 0.45 1.00 0.40
	CORR  Mu 0.63 0.57 0.51 0.51 0.40 1.00
   	N  N    33   33   33   33   33   33
run;

data cplot; 
	set results;
  	if( _type_ = 'INERTIA') then delete;
  	x = dim1;
  	y = dim2;
  	label y = 'Factor 1'
          x = 'Factor 2';
    text = parents;
  	xsys='2';
  	ysys='2';
  	size = 1.5;
  	keep x y text xsys ysys size;
run;

DATA PCORR(TYPE=CORR);
     INFILE test;
	 _TYPE_ = 'CORR';
     IF (_N_=1) THEN DO;  
          _NAME_ = '    ';
          _TYPE_='N   '; 
     END;
     IF (_N_=2) THEN DO; 
          _NAME_='    '; 
          _TYPE_='MEAN'; 
     END;
     IF (_N_=3) THEN DO; 
          _NAME_='    '; 
          _TYPE_='STD '; 
     END;
     INPUT X1-X12 #2 X13-X24;
     IF (_N_ = 4) THEN _NAME_='X1';
     IF (_N_ = 5) THEN _NAME_='X2';
     IF (_N_ = 6) THEN _NAME_='X3';
     IF (_N_ = 7) THEN _NAME_='X4';
     IF (_N_ = 8) THEN _NAME_='X5';
     IF (_N_ = 9) THEN _NAME_='X6';
     IF (_N_ = 10) THEN _NAME_='X7';
     IF (_N_ = 11) THEN _NAME_='X8';
     IF (_N_ = 12) THEN _NAME_='X9';
     IF (_N_ = 13) THEN _NAME_='X10';
     IF (_N_ = 14) THEN _NAME_='X11';
     IF (_N_ = 15) THEN _NAME_='X12';
     IF (_N_ = 16) THEN _NAME_='X13';
     IF (_N_ = 17) THEN _NAME_='X14';
     IF (_N_ = 18) THEN _NAME_='X15';
     IF (_N_ = 19) THEN _NAME_='X16';
     IF (_N_ = 20) THEN _NAME_='X17';
     IF (_N_ = 21) THEN _NAME_='X18';
     IF (_N_ = 22) THEN _NAME_='X19';
     IF (_N_ = 23) THEN _NAME_='X20';
     IF (_N_ = 24) THEN _NAME_='X21';
     IF (_N_ = 25) THEN _NAME_='X22';
     IF (_N_ = 26) THEN _NAME_='X23';
     IF (_N_ = 27) THEN _NAME_='X24';
run;

