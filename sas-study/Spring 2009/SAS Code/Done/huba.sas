
/*  This SAS code is posted as  huba.sas
    It read a correlation matrix and computes
    principal components.  The variables are

    _TYPE_   Identifies type of data
    _NAME_   variable name
        X1	 Cigarettes
		X2	 Beer
		X3	 Wine
		X4	 Hard liquor
		X5	 Cocaine
		X6	 Tranquilizers
		X7	 Over the counter medications used to get high
		X8	 Heroin and other opiates
		X9	 Marijuana
		X10  Hashish
		X11	 Inhalents (glue, gasoline, etc.)
		X12	 Halucinogenics (LSD, mescaline, etc.)
		X13  Amphetamine stimulants

Note that sample sizes are included at the bottom
of the correlation matrix.  */

options nonumber nodate nocenter linesize=80;

filename huba url "http://streaming.stat.iastate.edu/~stat501/data/huba.dat";
/*  Enter the data as a TYPE=CORR data set */

filename huba "huba.dat";

DATA huba(TYPE=CORR);
     infile huba;
     INPUT _TYPE_ $ _NAME_ $ x1-x13;
run;
 
PROC PRINT data=huba;
     title "Principal Component Analyis";
     title2 "Psychoactive Drug Use";
run;

/*  Compute principal components from
    the correlation matrix*/

PROC factor DATA=huba(TYPE=CORR) SCREE CORR SCORE METHOD=PRIN N=13  EV REORDER;
run;

