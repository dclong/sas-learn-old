

/*  Program to read the spotted owl data from
    Chapter 10 in Case Studies in Biometry.  */

data owls;
	infile 'c:/stat501/spotowls.dat' ;
	input type $  km91 km118 km140 km160 km177 km241 km338 ;
run;

proc print data=owls;
run;


/*  Compute estimates of three principal 
    components from the sample correlation 
    matrix and plot the scores      */

proc princomp data=set1 n=3 out=pcorr prefix=pcorr;
  	var x1-x10;
run;

proc print data=pcorr; 
run;

proc plot data=pcorr;
  	plot pcorr1*pcorr2=type / hpos=56;
  	plot pcorr1*pcorr3=type / hpos=56;
run;



/*  Test for homogeneous covariance matrices
    If spotted owls prefer certain types of
    habitat, the sample of spotted owl nesting
    mat exhibit less variation than a sample
    of randomly selected sites */


proc discrim data=owls pool=test wcorr pcorr;
  	class type;
  	var km91 km118 km140 km160 km177 km241 km338 ;
run;

/* Compare mean vectors with a Hotelling T2 test.
   The covariance matrices need not be homogeneous
   because the sample sizes are equal.  */

proc glm data=owls;
  	class type;
  	model km91 km118 km140 km160 km177 km241 km338 = type;
  	manova h=type;
run;

proc ttest data=owls;
   	class type;
   	var km91 km118 km140 km160 km177 km241 km338;
run;


proc npar1way data=owls;
   	class type;
   	var km91 km118 km140 km160 km177 km241 km338;
run;
