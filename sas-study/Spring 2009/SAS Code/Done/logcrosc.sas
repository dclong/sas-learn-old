
/* This program uses logistic regression to classify the
   subjects sent to the alcohol rehabilitation program
   in Story County. The data are posted as crime.dat

   This program is posted as logcrosc.sas
*/

DATA SET1;
  	INFILE 'C:\Documents and Settings\dclong\My Documents\SAS Study\Spring 2009\SAS Code\crime.dat';
  	INPUT ID $ RESULT AGE SEX EDUC EMOTION ETREAT LIVING ATREAT ALCADD
        		HEALTH FINANCE MARRIAGE PDRINK SIBS WORK WAGES JOBS DAGE
        				DFREQ STOP DRY DRUGS;
  	LABEL RESULT = 1/SUCCESS 2/DROPOUT
          EDUC = EDUCATION LEVEL
       	  EMOTION = EMOTIONAL PROBLEMS TEST SCORE
          ETREAT = PREVIOUS TREATMENT FOR EMOTIONAL PROBLEMS
          LIVING = LIVING ARRANGEMENT
          ATREAT = PREVIOUS TREATMENT FOT ALCOHOLISM
          ALCADD = ALCOHOLISM TEST SCORE
          HEALTH = HEALTH PROBLEMS
          FINANCE = FINANCIAL PROBLEMS
          MARRIAGE = MARITAL STATUS
          PDRINK = PARENTAL DRINKING
          SIBS = NUMBER OF SIBLINGS
          WORK = EMPLOYMENT STATUS
          WAGES = WAGES IN THOUSANDS
          JOBS = NUMBER OF JOBS IN LAST 5 YEARS
          DAGE = AGE WHEN DRINKING STARTED
          DFREQ = DAYS/WEEK DRINKING
          STOP = PREVIOUS ATTEMPT TO STOP
          DRY = LONGEST NON-DRINKING PERIOD (MONTHS)
          DRUGS = OTHER DRUG DEPENDENCIES;
/*  Recode classification variables as a set of binary variables */
  	IF MARRIAGE NE . THEN DO;
    	IF MARRIAGE=1 THEN M1=1; ELSE M1=0;
    	IF MARRIAGE=2 THEN M2=1; ELSE M2=0;
    	IF MARRIAGE=3 THEN M3=1; ELSE M3=0;
    END;
  	IF EDUC NE . THEN DO;
     	IF EDUC=1 THEN E1=1; ELSE E1=0;
     	IF EDUC=2 THEN E2=1; ELSE E2=0;
     	IF EDUC=3 THEN E3=1; ELSE E3=0;
    END;
  	IF LIVING NE . THEN DO;
     	IF LIVING=1 THEN L1=1; ELSE L1=0;
     	IF LIVING=2 THEN L2=1; ELSE L2=0;
     	IF LIVING=3 THEN L3=1; ELSE L3=0;
     	IF LIVING=4 THEN L4=1; ELSE L4=0;
    END;
RUN;

PROC FORMAT;
	VALUE FR 1 = 'SUCCESS'
             2 = 'DROPOUT';
run;

PROC FORMAT;
  	VALUE FEDUC 1 = '8 yeras or less'
              	2 = '9 to 11 years'
              	3 = 'High School'
              	4 = 'College';
run;

PROC FORMAT;
  	VALUE FT 1 = 'YES'
             2 = 'NO';
run;

PROC FORMAT;
  	VALUE FLIVE 1 = 'alone'
                2 = 'parents'
                3 = 'friends'
                4 = 'spouse'
                5 = 'institution';
run;

PROC FORMAT;
  	VALUE FMARR 1 = 'good marriage'
                2 = 'poor marriage'
                3 = 'divorced'
                4 = 'unmarried';
RUN;


/*  Sort and print the data file  */

PROC SORT DATA=SET1;
	BY RESULT;
RUN;


/*  Use resubstitution method to estimate misclassification
    probabilities for a model using selected covariates   */

proc logistic data=set1 nosimple covout outest=setp1;
  	model result =  ETREAT ATREAT E3 WORK/ itprint covb corrb ctable pprob=.4
                   maxiter=50 selection=none converge=.0001;
run;

PROC PRINT DATA=SETP1;
run;


/*  A program for crossvalidation estimation of misclassification;
    probabilities using logistic regression.

    First modify the data file to include only those variables
    used in the logistic regresssion model.  The response variable
    should appear first.  It must be recoded as 0 for success and
    1 for failure.  The explanatory variables are next, listed in
    the same order they appear in the model statement for PROC
    LOGISTIC.  Cases with missing data must be deleted because
    PROC IML cannot handle missing data.  */

DATA SET2; 
	SET SET1;
  	RESULT=RESULT-1;
  	KEEP RESULT ETREAT ATREAT E3 WORK;
	IF(RESULT=. OR ETREAT=. OR ATREAT=. OR E3=. OR WORK=.) THEN DELETE;
run;


/*  SELECT THE PARAMETER ESTIMATES FROM THE OUTPUT FILE PRODUCED
    BY PROC LOGISTIC */

DATA SETP; 
	SET SETP1;
  	IF(_TYPE_ = 'PARMS');
  	KEEP INTERCEPT ETREAT ATREAT E3 WORK;
run;

/*  START CROSSVALIDATION PROGRAM IN PROC IML  */

PROC IML;
	START LOGIST;
                                 * Set cutoff value.  If predicted;
                                 * probability exceeds cutoff value;
                                 * then observation is classified;
                                 * into group 0, otherwise it is;
                                 * classified into group 1;
  	CUT = 0.4;
                                 *Identify the input data set;
  	USE SET2;
                                 * Put the data into the matrix Y ;
                                 * It is assumed that the group/response;
                                 * is coded as a binary 0-1 variable in the;
                                 * first column.  The;
                                 * remaining columns contain the data ;
                                 * for the explanatory variables ;
                                 * A column of ones is added by this program ;
                                 * to provide an intercept term;
                                 * The logistic regression model is for the;
                                 * log-odds of membership in the group ;
                                 * coded 0 versus the group coded 1;
  	READ ALL INTO Y;
                                 * Compute the total number of observations;
  	N=NROW(Y);
  	NN=N;
  	K=NCOL(Y);
                                 * Put the columns that indicate the;
                                 * group/response into the matrix S;
  	S=J(N,1)-Y[ ,1];
                                 * Put the model matrix into A;
  	A=Y[ ,2:K];
  	A=J(N,1)||A;
  	AA=A;
                                   * Classification decisions are;
                                   * stored in yhat;
  	SS=S;
  	YHAT=S;
                                  * B contains initial parameter estimates;
 	USE SETP;
 	READ ALL INTO B;
  	BT=B;
  	PRINT,,," MODEL MATRIX IS" A ;
  	PRINT,,,"INITIAL PARAMTER VALUES" BT;
  	MAXIT=50;
  	HALVING=16;
  	CONVERGE=.000001;
                                   * Start iterations for jackknife;
                                   * estimation of misclassification;
  	N=NN-1;
  	B=BT`;
  	C=I(NN);
  	C=C[2:NN, ];
  	A=C*AA;
  	S=C*SS;
  	RUN NEWTON;
  	XA=AA[1, ];
  	XAB=(XA*B)><J(N,1,50);
  	XAB=XAB<>J(N,1,-50);
  	P = EXP(XAB);
  	P = P/(1+P);
  	IF(P > CUT) THEN 
		YHAT[1,1]=0; 
	ELSE 
		YHAT[1,1]=1;
  	NM=NN-1;
  	DO IJ = 2 TO NM;
    	IJM1=IJ-1;
    	IJP1=IJ+1;
    	CC=I(NN);
    	C=CC[1:IJM1, ]//CC[IJP1:NN, ];
    	A=C*AA;
    	S=C*SS;
    	B=BT`;
    	RUN NEWTON;
    	XA=AA[IJ, ];
    	XAB=(XA*B)><J(N,1,50);
    	XAB=XAB<>J(N,1,-50);
    	P = EXP(XAB);
    	P = P/(1+P);
    	IF(P > CUT) THEN 
			YHAT[IJ,1]=1; 
		ELSE 
			YHAT[IJ,1]=0;
  	END;
  	N=NN-1;
  	B=BT`;
  	C=I(NN);
  	C=C[1:N, ];
  	A=C*AA;
  	S=C*SS;
  	RUN NEWTON;
  	XA=AA[NN, ];
    XAB=(XA*B)><J(N,1,50);
    XAB=XAB<>J(N,1,-50);
  	P = EXP(XAB);
  	P = P/(1+P);
  	IF(P > CUT) THEN 
		YHAT[NN,1]=0; 
	ELSE 
		YHAT[NN,1]=1;
                                   * Output results to SET2;
  	Y = YHAT||SS||AA;
  	CREATE SET3 FROM Y;
  	APPEND FROM Y;
  	CLOSE SET3;
 	FINISH;

* -------------- Modified Newton-Raphson algortihm --------------------------;
*                                                                            ;
* User must supply initial values for parameters in B, and the FUN and       ;
* DERIV functions.  FUN evaluates the function to be maximized at the        ;
* current value of  B.  DERIV evaluates first and second partial             ;
* derivatives.  First partial derivatives are put int Q.  The       ;
* The negative of the matrix of second partial derivatives is put   ;
* into H                                                            ;
*----------------------------------------------------------------------------;
	START NEWTON;
  	CHECK=1;
  	DO ITER = 1 TO MAXIT WHILE(INT(CHECK/CONVERGE));
  		RUN FUN;
     	FOLD=F;
     	BOLD=B;
        B2=BOLD;
     	RUN DERIV;
     	HI=INV(H);
     	B=BOLD+HI*Q;
     	RUN FUN;
     	DO HITER = 1 TO HALVING WHILE(FOLD-F >= 0);
        	B=B2+2.*((.5)##HITER)*HI*Q;
        	RUN FUN;
        	B2=B;
     	END;
   		CHECK=((BOLD-B)`*(BOLD-B))/(B`*B);
   	END;
	FINISH;
*---------------USER SUPPLIED FUNCTION EVALUATION----------------------------
;
	START FUN;
  		RUN PROB;
  		F=S`*LOG(PP) +  (J(N,1)-S)`*LOG(J(N,1)-PP);
	FINISH;
*-------------COMPUTATION OF PREDICTED PROBABILITIES------------------------;
*                                                                           ;
	START PROB;
  	AB = A*B;
  	ABT= AB><J(N,1,20);
  	ABT= ABT<>J(N,1,-20);
  	PP = EXP(ABT)/(J(N,1)+EXP(ABT));
	FINISH;
*-------------USER SUPPLIED DERIVATIVE EVALUATION ---------------------------;
*                                                                            ;
* First partial derivatives are returned in Q and the negative of the        ;
* second partial derivatives are returned in the matrix  H.                  ;
*----------------------------------------------------------------------------;
	START DERIV;
  		RUN PROB;
  		Q = A`*(S-PP);
  		M = DIAG(PP)-PP*PP`;
  		H = A`*DIAG(M)*A;
	FINISH;

	RUN LOGIST;
run;
/*  Crossclassify the results of the crossvalidation  */

DATA SET3; 
	SET SET3;
  	LABEL COL1 = CLASSIFICATION
          COL2 = ACTUAL GROUP;
RUN;

PROC FREQ DATA=SET3;
  	TABLES COL2*COL1 / NOPERCENT NOCOL;
RUN;
