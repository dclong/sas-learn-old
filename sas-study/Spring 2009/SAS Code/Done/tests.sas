/*This progam is for question 2 of Assigment #5 of STAT 501*/

filename test url "http://streaming.stat.iastate.edu/~stat501/data/tests.cor";

DATA PCORR(TYPE=CORR);
     INFILE test;
	 _TYPE_ = 'CORR';
     IF (_N_=1) THEN DO;  
          _NAME_ = '    ';
          _TYPE_='N   '; 
     END;
     IF (_N_=2) THEN DO; 
          _NAME_='    '; 
          _TYPE_='MEAN'; 
     END;
     IF (_N_=3) THEN DO; 
          _NAME_='    '; 
          _TYPE_='STD '; 
     END;
     INPUT X1-X12 #2 X13-X24;
     IF (_N_ = 4) THEN _NAME_='X1';
     IF (_N_ = 5) THEN _NAME_='X2';
     IF (_N_ = 6) THEN _NAME_='X3';
     IF (_N_ = 7) THEN _NAME_='X4';
     IF (_N_ = 8) THEN _NAME_='X5';
     IF (_N_ = 9) THEN _NAME_='X6';
     IF (_N_ = 10) THEN _NAME_='X7';
     IF (_N_ = 11) THEN _NAME_='X8';
     IF (_N_ = 12) THEN _NAME_='X9';
     IF (_N_ = 13) THEN _NAME_='X10';
     IF (_N_ = 14) THEN _NAME_='X11';
     IF (_N_ = 15) THEN _NAME_='X12';
     IF (_N_ = 16) THEN _NAME_='X13';
     IF (_N_ = 17) THEN _NAME_='X14';
     IF (_N_ = 18) THEN _NAME_='X15';
     IF (_N_ = 19) THEN _NAME_='X16';
     IF (_N_ = 20) THEN _NAME_='X17';
     IF (_N_ = 21) THEN _NAME_='X18';
     IF (_N_ = 22) THEN _NAME_='X19';
     IF (_N_ = 23) THEN _NAME_='X20';
     IF (_N_ = 24) THEN _NAME_='X21';
     IF (_N_ = 25) THEN _NAME_='X22';
     IF (_N_ = 26) THEN _NAME_='X23';
     IF (_N_ = 27) THEN _NAME_='X24';
run;

PROC PRINT DATA=PCORR; 
     title f="Arial" c=red h=3 "Correlation Matrix for Test Scores";
run;

*The following code is for part (a);
proc factor data=pcorr(type=corr) method=prin n=5 rotate=varimax;
     var x1-x24;
run;

proc iml;
     x={4.2216125 3.2266243 3.1408509 2.2706496 1.5913912};
	 TVP=x/24;
	 print "The total variance accounted by the five rotated factor is";
	 print TVP;
quit;

*The following code is for part (b);
proc factor data=pcorr(type=corr) method=prinit n=5 rotate=varimax maxit=200 heywood;
     var x1-x24;
	 title f="Arial" h=4 c=red "Factor Analysis Using Principal Factor Method";
run;

PROC FACTOR DATA=PCORR(TYPE=CORR) METHOD=ML NFACTOR=5 ROTATE=VARIMAX;
     VAR X1-X24;
	 title f="Arial" c=red h=5 "Factor analysis using ML Method"; 
run;

proc factor data=pcorr(type=corr) method=ml n=4 rotate=varimax;
     var x1-x24;
	 title f="Arial" c=red h=5 "Test of the # of factors";
run;

proc factor data=pcorr(type=corr) method=ml n=5 rotate=promax;
     var x1-x24;
	 title f="Arial" c=red h=5 "Promax rotation on the result of ML method";
run;

proc factor data=pcorr(type=corr) method=ml n=5 rotate=quartimax;
     var x1-x24;
	 title f="Arial" c=red h=5 "Quartimax rotation on the result of ML method";
run;
