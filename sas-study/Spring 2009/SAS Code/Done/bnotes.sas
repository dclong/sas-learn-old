

/*  A SAS program to analyze the bank note data
    The data are stored in the file

                 bnotes.dat

    and the program is stored in the file

                 bnotes.sas
*/

filename bnotes url 'http://streaming.stat.iastate.edu/~stat501/data/bnotes.dat';

data bnotes;
     infile bnotes;
     input id x1 x2 x3 x4 x5 x6;
run;

/*   FIRST PRINT THE DATA FILE */
PROC PRINT DATA=bnotes;
run;

/*   COMPUTE CORRELATIONS */

PROC CORR DATA=bnotes;
     VAR X1-X6; 
run;

/*  Compute the Shapiro-Wilk W tests 
   for normality */

PROC UNIVARIATE DATA=bnotes NORMAL;
     VAR X1-X6;  
run;

/*   COMPUTE NORMAL Q-Q PLOTS  */

PROC RANK DATA=bnotes NORMAL=BLOM OUT=bnotes;
     VAR X1-X6; 
     RANKS Q1-Q6;
run;

/*   Use SASGRAPH to display the normal 
     probability plots */

/*  WINDOWS users should use these options 
    to prepare the figure for printing 
    as a postscript file*/

goptions cback=white colors=(black) device=WIN target=ps;

  axis1 label=(h=2.5 r=0 a=90 f=swiss)
        length = 5 in
        value=(h=0.25in f=swiss)
        color = black width=4.0;

  axis2 label = (h=2.5 f=swiss 'Standard Normal quantiles')
        value=(h=0.25in f=swiss)
        length = 5.5 in
        color=black width=4.0;

  symbol1 v=circle h=2 c=black width=2;

proc gplot data=bnotes; 
     PLOT X1*Q1 / vaxis=axis1 haxis=axis2;
     PLOT X2*Q2 / vaxis=axis1 haxis=axis2;
     PLOT X3*Q3 / vaxis=axis1 haxis=axis2;
     PLOT X4*Q4 / vaxis=axis1 haxis=axis2; 
     PLOT X6*Q6 / vaxis=axis1 haxis=axis2;  
     title ls=2.0in h=4 f=swiss  'Normal Q-Q Plot';
run;
 



/*   USE PROC IML    
      TO COMPUTE SUMMARY STATISTICS, PARTIAL 
      CORRELATIONS, CHI-SQUARE Q-Q PLOT FOR  
      MULTIVARIATE NORMALITY, AND A GOODNESS_
      OF-FIT TEST  */


DATA newnotes; 
     SET bnotes;
     KEEP X1-X6;
run;

PROC IML;
     START NORMAL;
     USE newnotes;          *ENTER THE DATA;
     READ ALL  INTO X;
     N=NROW(X);             *NUMBER OF OBSERVATIONS IS N;
P=NCOL(X);             *COMPUTE NUMBER OF TRAITS = P;
SUM=X[+, ];            *COMPUTE TOTAL FOR EACH TRAIT;
A=X`*X-SUM`*SUM/N;     *COMPUTE CORRECTED CROSSPRODUCTS MATRIX;
S=A/(N-1);             *COMPUTE SAMPLE COVARIANCE MATRIX;
XBAR=SUM/N;            *COMPUTE TRANSPOSE OF SAMPLE MEAN VECTOR;
SCALE=INV(SQRT(DIAG(A)));
R=SCALE*A*SCALE;       *COMPUTE SAMPLE CORRELATION MATRIX;

PRINT,,,,," THE SAMPLE MEAN VECTOR";
PRINT XBAR;
PRINT,,,,," THE SAMPLE CORRELATION MATRIX";
PRINT R;

                       * Compute a chi-square probability plot;

E=X-(J(N,1)*XBAR);     *MATRIX OF DEVIATIONS FROM MEAN VECTOR;
D=VECDIAG(E*INV(S)*E`);    *SQUARED MAHALANOBIS DISTANCES;
RD = RANK(D);            * COMPUTE RANKS FOR THE D VALUES;
RD =(RD-.5)/N;
PD2=P/2;
Q=2*GAMINV(RD,PD2);  

/* COMPUTE CHI-SQUARE PLOTTING POSITIONS */
DD=D||Q;
CREATE CHISQ FROM DD ;   

/* OPEN A FILE TO TEMPORARILY STORE RESULTS */
APPEND FROM DD;

/* Compute the p-value for a correlation test
   of multivariate normality, using the points on 
   chi-square probability plot */

rpn = t(D)*Q-(sum(D)*sum(Q))/N;
rpn = rpn/sqrt((ssq(D)-(sum(D)**2)/N)*(ssq(Q)-(sum(Q)**2)/N));

/* Simultate the p-value for the 
   correlation test  */

ns=10000;
pvalue=0;
do i=1 to ns;
  do j1=1 to n;
  do j2=1 to p;
    x[j1,j2] = rannor(-100);
    end;
    end;

   SUMX=X[+, ];           
   A=t(X)*X-t(SUMX)*SUMX/N;     
   S=A/(N-1);             
   XBAR=SUMX/N;           
   E=X-(J(N,1)*XBAR);   
   DN=VECDIAG(E*INV(S)*t(E));   
   RN = RANK(DN);            
   RN =(RN-.5)/N;
   PD2=P/2;
   QN=2*GAMINV(RN,PD2);  
   rpnn = t(DN)*QN-(sum(DN)*sum(QN))/N;
   rpnn = rpnn/sqrt((ssq(DN)-(sum(DN)**2)/N)*(ssq(QN)-(sum(QN)**2)/N));
   if(rpn>rpnn) then pvalue=pvalue+1;   
   end;
   pvalue=pvalue/ns;

print,,,,,"Correlation Test of Normality";
print N P rpn pvalue;

FINISH;
RUN NORMAL;



/*   Display THE CHI-SQUARE PROBABILITY PLOT FOR 
     ASSESSING MULTIVARIATE NORMALITY */



  axis1 label = (h=2.5 r=0 a=90 f=swiss 'Ordered distances')
        value =(h=0.25in f=swiss)
        length = 5 in
        color = black width=4.0;

  axis2 label = (h=2.5 f=swiss 'Chi-square quantiles')
        length = 5.5 in
		value =(h=0.25in f=swiss)
        color=black width=4.0;

  symbol1 v=circle h=2 c=black width=2;

proc gplot data=CHISQ;
  	plot col1*col2 / vaxis=axis1 haxis=axis2;
  	title ls=2.0in h=4 f=swiss  'Chi-square Plot';
run;
