

/*  This program is posted as  records.sas
    It performs separate factor analyses on track 
    records from 54 countries for men and women 
    from Tables 1.9 and 8.6 in Johnson & Wichern.
    The first three times are in second, and the 
    remaining times are in minutes. The data are 
    posted as records.women.dat and records.men.dat */

options nonumber nodate linesize=64;

data women;
  	infile "c:\stat501\data\records.women.dat";
  	input country $ x1-x7;
  	label x1 = 100m 
          x2 = 200m 
          x3 = 400m
          x4 = 800m
          x5 = 1500m
          x6 = 3000m
          x7 = marathon;
run;

data men;
  	infile "c:\stat501\data\records.men.dat";
  	input country $ x1-x8;
  	label x1 = 100m 
          x2 = 200m 
          x3 = 400m
          x4 = 800m
          x5 = 1500m
          x6 = 5000m
		  x7 = 10000m
          x8 = marathon;
run;


/* Print the data files  */

proc print data=women; 
run;

proc print data=men;
run;


/*  Use maximum likelihood estimation 
    Search for appropriate number of factors */

proc factor data=women method=ml nfactors=1 maxiter=75 heywood;
  	var x1-x7;
  	priors smc;
run;

proc factor data=women method=ml nfactors=2 maxiter=75 heywood
   					rotate=varimax out=women2v;
  	var x1-x7;
  	priors smc;
run;

proc factor data=women method=ml nfactors=2 maxiter=75 heywood
   					rotate=promax out=women2p;
  	var x1-x7;
  	priors smc;
run;

proc factor data=women method=ml nfactors=3 maxiter=75 heywood
   				rotate=varimax out=women3v;
  	var x1-x7;
  	priors smc;
run;

proc factor data=women method=ml nfactors=3 maxiter=75 heywood
   	rotate=promax out=women3p;
  	var x1-x7;
  	priors smc;
run;

proc factor data=women method=ml nfactors=4 maxiter=75 heywood
   				rotate=varimax out=women4v;
  	var x1-x7;
  	priors smc;
run;

proc factor data=women method=ml nfactors=4 maxiter=75 heywood
   			rotate=promax out=women4p;
  	var x1-x7;
  	priors smc;
run;

proc factor data=men method=ml nfactors=1 maxiter=75 heywood;
  	var x1-x8;
  	priors smc;
run;

proc factor data=men method=ml nfactors=2 maxiter=75 heywood
   				rotate=varimax out=men2v;
  	var x1-x8;
  	priors smc;
run;

proc factor data=men method=ml nfactors=2 maxiter=75 heywood
   			rotate=promax out=men2p;
  	var x1-x8;
  	priors smc;
run;


proc factor data=men method=ml nfactors=3 maxiter=75 heywood
   					rotate=varimax out=men3f;
  	var x1-x8;
  	priors smc;
run;

  
proc factor data=men method=ml nfactors=3 maxiter=75 heywood
   				rotate=promax out=men3p;
  	var x1-x8;
  	priors smc;
run;

proc factor data=men method=ml nfactors=4 maxiter=75 heywood;
  	var x1-x8;
  	priors smc;
run;



/*  Plot the scores for rotated factors */


goptions cback=white colors=(black) 
    targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=2.5 )
      ORDER = -2.5 to 2.5 by 1.
      value=(f=triplex h=2.5)
      length= 5.5 in;

axis2 label=(f=swiss h=2.6 r=0 a=90)
      order = -2.5 to 2.5 by 1.
      value=(f=triplex h=2.5)
      length = 4.5 in;

SYMBOL1 V=circle H=2.5 l=1 w=3 i=none;
      
 proc gplot data=men2v;
   plot factor2*factor1 / vaxis=axis2 haxis=axis1;
   title1 ls=0.1in h=3.0 f=swiss "Varimax Factor Scores";
   title2 h=2.5 f=swiss "Male Track Records";
     label factor1='Distance Races ';
     label factor2 = 'Sprints';
   footnote ls=0.4in '  ';
   RUN;

 proc gplot data=men2v;
   plot factor2*factor1 / vaxis=axis2 haxis=axis1;
   title1 ls=0.1in h=3.0 f=swiss "Promax Factor Scores";
   title2 h=2.5 f=swiss "Male Track Records";
     label factor1='Distance Races ';
     label factor2 = 'Sprints';
   footnote ls=0.4in '  ';
   RUN;

   proc gplot data=women3v;
   plot factor2*factor1 / vaxis=axis2 haxis=axis1;
   title1 ls=0.1in h=3.0 f=swiss "Varimax Factor Scores";
   title2 h=2.5 f=swiss "Female Track Records";
     label factor1='Sprints ';
     label factor2 = 'Distance Races';
   footnote ls=0.4in '  ';
   RUN;

 proc gplot data=women3v;
   plot factor3*factor1 / vaxis=axis2 haxis=axis1;
   title1 ls=0.1in h=3.0 f=swiss "Varimax Factor Scores";
   title2 h=2.5 f=swiss "Female Track Records";
     label factor1='Sprints ';
   footnote ls=0.4in '  ';
   RUN;

   proc gplot data=women3v;
   plot factor3*factor2 / vaxis=axis2 haxis=axis1;
   title1 ls=0.1in h=3.0 f=swiss "Varimax Factor Scores";
   title2 h=2.5 f=swiss "Female Track Records";
     label factor2='Distance Races ';
   footnote ls=0.4in '  ';
   RUN;
