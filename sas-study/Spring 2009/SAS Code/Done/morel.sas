
 /*  This program performs a one-way MANOVA
     on the test score data posted as morel.dat
     The code is posted as morel.sas  */

options linesize=64 nocenter nonumber ;

filename morel url "http://streaming.stat.iastate.edu/~stat501/data/morel.dat";

filename morel "morel.dat";

data set1;
     infile morel;
     INPUT group x1 x2 x3 x4;
  /* LABEL group = student group
              x1 = aptitude
              x2 = mathematics
              x3 = langauge
              x4 = general knowledge; */
run;

PROC PRINT data=set1;  
run;

/*  Check normality assumption for each variable
    within each of the three student groups and
    compute box plots                           */

PROC BOXPLOT DATA=set1;
     PLOT (x1 x2 x3 x4)*group / BOXSTYLE=SCHEMATICID;
run;

PROC SORT DATA=set1; 
      BY group; 
run;

PROC UNIVARIATE DATA=set1 NORMAL;
     BY group;
     VAR x1 x2 x3 x4;
run;


/* Check homogeneity of covariance matrices */

PROC DISCRIM DATA=set1 POOL=test SLPOOL=.01 
     WCOV PCOV;  
     CLASS group;  
     VAR X1-X4;
run;


/* Perform a one-way MANOVA*/

PROC GLM DATA=set1;
     CLASS group;
     MODEL x1-x4 = group / P SOLUTION;
     MANOVA H=group /PRINTH PRINTE;
     Manova H=group M= (1 0 0 -1, 0 1 0 -1, 0 0 1 -1) prefix=diff / printh printe;
     Repeated test 4 profile / printm printh printe;
run;


/*  Plot the data  */

goptions cback=white colors=(black) 
    targetdevice=ps rotate=portrait;

axis1 label=(f=swiss h=3.5 )
      ORDER = 0 to 100 by 20
      value=(f=triplex h=3.0)
      length= 5 in;

axis2 label=(f=swiss h=3.5 r=0 a=90)
      order = 0 to 100 by 20
      value=(f=swiss h=3.0)
      length = 5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=square H=2.2 ;
	   symbol3 V=dot h=2.2;


PROC GPLOT DATA=set1;
      PLOT x2*x1=group / vaxis=axis2 haxis=axis1;
      TITLE1  H=3.5 F=swiss "Plot of Aptitude Scores Against Mathematics Scores";
      LABEL x1='Aptitude ';
      LABEL x2 = 'Mathematics';
RUN;


/* Use a Mixed model analysis  */

data set2;
     set set1;
     subject=_N_;
     array x(i) x1-x4;
     do over x;
        y=x;
	    test = i;
        output;
     end;
     keep group subject y test;
run;

proc print data=set2;
run;

proc mixed data=set2;
     class group test subject;
     model y = group test group*test / ddfm=kr;
     random subject / subject=subject(group);
     lsmeans group*test / diff ;
run;











