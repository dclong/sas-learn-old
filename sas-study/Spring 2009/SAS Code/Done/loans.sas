
options nocenter nonumber linesize=84;

/*  This program is posted as  loans.sas
    It provides an illustration of both
    linear and quadratic discriminant analysis.
    Crossvalidation is used to assess
    misclassification probabilities.  The
    data are posted in the file loans.dat  */
filename loan url "http://streaming.stat.iastate.edu/~stat501/data/loans2.dat";

filename loan "loans2.dat";

DATA LOANS;
     INFILE loan;
     INPUT ID  TYPE X1-X8;
     LABEL X1 = C. ASSETS / C. LIABILITIES
           X2 = W. CAPITAL / TOTAL ASSETS
           X3 = C. LIABILITIES / T. ASSETS
           X4 = C. ASSETS / TOTAL ASSETS
           X5 = T. LIABILITIES / T. ASSETS
           X6 = CHANGE IN NET WORTH / T. ASSETS
           X7 = FARM LAND VALUE / T. ASSETS
           X8 = T. LIABILITIES / NET WORTH;
RUN;

PROC FORMAT;
     VALUE T 1='GOOD'
             2='BAD'; 
RUN;

PROC SORT DATA=LOANS; 
     BY TYPE;
RUN;

PROC PRINT DATA=LOANS N UNIFORM;
     VAR TYPE X1-X8;
RUN;

/*  Assess normality for each variable */

PROC UNIVARIATE DATA=LOANS NORMAL;
     BY TYPE;
     VAR X1-X8;
     FORMAT TYPE T.;
RUN;

/* Examine Box Plots */

proc boxplot data=loans;
     plot (x1-x8)*type / boxstyle=schematic;
run;


/* Test for homogeneous covariance matrices.
   In this case the program decides to use
   quadratic discriminant analysis        */

PROC DISCRIM DATA=LOANS POOL=TEST ANOVA CROSSLIST MANOVA METHOD=NORMAL POSTERR
               SIMPLE SLPOOL=.01 OUTSTAT=DISCRIM1;
     CLASS TYPE;
     VAR X1-X8;
     ID ID;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
	 title f="Arial" c=red h=4 "Discrimination Analysis";
run;


/*  Force the program to do linear
    discriminant analysis  */

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE METHOD=NORMAL;
     CLASS TYPE;
     VAR X1-X8;
     ID ID;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
	 title f="Arial" c=red h=4 "Discrimination Analysis Using Linear Rule";
run;


/*  Use linear discriminant analysis to
    search for a good subset of variables */

PROC STEPDISC DATA=LOANS SW SLE=.25 SLS=.25;
     VAR X1-X8;
     CLASS TYPE;
	 title f="Arial" c=red h=4 "Variable selection using stepwise method";
run;

PROC STEPDISC DATA=LOANS BW SLE=.25 SLS=.25;
     VAR X1-X8;
     CLASS TYPE;
	 title f="Arial" c=red h=4 "Variable selection using backward method";
run;

/* Obtain more information about the
   performance of linear discriminant
   models with various subsets of variables */

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
	 title f=Arial c=red h=4 "Discriminaiton using variable X2";
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2 X7;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
	 title f=Arial c=red h=4 "Discrimination analysis using variable X2 and X7";
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2 X7 X4;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
	 title f=arial c=red h=4 "Discrimination analysis using variables x2, x4 and x7";
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2 X7 X4 X8;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2 X7 X4 X8 X3;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2 X7 X4 X8 X3 X6;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
run;

/*  Classifying new cases that are not in the training sample.
    First save information about the classification rule to
    a file in a specified directory   */

libname outdat "c:\stat501\sas\";

PROC DISCRIM DATA=loans POOL =Yes OUTSTAT=outdat.rule1;
     CLASS type;
     VAR x2 x7;
     PRIORS "GOOD"=0.8 "BAD"=0.2;
     FORMAT TYPE T.;
run;

/* Access and print the saved file */

PROC PRINT data=outdat.rule1;
run;


/* Use this stored rule to classify new cases. */

data set2;
     input ID $ Type X2 X7;
     datalines;
     new1 . 0.429 0.512
     new2 . 0.124 0.100
     new3 . -0.085 0.635
run;

proc discrim data=outdat.rule1 testdata=set2 testlist;
     class type;
     var x2 x7;
     testclass type;
     FORMAT TYPE T.;
run;
