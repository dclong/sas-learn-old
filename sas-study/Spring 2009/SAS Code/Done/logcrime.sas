

/* This program uses logistic regression to classify the
   subjects sent to the alcohol rehabilitation program
   in Story County. The data are posted as crime.dat

   This code is posted as logcrime.sas
*/



DATA SET1;
  	INFILE 'crime.dat';
  	INPUT ID $ RESULT AGE SEX EDUC EMOTION ETREAT LIVING ATREAT ALCADD
        		HEALTH FINANCE MARRIAGE PDRINK SIBS WORK WAGES JOBS DAGE
        		DFREQ STOP DRY DRUGS;
  	LABEL RESULT = 1/SUCCESS 2/DROPOUT
          EDUC = EDUCATION LEVEL
       	  EMOTION = EMOTIONAL PROBLEMS TEST SCORE
          ETREAT = PREVIOUS TREATMENT FOR EMOTIONAL PROBLEMS
          LIVING = LIVING ARRANGEMENT
       	  ATREAT = PREVIOUS TREATMENT FOT ALCOHOLISM
          ALCADD = ALCOHOLISM TEST SCORE
          HEALTH = HEALTH PROBLEMS
          FINANCE = FINANCIAL PROBLEMS
          MARRIAGE = MARITAL STATUS
          PDRINK = PARENTAL DRINKING
          SIBS = NUMBER OF SIBLINGS
          WORK = EMPLOYMENT STATUS
          WAGES = WAGES IN THOUSANDS
          JOBS = NUMBER OF JOBS IN LAST 5 YEARS
          DAGE = AGE WHEN DRINKING STARTED
          DFREQ = DAYS/WEEK DRINKING
          STOP = PREVIOUS ATTEMPT TO STOP
          DRY = LONGEST NON-DRINKING PERIOD (MONTHS)
          DRUGS = OTHER DRUG DEPENDENCIES;
/*  Recode classification variables as a set of binary variables */
  	IF MARRIAGE NE . THEN DO;
     	IF MARRIAGE=1 THEN M1=1; ELSE M1=0;
     	IF MARRIAGE=2 THEN M2=1; ELSE M2=0;
     	IF MARRIAGE=3 THEN M3=1; ELSE M3=0;
    END;
  	IF EDUC NE . THEN DO;
     	IF EDUC=1 THEN E1=1; ELSE E1=0;
     	IF EDUC=2 THEN E2=1; ELSE E2=0;
     	IF EDUC=3 THEN E3=1; ELSE E3=0;
    END;
  	IF LIVING NE . THEN DO;
     	IF LIVING=1 THEN L1=1; ELSE L1=0;
     	IF LIVING=2 THEN L2=1; ELSE L2=0;
     	IF LIVING=3 THEN L3=1; ELSE L3=0;
     	IF LIVING=4 THEN L4=1; ELSE L4=0;
    END;
RUN;

PROC FORMAT;
  VALUE FR 1 = 'SUCCESS'
           2 = 'DROPOUT';
run;

PROC FORMAT;
  VALUE FEDUC 1 = '8 yeras or less'
              2 = '9 to 11 years'
              3 = 'High School'
              4 = 'College';
run;

PROC FORMAT;
  VALUE FT 1 = 'YES'
           2 = 'NO';
PROC FORMAT;
  VALUE FLIVE 1 = 'alone'
              2 = 'parents'
              3 = 'friends'
              4 = 'spouse'
              5 = 'institution';
PROC FORMAT;
  	VALUE FMARR 1 = 'good marriage'
              2 = 'poor marriage'
              3 = 'divorced'
              4 = 'unmarried';
RUN;


/*  Sort and print the data file  */

PROC SORT DATA=SET1; 
	BY RESULT;
RUN;


/*  Use the stepwise selection feature of PROC LOGISTIC
    to select a godd subset of variables.  Note that cases with
    missing values are deleted.   */

proc logistic data=set1;
  	model RESULT = AGE SEX E1-E3 EMOTION ETREAT L1-L4 ATREAT ALCADD
        	HEALTH FINANCE M1-M3 PDRINK SIBS WORK WAGES JOBS DAGE
        		DFREQ STOP DRY DRUGS/ sle=.25 sls=.25 selection=s details
                   				maxiter=50 converge=.0001 covb itprint;
run;

/*  Use backward elimination to select a good subset of variables */

proc logistic data=set1;
  	model RESULT = AGE EMOTION ETREAT L1-L4 ATREAT
        	HEALTH  M1-M3 PDRINK SIBS WORK WAGES DAGE
        		DFREQ STOP DRUGS/  sls=.05 selection=b details
                   	maxiter=50 converge=.0001 covb itprint;
run;

/*  Use resubstitution method to estimate misclassification
    probabilities for the selected models  */

proc logistic data=set1 nosimple covout outest=setp1;
  	model RESULT = AGE EMOTION ETREAT L2 ATREAT HEALTH PDRINK WAGES
                 DFREQ  /  covb corrb ctable pprob=.5
                         		maxiter=50 selection=none converge=.0001;
run;

proc logistic data=set1 nosimple covout outest=setp1;
  	model RESULT = E3 ETREAT ATREAT HEALTH WORK
                /  covb corrb ctable pprob=.5
                   		maxiter=50 selection=none converge=.0001;
run;

proc logistic data=set1 nosimple covout outest=setp1;
  	model RESULT = AGE ATREAT/  covb corrb ctable pprob=.5
                   maxiter=50 selection=none converge=.0001;
run;
