/*  We first generate points x1 and x2 from a bivariate normal
distribution, with mean vector [0 0] and sigma_11 = sigma_22 = 3
and sigma_12 = 0.  For each value of x1 in (-10, 10) we generate 
100 values of x2 also in (-10, 10).  */

title ;
 
data one;
  	do i = 1 to 100 ;
    	x1 = 0.2*(i - 1) - 10;
    	do j = 1 to 100 ;
	  		x2 = 0.2*(j - 1) - 10 ;
	  		kernel = exp(-((x1**2 / 3) + (x2**2 / 3))/2) ;
	  		constant = 1 / sqrt(2 * 3.14159 * 9) ;
	  		likelihood = constant * kernel ;
	  		output ;
		end ;
  	end ;
run;

/* I am setting the colors that I want SAS to use for the plot.
  The first two are used for axis and labels and the next is used
  for the figure itself. */

goptions colors = (black black blue green) ;

/* This is the simplest 3d plot statement.  */

proc g3d data = one ;
  	plot x1 * x2 = likelihood ;
run;

/* Here I am rotating the x1 and x2 axes around the likelihood axis, 
and in the next plot I am both rotating and tilting the plot so that
we get a different view, more 'from the top'. */

proc g3d data = one ;
  	plot x1 * x2 = likelihood / rotate = 45 ;
run;

proc g3d data = one ;
  	plot x1 * x2 = likelihood / rotate = 45 tilt = 20 ;
run;
