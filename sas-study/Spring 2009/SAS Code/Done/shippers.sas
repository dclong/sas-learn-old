
/*  This program is posted as shippers.sas in the SAS 
    folder of the course web page.
    It does correspondence analysis of responses
    to a suvey of users of overnight delivery services.
    The data are posted as  shippers.dat  */

data set1;
	infile "c:/stat501/data/shippers.dat";
	input work $ c1-c15;
run;


data set2;
 	set set1;
 	attribute=' c1'; y=c1; output;
 	attribute=' c2'; y=c2; output;
 	attribute=' c3'; y=c3; output;
	attribute=' c4'; y=c4; output;
 	attribute=' c5'; y=c5; output;
	attribute=' c6'; y=c6; output;
	attribute=' c7'; y=c7; output;
	attribute=' c8'; y=c8; output;
	attribute=' c9'; y=c9; output;
	attribute='c10'; y=c10; output;
	attribute='c11'; y=c11; output;
	attribute='c12'; y=c12; output;
	attribute='c13'; y=c13; output;
	attribute='c14'; y=c14; output;
	attribute='c15'; y=c15; output;
	keep work attribute y;
run;

/*  Display the original continency table  */

proc freq data=set2;
  	tables work*attribute / chisq;
  	weight y;
run;

/*  Perform correspondence analysis     */

proc corresp data=set1 out=results cp rp dimens=2 profile=both short;
  	var c1-c15;
  	id work;
run;

proc print data=results;
run;


************Create a data set with plotting position names**********;

data coor;
  	set results;
  	if _type_='INERTIA'   then delete;
  	x   =dim1;
  	y   =dim2;
  	text=work;
  	label y='Factor 2'
          x='Factor 1';
  	xsys='2';
  	ysys='2';
  	size=1.5;
  	keep x y text xsys ysys size;
run;


goptions targetdevice=ps;

/* Use PROC GPLOT  in  SASGRAPH  to display the results  */

proc gplot data=coor;
  	symbol1 v=point;
  	axis1 length=6 IN order=-0.60 to 1.00 by .2;
  	axis2 length=6 IN order=-0.40 to 0.40 by .1;
  	plot   y*x=1/haxis= axis1 vaxis= axis2 frame href=0 vref=0 annotate=coor;
run;
