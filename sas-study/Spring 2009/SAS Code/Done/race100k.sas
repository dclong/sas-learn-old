
/* This program is posted as race100k.sas
   It computes scatter principal components
   for the 100k road race data (Everitt 1994). 
   The data are posted as
 
                  race100k.dat
 
   There is one line of data for each of 80 
   racers with eleven numbers on each line.  
   The first ten columns give the times (minutes)
   to complete successive 10k segments of the race.  
   The last column has the age (in years) of the 
   racer.  */

options nodate nonumber linesize=64 nocenter;

Filename race100k url "http://streaming.stat.iastate.edu/~stat501/data/race100k.dat";

data set1;
     infile race100k;
     input x1-x10 age;
     newage = 'M';
     if(age > 40) then newage='S';
run;

proc print data=set1;
run;

/* Compute estimates of principal 
   components from the sample covariance
   matrix and plot the scores    */

proc princomp data=set1 cov n=3 out=scores prefix=prin;
     var x1-x10;
run;

proc print data=scores;
run;

proc plot data=scores;
     plot prin1*prin2=newage/ hpos=56;
     plot prin1*prin3=newage/ hpos=56;
run;

proc corr data=scores;
     var prin1 prin2 prin3 x1-x10 age;
run;

/*  Compute estimates of principal 
    components from the sample correlation 
    matrix and plot the scores      */

proc princomp data=set1 n=3 out=pcorr prefix=pcorr;
     var x1-x10;
run;

proc print data=pcorr; 
run;

proc plot data=pcorr;
     plot pcorr1*pcorr2=newage / hpos=56;
     plot pcorr1*pcorr3=newage / hpos=56;
run;
