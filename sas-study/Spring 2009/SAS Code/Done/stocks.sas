/*  This program is posted as  stocks.sas
    It performs factor analyses for the 
    weekly stock return data from Johnson 
    & Wichern.  The data are posted as
    stocks.dat  
*/
filename stock url "http://streaming.stat.iastate.edu/~stat501/data/stocks.dat";

data set1;
     infile stock;
     input x1-x5;
     label x1 = ALLIED CHEM
           x2 = DUPONT
           x3 = UNION CARBIDE
           x4 = EXXON
           x5 = TEXACO;
run;

/* Print the data file  */

proc print data=set1;
run;

title 'FACTOR ANALYSIS OF STOCK PRICES';

/* Compute principal components and 
   do a varimax rotation */

proc factor data=set1 method=prin scree  nfactors=5 mineigen=0
     			simple corr ev nplot=2 res msa out=scorepc  outstat=facpc;
     var x1-x5;
run;

proc print data=scorepc; 
run;

proc print data=facpc; 
run;


/* Use information on the principal 
   components computed by PROC FACTOR 
   to rotate the first 2 components */

proc factor data=facpc n=2 rotate=varimax plot outstat=facpc2 score round reorder;
run;

/*  Compute and plot the scores for 
    the two rotated components */

proc score data=set1 score=facpc2 out=scorepc2;  
run;

goptions cback=white colors=(black) targetdevice=ps rotate=portrait;

axis1 label=(f=swiss h=3.0 )
      ORDER = -3 to 3 by 1
      value=(f=triplex h=3.0)
      length= 5.5 in;

axis2 label=(f=swiss h=3.1 r=0 a=90)
      order = -3 to 3 by 1
      value=(f=triplex h=3.0)
      length = 5.5 in;

SYMBOL1 V=circle H=2.5 l=1 w=3 i=none;
      
proc gplot data=scorepc2;
     plot factor2*factor1 / vaxis=axis2 haxis=axis1;
     title1 ls=0.3in h=4.0 f=swiss "Factor Scores";
     title2 h=3.0 f=swiss "Stock Market Data";
     label factor1='Market Factor ';
     label factor2 = 'Chemical vs Oil Stocks';
     footnote ls=0.4in '  ';
RUN;


/* Compute  2  factors with the iterated 
   principal factor method and rotate the 
   factors with a varimax rotation */

proc factor data=set1 method=prinit nfactors=2 nplot=2 res msa
     			maxiter=75 conv=.0001 outstat=facpf rotate=varimax  reorder;
     var x1-x5;
     priors smc;
run;


/*  Use maximum likelihood estimation */

proc factor data=set1 method=ml nfactors=1 res msa maxiter=75 
             conv=.0001 outstat=facml1;
     var x1-x5;
     priors smc;
run;

proc factor data=set1 method=ml nfactors=2 res msa mineigen=0 maxiter=75 
             conv=.0001 outstat=facml2 rotate=varimax reorder;
     var x1-x5;
     priors smc;
run;

proc factor data=set1 method=prin nfactors=3 mineigen=0 res msa
             maxiter=75 conv=.0001 outstat=facml3 rotate=varimax reorder;
     var x1-x5;
     priors smc;
run;

/* Apply other rotations to the two
   factor solution  */

proc factor data=facml2 nfactors=2 conv=.0001 rotate=quartimax reorder;
     var x1-x5;
     priors smc;
run;

proc factor data=facml2 nfactors=2 conv=.0001 rotate=promax reorder score;
     var x1-x5;
     priors smc;
run;


