
/*  This program performs cluster analysis on the Stanford
    diabetes data.  The code is posted as diabetes.sas
	and the data are posted as in the data folder as diabetes.dat

    OGTT refers to a three hour oral glucose tolerance test  */


DATA SET1;
  	INFILE 'c:/stat501/data/diabetes.dat';
  	INPUT patient x1-x5;
/*  LABEL  x1 = 'relative weight'
           x2 = 'fasting plasma glucose'
           x3 = 'area under the plasma glucose curve (OGTT)'
           x4 = 'area under plasma insulin curve (OGTT)'
           x5 = 'steady state plasma glucose (SSPG)';           */
run;


/* First standardize each variable to have mean zero and
   unit standard deviation                               */

proc standard data=set1 out=set2 mean=0 std=1 ;
   	var x1-x5;
run;

/*  This is a SAS macro for hierarchical cluster analysis.
    It uses PROC CLUSTER followed by PROC TREE to produce
    a file, callled OUT, containing the original data and
    a variable called CLUSTER that defines the clusters.
    PROC CANDISC provides additional information about the
    clusters as well as 2-dimensional projections.  To apply
    this macro to other data you will need to change the
    data set name, the list of variables in the  VAR 
    statement, the name of the subject identification
    variable in the ID statement, the title in the TITLE
    statement. */


%MACRO ANALYZE(METHOD,NCL);
   PROC CLUSTER DATA=SET2 OUT=TREE METHOD=&METHOD P=35 ccc STANDARD;
   VAR X1-X5;
   ID patient;
   TITLE "Stanford Diabetes Data";
   TITLE2;
   %LET K=1;
   %LET N=%SCAN(&NCL,&K);
   %DO %WHILE(&N NE);

/* Compute the tree and create a file with cluster labels */

PROC TREE DATA=TREE NOPRINT OUT=OUT NCL=&N;
  COPY PATIENT x1-x5;
   TITLE2 "COMPUTE &N CLUSTERS USING METHOD=&METHOD";
PROC SORT DATA=OUT; BY CLUSTER;

/* Delete the following line to avoid printing the 
   data with a column of cluster labels            */

   proc print data=out; run;                                   

/*  Compute canonical discriminants and display clusters */

PROC CANDISC data=out simple OUT=CAN ncan=5 prefix=can;
     CLASS cluster;
     VAR x1-x5;
     run;

PROC PLOT;
     PLOT CAN2*CAN1=CLUSTER/HPOS=56;
     PLOT CAN3*CAN1=CLUSTER/HPOS=56;
     TITLE2 "PLOT OF &N CLUSTERS FROM METHOD=&METHOD";
     RUN;
%LET K=%EVAL(&K+1);
%LET N=%SCAN(&NCL,&K);
%END;
%MEND;


/*  Run the macro for several methods        */

   %ANALYZE(WARDS, 7 10)
   %ANALYZE(centroid, 7)
   %ANALYZE(complete, 7)
   %ANALYZE(average, 7)
   %ANALYZE(single, 7)
