
/*  This program is posted as  chbones.sas
    It applies PROC FACTOR to the estimated 
    correlation matrix for the chicken bone data 
     The sample size is n=276. The variables are
       X1  =  skull length
       X2  =  skull breadth
       X3  =  femur length
       X4  =  tibia length
       X5  =  humerus length
       X6  =  ulna length    */

/*  Enter the data as a TYPE=CORR data set */

DATA chbones(TYPE=CORR);
     INPUT _TYPE_ $ _NAME_ $ X1 X2 X3 X4 X5 X6;
     datalines;
  CORR  X1  1.000 0.505 0.569 0.602 0.621 0.603
  CORR  X2  0.505 1.000 0.422 0.467 0.482 0.450
  CORR  X3  0.569 0.422 1.000 0.926 0.877 0.878
  CORR  X4  0.602 0.467 0.926 1.000 0.874 0.894
  CORR  X5  0.621 0.482 0.877 0.874 1.000 0.937
  CORR  X6  0.603 0.450 0.878 0.894 0.937 1.000
     N   N    276   276   276   276   276   276
run;
 
PROC PRINT data=chbones;
     title "Factor Analyis of Chicken Bone Data";
run;

/*  Compute factors and apply a varimax rotation */

PROC FACTOR DATA=chbones(TYPE=CORR) METHOD=ml N=2 rotate=varimax residuals;
     var X1-X6;
     priors smc;
run;

/*  Compute factors and apply a varimax rotation */

PROC FACTOR DATA=chbones(TYPE=CORR) METHOD=ml N=2 rotate=varimax residuals  heywood outstat=VarStat;
     var X1-X6;
     priors smc;
run;

proc print data=VarStat;
run;

data VLoading;
     set VarStat;
	 if _type_ eq "PATTERN";
	 if _name_ eq "Factor1" then _name_ = "VF1";
	 else _name_ ="VF2";
RUN;

proc print data=Vloading;
run;

proc transpose data=VLoading out=VLoading2;
run;

proc print data=VLoading2;
run;

proc factor data=chbones(type=corr) method=ml n=2 rotate=promax residuals heywood outstat=ProStat;
     var x1-x6;
	 priors smc;
run;

proc print data=ProStat;
run;

data PLoading;
     set ProStat;
	 where _type_ eq "STRUCTUR";
	 if _name_ eq "Factor1" then _name_ = "PF1";
	 else _name_="PF2";
run;

proc print data=PLoading;
run;

proc transpose data=PLoading out=PLoading2;
     var x1-x6;
run;

proc print data=PLoading2;
run;

data ALoadings;
     merge VLoading2 PLoading2;
run;

proc print data=ALoadings;
run;

proc gplot data=ALoadings;
     plot VF2*VF1 PF2*pf1/overlay legend=legend1 haxis=axis1 vaxis=axis2;
	 goptions reset=all;
	 symbol1 v=circle c=red h=1.5 w=2;
	 symbol2 v=triangle c=blue h=1.5 w=2;
	 axis1 label=(f="Arial" c=magenta h=5pct "Factor1") c=cyan width=3 minor=none
               value=(f="Arial" c=red h=3pct);
     axis2 label=(f="Arial" c=magenta h=5pct "Factor2") c=cyan width=3 minor=none
               value=(f="Arial" c=red h=3pct); 
	 legend1 down=2 position=(top left inside) cshadow=black frame
	           value=(f="Arial" h=12pt t=1 "Varimax" t=2 "Promax")
			   label=(f="Arial" h=2 "Method");
	 title f="Arial" c=black h=18pt "Factor plot of Varimax and Promax";
run;
