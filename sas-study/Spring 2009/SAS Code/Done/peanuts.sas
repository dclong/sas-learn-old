
 /*  This program performs a two-way MANOVA
     on the peanut data posted as peanuts.dat
     The code is posted as peanuts.sas  */
filename peanut url "http://streaming.stat.iastate.edu/~stat501/data/peanuts.dat";

options linesize=64 nocenter nonumber ;

data set1;
     infile peanut;
     INPUT location variety x1 x2 x3;
  /* LABEL group = student group
              x1 = yield
              x2 = SdMatKer
              x3 = SeedSize; */
run;

PROC PRINT data=set1;  
run;

PROC GLM DATA=set1;
     CLASS location variety;
     MODEL x1-x3 = location variety location*variety / P SOLUTION;
     MANOVA H=location*variety /PRINTH PRINTE;
     MANOVA H=variety / printH printE;
     MANOVA H=location / printH printE;
     Repeated traits 3 profile / printm;
run;

