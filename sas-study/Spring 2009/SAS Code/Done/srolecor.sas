
/*  This program is posted as  srolecor.sas
    It performs a simple correspondence analysis for the
    mental health status data collected by Srole, et. al. and
    analyzed by Goodman (see Agresti section 8.5.2) */


/*  Enter the observed counts */

data set1;
 	input parents $ well mild moderate impaired;
 	cards;
 	A 64 94 58 46
 	B 57 94 54 40
 	C 57 105 65 60
 	D 72 141 77 94
 	E 36 97 54 78
 	F 21 71 54 71
run;


/*  Output the table as a matrix with one column and retain the
    column labels for the original table as the variable  'health'  */

data set2;
 	set set1;
 	health='    Well'; y=well; output;
 	health='    Mild'; y=mild; output;
 	health='Moderate'; y=moderate; output;
 	health='Impaired'; y=impaired; output;
 	keep parents health y;
run;


/*  Display the original continency table  */

proc freq data=set2 order=data;
  	tables parents*health / chisq;
  	weight y;
run;

/*  Do the correspondence analysis and output results to the
    temporary SAS file called 'results'                     */

proc corresp data=set1 out=results cp rp dimens=3 profile=both short  ;
     var well mild moderate impaired;
     id parents;
run;

proc print data=results;
run;

/*  Plot the results with labels  */

proc plot data=results vtoh=2;
   	plot dim2*dim1='*' $ parents  /box href=0 vref=0;
   	plot dim3*dim1='*' $ parents  /box href=0 vref=0;
run;


/* Create plots with higher resolution using SASGraph  */

goptions colors=(black) cback=white targetdevice=ps;

data cplot; 
	set results;
  	if( _type_ = 'INERTIA') then delete;
  	x = dim1;
  	y = dim2;
  	label y = 'Factor 1'
          x = 'Factor 2';
    text = parents;
  	xsys='2';
  	ysys='2';
  	size = 1.5;
  	keep x y text xsys ysys size;
run;

proc gplot data=cplot;
  	symbol1 v=point;
  	axis1 length = 6 in order = -.3 to .3 by .1;
    axis2 length = 6 in order = -.3 to .3 by .1;
  	plot y*x = 1 / haxis=axis1 vaxis=axis2 frame href=0 vref=0 annotate=cplot;
  	title1 'Associations Between Mental Health Status' h=2.5 f=swiss;
  	title2 'and Socioeconomic Status' h=2.5 f=swiss;
run;
