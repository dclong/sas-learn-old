
/*  This program is posted as  spearman.sas
    It applies PROC FACTOR to the estimated 
    correlation matrix for the Spearman data 
    The variables are test scores for :

      

/*  Enter the data as a TYPE=CORR data set */

DATA spearman(TYPE=CORR);
  	INPUT _TYPE_ $ _NAME_ $ C F E M D Mu;
    datalines;
	CORR  C  1.00 0.83 0.78 0.70 0.66 0.63
	CORR  F  0.83 1.00 0.67 0.67 0.65 0.57
	CORR  E  0.78 0.67 1.00 0.64 0.54 0.51
	CORR  M  0.70 0.67 0.64 1.00 0.45 0.51
	CORR  D  0.66 0.65 0.54 0.45 1.00 0.40
	CORR  Mu 0.63 0.57 0.51 0.51 0.40 1.00
   	N  N    33   33   33   33   33   33
run;
 


PROC PRINT data=spearman;
     title "Factor Analyis of Spearman Data";
run;


/*  Compute one factor */

PROC FACTOR DATA=spearman(TYPE=CORR) METHOD=ml N=1;
     var C F E M D Mu;
     priors smc;
run;
