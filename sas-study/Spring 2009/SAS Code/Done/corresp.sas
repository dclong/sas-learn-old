


/*  This program is posted as  corresp.sas
    It performs a simple correspondence analysis for the work
    activity/benefits contingency table discussed in the lectures.
    The table of counts is posted in the file  corresp.dat  */

/*  Enter the observed counts */

data set1;
	infile "corresp.dat";
 	input work $ varw free huma sche sala secu comp inte near atmo soci
           		auto like othe none outd noa node grad smal medi larg;
run;

proc print data=set1;
run;
/*  Output the table as a matrix with one column and retain the
    column labels for the original table as the variable  'job'  */

data x2;
 	set set1;
 	job='varw';y=varw;output;
 	job='free';y=free;output;
 	job='huma';y=huma;output;
 	job='sche';y=sche;output;
 	job='sala';y=sala;output;
 	job='secu';y=secu;output;
 	job='comp';y=comp;output;
 	job='inte';y=inte;output;
 	job='near';y=near;output;
 	job='atmo';y=atmo;output;
 	job='soci';y=soci;output;
 	job='auto';y=auto;output;
 	job='like';y=like;output;
 	job='othe';y=othe;output;
 	job='none';y=none;output;
 	job='outd';y=outd;output;
 	job='noa ';y=noa ;output;
 	keep work job y;
run;

proc print data=x2;
run;

/*  Display the original continency table  */

proc freq data=x2;
  	tables work*job / chisq;
  	weight y;
run;


/*  Do the correspondence analysis and output results to the
    temporary SAS file called 'results'                     */

data france;
  	set set1;
run;

proc corresp data=france out=results cp rp dimens=3 profile=both short  ;
     var      varw free huma sche sala secu comp inte near atmo soci auto like
           othe none outd noa node grad smal medi larg;
     supplementary node grad smal medi larg;
     id  work;
run;

proc print data=results;
run;

proc plot data=results vtoh=2;
   plot dim2*dim1='*' $ work  /box href=0 vref=0;
   plot dim3*dim1='*' $ work  /box href=0 vref=0;
   plot dim3*dim2='*' $ work  /box href=0 vref=0;
run;
