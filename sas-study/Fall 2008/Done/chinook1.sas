

/*  Program to analyze the 1999 Chinook
    salmon data.  This program is stored
    in the file

              chinook1.sas        */

options linesize=64 nodate nonumber;

data set1;
  	infile 'C:\Documents and Settings\dclong\My Documents\SAS Study\Fall 2008\SAS Code\Data\hdata.dat';
  	input (year month day biweek run gear age sex length)
        (4. 2. 2. 1. 1. 1. 2. $1. 4.);
	rage=int(age/10);
	oage=age-(10*rage);
run;


proc sort data=set1; 
	by gear run;
run;

proc freq data=set1; 
	by gear run;
  	table sex / binomial (p=.5);
run;
