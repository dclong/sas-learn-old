
/* Program to analyze data for problem 1
   on assignment 10 in 2006.             */


data set1;
	input A B y1-y4;
  	array c(i) y1-y4;
    do over c;
    	subject = i;
     	y = c;
     	output;
   	end;
  	keep a b subject y;
	cards;
1 1 2.4 2.7 2.3 2.5
1 2 4.6 4.2 4.9 4.7
1 3 4.8 4.5 4.4 4.6
2 1 5.8 5.2 5.5 5.3
2 2 8.9 9.1 8.7 9.0
2 3 9.1 9.3 8.7 9.4
3 1 6.1 5.7 5.9 6.2
3 2 9.9 10.5 10.6 10.1
3 3 13.5 13.0 13.3 13.2
run;


proc glm data=set1;
  	class a b;
  	model y = a b a*b / solution clm;
  	lsmeans a*b / stderr pdiff;
run;

proc sort data=set1; 
	by a b;
run;

proc means data=set1 noprint; 
	by a b;
  	var y;
  	output out=means mean=my;
run;


 goptions cback=white colors=(black)
    targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=2.5)
      ORDER = 1 to 3 by 1 
      offset=(1cm, 1cm)
      value=(f=swiss h=2.0)
      w=3.0 length= 6.5 in;

axis2 label=(f=swiss h=2.0)
      order = 2 to 14 by 2
      value=(f=swiss h=2.0)
      w= 3.0 length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.0 w=3 l=1 i=join ;
       SYMBOL2 V=square H=2.0 w=3 l=3 i=join ;
       SYMBOL3 V=diamond H=2.0 w=3 l=17 i=join ;


  PROC GPLOT DATA=means;
    PLOT my*a=b / vaxis=axis2 haxis=axis1;
    title1  H=3.0 F=swiss "Hayfever Relief";
       LABEL my='Hours';
       LABEL a = 'Factor A';
   RUN;
