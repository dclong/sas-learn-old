

options nonumber nosummary nodate linesize=64;

data set1;
	infile "C:\Documents and Settings\dclong\My Documents\SAS Study\Fall 2008\Data\biomass.dat";
  	input loc $ type $ biomass x1-x5;
  	label x1 = sal
       	  x2 = ph
       	  x3 = K
          x4 = Na
          x5 = Zn;		
run;

/* Print the data file  */

proc print data=set1;
run;

title 'Biomass Analysis';


/* Compute correlations  */

proc corr data=set1;
	var biomass x1-x5; 
run;
  

/* Fit regression models  */

proc reg data=set1;
	model biomass = x1-x5 /vif collin partial;
    output out=set1 residual=resid predicted=yhat; 
run;


proc rank data=set1 out=set1 normal=blom;
	var resid; 
	ranks q; 
run;

goptions cback=white colors=(black)
     device=win target=winprtc rotate=portrait;

  axis1 label=(h=2.5 r=0 a=90 f=swiss 'Residuals')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  axis2 label=(h=2.3 f=swiss 'Standard Normal Quantiles')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  axis3 label=(h=2.3 f=swiss 'Predicted Biomass')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  symbol1 v=circle i=none h=2 w=3 c=black;

proc gplot data=set1;
  plot resid*q / vaxis=axis1 haxis=axis2;
  title h=3.0 ls=1.0in f=swiss 
        c=black 'Normal Probability Plot';
  footnote ls=0.6in '  ';
  run;

proc gplot data=set1;
  plot resid*yhat / vaxis=axis1 haxis=axis3;
  title h=3.0 ls=1.0in f=swiss 
        c=black 'Residual Plot';
  footnote ls=0.6in '   ';
  run;


  
proc reg data=set1;
 	model biomass = x1-x5 / selection= rsquare
          cp aic sbc mse best=5 stop=5; 
run;
