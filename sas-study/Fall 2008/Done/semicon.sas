/*  Program to analyze an unreplicated factorial
    experiment where each factor has two levels.
    This program is posted as  process.sas
    It is applied to to the process development
    data from Box, Hunter, and Hunter.           */

Data set1;
  	input a b c d e y ;
  	ab=a*b; 	
	ac=a*c; 
	ad=a*d; 
	ae=a*e; 
  	bc=b*c; 
	bd=b*d; 
	be=b*e; 
	cd=c*d;
  	ce=c*e; 
	de=d*e;
  	abc=a*b*c; 
	abd=a*b*d; 
	abe=a*b*e;
  	acd=a*c*d; 
	ace=a*c*e; 
	ade=a*d*e;
  	bcd=b*c*d; 
	bce=b*c*e; 
	bde=b*d*e; 
  	cde=c*d*e; 
	abcd=a*b*c*d; 
	abce=a*b*c*e;
  	abde=a*b*d*e; 
	acde=a*c*d*e; 
  	bcde=b*c*d*e; 
	abcde=a*b*c*d*e;
	datalines;
-1 -1 -1 -1 -1  7
 1 -1 -1 -1 -1  9
-1  1 -1 -1 -1 34
 1  1 -1 -1 -1 55
-1 -1  1 -1 -1 16
 1 -1  1 -1 -1 20
-1  1  1 -1 -1 40
 1  1  1 -1 -1 60
-1 -1 -1  1 -1  8
 1 -1 -1  1 -1 10
-1  1 -1  1 -1 32
 1  1 -1  1 -1 50
-1 -1  1  1 -1 18
 1 -1  1  1 -1 21
-1  1  1  1 -1 44
 1  1  1  1 -1 61
-1 -1 -1 -1  1  8
 1 -1 -1 -1  1 12
-1  1 -1 -1  1 35
 1  1 -1 -1  1 52
-1 -1  1 -1  1 15
 1 -1  1 -1  1 22
-1  1  1 -1  1 45
 1  1  1 -1  1 65
-1 -1 -1  1  1  6
 1 -1 -1  1  1 10
-1  1 -1  1  1 30
 1  1 -1  1  1 53
-1 -1  1  1  1 15
 1 -1  1  1  1 20
-1  1  1  1  1 41
 1  1  1  1  1 63
run;


/* Print the data file. */

proc print data=set1;
run;


/*  Fit the regression model that produces values
    of orthogonal contrasts as regression coefficients.
    The least squares estimates of the regression
    coefficients are written to a temporary file  */

proc reg data=set1 outest=parms;
  	model y = a b c d e ab ac ad ae bc bd be
            	cd ce de abc abd abe acd ace ade
            	bcd bce bde cde abcd abce abde 
            	acde bcde abcde;
run;


/*  Print the file containing the estimates of the
    regression coefficients                        */

proc print data=parms;
run;


/*  Create a normal probability plot for the estimates
    of the regression coefficients.  First transpose
    the file.  */

proc transpose data=parms out=effects prefix=value name=effect;
  	var a b c d e ab ac ad ae bc bd be
            cd ce de abc abd abe acd ace ade
            bcd bce bde cde abcd abce abde 
            acde bcde abcde;
run;

proc print data=effects;
run;

proc rank data=effects normal=blom out=effects;
  	var value1;
  	ranks q;
run;

 /*  Create a normal probability plot  */

goptions cback=white colors=(black) targetdevice=ps;

axis1 label=(f=swiss h=2.0) ORDER = -3 to 3 by 1
      value=(f=swiss h=1.8) w=3.0  length= 5.5 in;

axis2 label=(f=swiss h=2.0 a=90)  order = -1 to 17 by 2
      value=(f=swiss h=1.8)  w=3.0  length = 5.5 in;

       SYMBOL1 V=CIRCLE H=2.0 i=none ;

   PROC GPLOT DATA=effects;
      PLOT value1*q  / overlay vaxis=axis2 haxis=axis1;
       title  h=2.8 f=swiss "Normal Probability Plot";
       LABEL q='Normal Scores';
       LABEL value1 = 'Effects';
   RUN;


/*  Fit a model containing only the main effects
    and the significant interactions identified
    in the normal probability plot.            */


proc glm data=set1;
  	class a b c d e;
  	model y = a b c d e a*b / p clm ss1;
  	lsmeans a /pdiff stderr;
  	lsmeans b /pdiff stderr;
  	lsmeans c /pdiff stderr;
  	lsmeans d /pdiff stderr;
  	lsmeans e /pdiff stderr;
  	lsmeans ab /pdiff stderr;
  	output out=setr predicted=yhat residual=resid;
run;


proc univariate data=setr normal;
  	var resid; 
run;

proc rank data=setr normal=blom out=setr;
  	var resid;
  	ranks q;
run;

 /*  Create a normal probability plot  */

goptions cback=white colors=(black) targetdevice=ps;

axis1 label=(f=swiss h=2.0)
      value=(f=swiss h=1.8) w=3.0  length= 5.5 in;

axis2 label=(f=swiss h=2.0 a=90) 
      value=(f=swiss h=1.8)  w=3.0  length = 5.5 in;

       SYMBOL1 V=CIRCLE H=2.0 i=none ;

   PROC GPLOT DATA=setr;
      PLOT resid*q  / overlay vaxis=axis2 haxis=axis1;
       title  h=2.8 f=swiss "Normal Probability Plot";
       LABEL q='Normal Scores';
       LABEL resid = 'Residuals';
   RUN;

 PROC GPLOT DATA=setr;
      PLOT resid*yhat  / overlay vaxis=axis2 haxis=axis1;
       title  h=2.8 f=swiss "Residual Plot";
       LABEL yhat='Estimated means';
       LABEL resid = 'Residuals';
   RUN;
