


/*  Analysis of blood glucose data
    from problem 1 on assignment 1  */

data setg;
 	 infile "c:\\st500\hw1f06p1.txt";
  	input status $11.0 glucose;
run;


proc sort data=setg; 
	by status; 
run;

proc print data=setg; 
run;

proc univariate data=setg normal;
   	by status;  
	var glucose; 
run;

proc boxplot data=setg;
  	plot glucose*status / boxstyle=schematic;
  	insetgroup n mean q1 q2 q3 / font=swiss  pos=topoff;
  	title 'Results of Glucose Tolerence tests';
run;
