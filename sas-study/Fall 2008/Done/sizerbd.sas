
  /*  This program uses PROC IML in SAS to compute
      the number of blocks for balanced randomized block
      designs with  a  treatments and  n  observations
      for each treatment within each block. It is posted
      in the file
                      sizerbd.sas
  */

   proc iml;
     start sizerbd;

     alpha = .05;       /* specify the type I error level */
     power = .90;       /* specify the power of the test */
     beta = 1 - power;  /* Compute the type II error level */
     n = 1;             /* Enter the number of observations for
                           each treatment within each block  */

                        /* specify sizes of effects to be detected
                           divided by the standard deviation for
                           one observation.  Enter one value
                           for each treatment and
                           separate the values with commas   */

     d = {-1.0, 1.0, 0, 0};

     a = nrow(d);       /* Compute the number of treatments */

     ssd = ssq(d);      /* Compute the sum of the squared elements
                           of the vector  d  */

     b = 1;             /* initialize the number of blocks */
     p = 1;

                        /* Find the number of blocks, the largest
                           value it will return is b=1001  */

     do i = 1 to 1000 while (p > beta);
          b = b + 1;
        ndf = a-1;
        ddf = n*a*b - a - b + 1;
          f = finv(1-alpha, ndf, ddf);
         nc = n*b*ssd;
          p = probf(f, ndf, ddf, nc);
     end;



     print   "Number of Observations Required for Each Factor Level" ;
     print   "           Significance level:  "  alpha;
     print   "                        power:  "  power;
     print   "         Number of treatments:  "  a;
     print   "Number of times each treatment",
             "        is used in each block:  "  n;
     print   "               Scaled effects:  "  d;
     print   "             Number of blocks:  "  b;


    finish;

    run sizerbd;
