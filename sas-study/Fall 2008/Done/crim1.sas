

/*  This program is stored in 
    the file  crim1.sas  */

/*  Enter the data as counts 
    in a 2-dimensional table */

options nodate nonumber linesize=64;

data set1;
	input response year count;
  	cards;
1 1 105
1 2  68
1 3  42
1 4   61
2 1  265
2 2  196
2 3   72
2 4  144
3 1 1066
3 2 1092
3 3  580
3 4 1174
4 1  173
4 2  138
4 3   51
4 4  104
5 1    4
5 2   10
5 3    8
5 4    7
run;

/*  Use PROC FORMAT to assign labels 
    to the row and column categories */

proc format;
	value rowfmt 1 = 'Too harshly'
                 2 = 'About right'
                 3 = 'Too lenient'
                 4 = 'No opinion'
                 5 = 'No answer';
  	value colfmt 1 = '1972'
                 2 = '1973'
                 3 = '1974'
                 4 = '1975';
run;


/*  Compute test for independence  */

title 'Annual Opinions on Treatment of Criminals';

proc freq data=set1;
	table response*year / chisq expected cellchi2;
  	weight count;
  	format response rowfmt.
           year colfmt.;
run;

/*  Compare responses for each 
    pair of years without printing 
    tables of counts and proportions */

data set2; 
	set set1;
    if(year=1 or year=2);
    title 'Comparison Between 1972 and 1973';
run;

proc freq data=set2;
	table response*year / chisq noprint;
  	weight count;
run;

data set2; 
	set set1;
    if(year=1 or year=3);
    title 'Comparison Between 1972 and 1974';
run;

proc freq data=set2;
	table response*year / chisq noprint;
  	weight count;
run;

data set2; 
	set set1;
    if(year=1 or year=4);
    title 'Comparison Between 1972 and 1975';
run;


proc freq data=set2;
	table response*year / chisq noprint;
	weight count;
run;

data set2; 
	set set1;
 	if(year=2 or year=3);
    title 'Comparison Between 1973 and 1974';
run;

proc freq data=set2;
	table response*year / chisq noprint;
    weight count;
run;

data set2; 
	set set1;
    if(year=2 or year=4);
    title 'Comparison Between 1973 and 1975';
run;

proc freq data=set2;
	table response*year / chisq noprint;
    weight count;
run;

data set2; 
	set set1;
    if(year=3 or year=4);
    title 'Comparison Between 1974 and 1975';
run;

proc freq data=set2;
	table response*year / chisq noprint;
    weight count;
run;
