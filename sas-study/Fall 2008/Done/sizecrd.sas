
  /*  This program uses PROC IML in SAS to
      compute sample sizes for completely randomized
      designs involving one factor with  r  levels.

      It is posted in the SAS folder of the course 
      web page as sizecrd.sas */

   proc iml;
     start sizecrd;

     alpha = .05;       /* specify the type I error level */
     power = .90;       /* specify the power of the test */
     beta = 1 - power;  /* Compute the type II error level */
                        /* specify sizes of effects to be detected
                           divided by the standard deviation for
                           one observation.  Enter one value
                           for each level of the factor  and
                           separate the values with commas   */

     d = {-0.5, 0.5, 0, 0, 0};

     r = nrow(d);       /* Compute the number of levels */

     ssd = ssq(d);      /* Compute the sum of the squared elements
                           of the vector  d  */

     n = 1;             /* initialize the sample size */
     p = 1;

                        /* Find the sample size, the largest
                           value it will return is n=1001  */

     do i = 1 to 1000 while (p > beta);
          n = n + 1;
        ndf = r-1;
        ddf = r*(n-1);
          f = finv(1-alpha, ndf, ddf);
         nc = n*ssd;
          p = probf(f, ndf, ddf, nc);
     end;



     print   "Number of Observations Required for Each Factor Level" ;
     print   "           Significance level:  "  alpha;
     print   "                        power:  "  power;
     print   "      Number of factor levels:  "  r;
     print   "               Scaled effects:  "  d;
     print   "                  Sample size:  "  n;


    finish;

    run sizecrd;
