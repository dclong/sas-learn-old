
proc print data=sasuser.three;
run;

proc sql;
	select *
		from sasuser.three,sasuser.four
		where three.x=four.x;
quit;

proc sql;
	select t.x,t.A,f.B
		from sasuser.three t, sasuser.four f
		where t.x=f.x;
quit;

proc sql;
	select *
		from sasuser.three t1,sasuser.three t2
		where t1.x=t2.x;
quit;

proc sql;
	select *
		from sasuser.one full join sasuser.two
		on one.x=two.x;
quit;

proc sql;
	select two.x,A,B
		from sasuser.one right join sasuser.two
		on one.x=two.x;
quit;

proc sql;
	select two.x,A,B
		from sasuser.one right join sasuser.two
		on one.x=two.x
		where two.x>2;
quit;

proc sql;
	select *
		from sasuser.one inner join sasuser.two
		on one.x=two.x;
quit;

proc print data=sasuser.two;
run;

proc print data=sasuser.six;
run;

data sasuser.five;
	input x A $;
	datalines;
	1 a
	2 b
	3 c
run;

data sasuser.six;
	input x B $;
	datalines;
	1 x
	2 y
	3 z
run;

proc sql;
	select five.x,a,b
	from sasuser.five,sasuser.six
		where five.x=six.x;
run;

data merged;
	merge sasuser.one sasuser.two;
	by x;
run;

proc print data=merged;
run;

proc print data=sasuser.three;
run;

proc sql;
	select coalesce(one.x,two.x) as x,a,b
		from sasuser.one full join sasuser.two
		on one.x=two.x;
quit;

proc print data=sasuser.eight;
run;

data sasuser.seven;
	input x A $;
	datalines;
	1 a
	1 a
	1 b 
	2 c
	3 v
	4 e
	6 g
run;

data sasuser.eight;
	input x B $;
	datalines;
	1 x
	2 y
	3 z
	3 v 
    5 w
run;

proc sql;
	select count(*) as NO
		from
	(select seven.x,a
		from sasuser.seven
	except corr	all
	select eight.x,b
		from sasuser.eight);
quit;

proc sql;
	select seven.x,a
		from sasuser.seven
	intersect all
	select eight.x,b
		from sasuser.eight;
quit;

proc sql;
	select seven.x,a
		from sasuser.seven
	intersect corr
	select eight.x,b
		from sasuser.eight;
quit;

proc sql;
	select seven.x,a
		from sasuser.seven
	intersect corr all
	select eight.x,b
		from sasuser.eight;
quit;

proc sql;
	select seven.x,a
		from sasuser.seven
	union
	select eight.x,b
		from sasuser.eight;
quit;

proc sql;
	select seven.x,a
		from sasuser.seven
	union corr all
	select eight.x,b
		from sasuser.eight;
quit;


proc sql;
	select seven.x,a
		from sasuser.seven
	outer union corr
	select eight.x,b
		from sasuser.eight;
quit;

data set1;
	set sasuser.seven sasuser.eight;
run;

proc print data=set1;
run;
























