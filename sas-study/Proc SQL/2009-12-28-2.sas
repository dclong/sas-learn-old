
proc print data=sasuser.bottle;
run;


proc sql;
	select model,
		case model
			when 1 then "First"
			when 2 then "Second"
			when 3 then "Third"
		end
		from sasuser.bottle;
quit;

proc print data=one;
run;

data one;
	set sasuser.one;
run;

proc sql;
	delete from one;
	select * 
		from one;
quit;

proc sql;
	describe table one;
quit;

data two;
	set sasuser.two;
run;

proc print data=two;
run;

proc sql;
	alter table two
		add Bonus num format=comma10.2,
			level char(3);
	select *
		from two;
quit;

proc sql;
	alter table two
		add New num;
	select *
		from two;
quit;

proc sql;
	alter table two
		drop new,bonus,level;
	select *
		from two;
quit;

proc sql;
	alter table two
		modify x format=dollar11.2 label="new";
quit;

proc print data=two;
run;

proc sql;
	select *
		from two;
quit;

proc sql;
	alter table two
		add bonus num format=dollar11.2
		modify x format=date9.
		drop b;
	select *
		from two;
quit;

proc sql;
	drop table two;
quit;
