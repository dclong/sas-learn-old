
proc sql;
	create table set1
		(drop=day2 day3 day4)
		like sasuser.bottle;
quit;

proc sql;
	describe table set1;
quit;

proc sql;
	create table set2
		(keep=model machine day1)
		like sasuser.bottle;
quit;

proc sql;
	describe table set2;
quit;

proc sql;
	create table set3
		(ID char(5) primary key,
		name char(10) ,
		Gender char (1) not null check(gender in ("M","F")),
		HDate date label="Hire Date");
quit;

proc sql;
	create table set4
		(ID char(5) primary key,
		day1 num,
		day2 num check (day2>day1));
quit;

proc sql;
	describe table set4;
quit;

proc sql;
	insert into set4
		set ID="ID00",
			day1=2.3,
			day2=32;
quit;

proc print data=set4;
run;

proc sql undo_policy=none;
	create table set5
		(Destination char(3),
		BeginDate num format=date9.,
		EndDate num format=date9.,
		Discount num,
		constraint ok_discount check (discount le 0.5),
		constraint notnull_dest not null (destination));
quit;

proc sql;
	describe table set5;
quit;

proc sql; 
	insert into set5
		values("CDG","03mar2000"d,"10mar2000"d,.15)
		values("LHR","10mar2000"d,"12mar2000"d,.55);
quit;

proc print data=set5;
run;

proc sql;
	describe table set5;
quit;

proc sql;
	describe table constraints set5;
quit;

proc print data=sasuser.bottle;
run;

proc sql;
	update sasuser.bottle
		set day1=20
		where model=1;
	select *
		from sasuser.bottle;
quit;

proc sql;
	update sasuser.bottle
		set day2=0;
	select *
		from sasuser.bottle;
quit;

proc print data=sasuser.bottle;
run;

proc sql;
	update sasuser.bottle
		set day3=0
			where model=1;
quit;

proc sql;
	update sasuser.bottle
		set day3=1
			where model=1;
	update sasuser.bottle
		set day3=2
			where model=2;
	update sasuser.bottle
		set day3=3
			where model=3;
	update set1
		set  a="good";
quit;

data set1;
	set sasuser.one;
run;

proc print data=set1;
run;

proc sql;
	update sasuser.bottle
		set day4=0,day5=1
			where model=2;
quit;

proc sql;
	update sasuser.bottle
		set day5=
		case
			when model=1 then 1
			when model=2 thne 2
			else 3
		end;
quit;

proc print data=sasuser.bottle;
run;

proc sql;
	update sasuser.bottle
		set day5=4-day5;
quit;
	
proc sql;
	update sasuser.bottle
		set day5=model*2;
quit;	

proc sql;
	update sasuser.bottle
		set day3=
		case 
			when day3=1 then 2
			when day3=2 then 3
			when day3=3 then 0
		end;
	select *
		from sasuser.bottle;
quit;

proc sql;
	update sasuser.bottle
		set day3=3 
			where day3=2;
	update sasuser.bottle
		set day3=0 
			where day3=3;
	update sasuser.bottle
		set day3=1
			where day3=0;
	select *
		from sasuser.bottle;
quit;

proc sql;
	update sasuser.bottle
		set day2=
		case
			when day2=0 then 1
			when day3=1 then 2
		    else 3
		end;
quit;

proc sql;
	update sasuser.bottle
		set day2=
		case
			when day5=2 then 0
			when day3=1 then 2
		    else 3
		end;
quit;

proc sql;
	update sasuser.bottle
		set day3=
		case
			when day5=4 then 0
			when day2=2 then 2
		    else 3
		end;
	select *
		from sasuser.bottle;
quit;

proc sql;
	update sasuser.bottle
		set day3=
		case day2
			when 0 then 1
			when 2 then -1
			else 0
		end;
	select *
		from sasuser.bottle;
quit;
































