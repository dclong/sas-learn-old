
proc print data=sasuser.bottle;
run;

proc sql outobs=5;
    create table part as 	
		select * 
			from sasuser.bottle;
quit;

proc print data=part;
run;

proc sql inobs=3;
	create table set1 as 
		select *
			from sasuser.bottle;
quit;

proc print data=set1;
run;

proc sql;
	select distinct model
		from sasuser.bottle
			order by 1;
quit;

