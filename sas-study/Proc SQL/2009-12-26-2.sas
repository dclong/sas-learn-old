
proc sql;
	create table discount
		(Destination char(3),
		 BeginDate num Format=date9.,
		 EndDate num format=date9.,
		 Discount num);
	
quit;

proc sql;
describe table discount;
quit;

proc print data=discount;
run;

proc sql;
	create table nine
		like sasuser.bottle;
quit;

proc sql;
	describe table nine;
quit;

data ten;
	input x A $ length=3;
	datalines;
	1 adu
	2 Benjamin
run;

proc print data=ten;
run;


