
proc print data=sasuser.bottle;
run;

proc sql;
	title "all";
	select *
		from sasuser.bottle
		where model>1 and day1>all
		(select day1
			from sasuser.bottle
			where model=1)
		order by model,machine;
quit;

proc print data=sasuser.bottle;
run;

proc sql;
	select model,machine,day1+day2+day3 as total
		from sasuser.bottle
		where calculated total<=200
		order by 1,2;
quit;

proc sql;
	select avg(day1+day2+day3)
		from sasuser.bottle;
quit;

proc sql;
	create table set1 as 
	select model,machine,day1+day2+day3 as total
	from sasuser.bottle;
quit;

proc print data=set1;
run;

proc sql;
	select avg(total) 
		from set1;
quit;

proc sql;
	select model,machine,day1+day2+day3 as total,calculated total/2 as half
		from sasuser.bottle;
quit;

proc sql;
	select 
