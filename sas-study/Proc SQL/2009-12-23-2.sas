
filename brain url "http://www.public.iastate.edu/~pdixon/stat500/data/brain.txt";

data sasuser.brain;
	infile brain;
	input gest brain body litter species $;
run;

proc print data=sasuser.brain;
	title "Data Set brain";
run;

proc sql;
	title "concatenation";
	select * 
		from sasuser.brain
		where species="""White" || "-h"
		order by gest;
quit;

proc sql;
	title "and";
	select *
		from sasuser.brain
		where gest>=100 and gest <=200;
quit;

proc sql;
	title "between and";
	select *
		from sasuser.brain
		where gest between 100 and 200;
quit;

proc sql;
	title "contains";
	select * 
		from sasuser.brain
		where species contains "White";
quit;

proc sql;
	title "in";
	select *
		from sasuser.brain
		where species in ("""White-h","""Tree","""Slow")
		order by gest;
quit;

proc sql;
	title "missing values";
	select *
		from sasuser.brain
		where gest is missing
		order by 1;
quit;

proc sql;
	title "like";
	select *
		from sasuser.brain
		where species like "%h_"
		order by 1;
quit;

proc sql;
	title "Sound like";
	select *
		from sasuser.brain
		where species=*"slow"
		order by 1;
quit;

proc sql;
	footnote "footnote";
	select model,machine,day1+day2+day3 as total label="Total"
		from sasuser.bottle
		order by 1,2;
quit;

proc sql;
	footnote;
	select model,machine,"the sum is" as SUM,day1+day2 as sum
		from sasuser.bottle
		order by 1,2;
quit;

proc sql;
	select day1,avg(day1) as average
		from sasuser.bottle
		where day1>70;
quit;

proc sql;
	select avg(day1) 
		from sasuser.bottle;
quit;

proc sql;
	select day1+day2 as average
		from sasuser.bottle;
quit;








