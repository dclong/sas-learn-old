proc print data=sasuser.plate;
run;

data sasuser.plate2;
	set sasuser.plate;
	if time="time" then time="";
run;

proc print data=sasuser.plate2;
run;

proc sql;
	select nmiss(time) as count 
		from sasuser.plate2;
quit;

proc sql;
	select distinct time as count
		from sasuser.plate2;
quit;

proc sql;
	select time,count(subject) as count
		from sasuser.plate2
		group by 1;
quit;

proc print data=sasuser.bottle;
run;

proc sql;
	select model,machine,avg(day1)
		from sasuser.bottle
		group by model;
quit;

proc sql;
	validate
	select model,machine,day1/sum(day1) as percent
		from sasuser.bottle
		order by 1,2;
quit;

data sasuser.one;
	input x A $;
	datalines;
	1 a
	2 b
	4 d
run;

proc print data=sasuser.one;
run;

data sasuser.two;
	input x B $;
	datalines;
	2 x
	3 y
	5 v
run;

proc print data=sasuser.two;
run;

proc sql;
	select *
		from sasuser.one,sasuser.two ;
run;

data one;
	set sasuser.one;
run;

data two;
	set sasuser.two;
run;

proc sql;
	create table three as 
	select *
		from sasuser.one,sasuser.two
		where one.x=two.x;
quit;

proc sql;
	create table three as 
	select one.x as ID,two.x,a,b
		from sasuser.one, sasuser.two
		where one.x=two.x;
quit;

data sasuser.three;
	input x A $;
	datalines;
	1 a1
	1 a2
	2 b1
	2 b2
	4 d
run;

proc print data=sasuser.three;
run;

data sasuser.four;
	input x B $;
	datalines;
	2 x1
	2 x2
	3 y
	5 v
run;

proc print data=sasuser.four;
run;

proc sql;
	select *
		from sasuser.three,sasuser.four
		where three.x=four.x;
quit;

data set1;
	merge sasuser.three sasuser.four;
	if three.x=four.x;
run;

proc print data=set1;
run;
























