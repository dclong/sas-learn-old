
filename bottle url "http://www.public.iastate.edu/~pdixon/stat500/data/bottle.txt";

data sasuser.bottle;
	infile bottle;
	input model machine day1 day2 day3 day4 day5;
	if _n_ ne 1;
run;

proc print data=bottle;
	title "Data Set bottle";
run;

proc sql;
	title "SQL Procedure";
	select model,machine,day1,day2 as set2
		from bottle
		where machine>=3
		order by day1;
quit;

proc print data=set2;
	title "Data Set set2";
run;

proc sql;
	title "SQL Procedure";
	select model,machine,day1,day2,day3+day4+day5 as SUM
	from bottle
	where machine ge 3
	order by model desc, 2 desc;
quit;

data set1;
	title "Data Set set1";
	set bottle;
	keep model machine day1 day2;
run;

proc print data=set1;
run;

data set2;
	title "Data Set set2";
	set bottle;
	keep model machine day3 day4;
run;

proc print data=set2;
run;

proc sql;
	title "Combine Two Data Sets";
	select set1.model,set1.machine,day1,day2,day3,day4
		from set1,set2
		order by 1 desc,2 desc;
quit;

proc print data=bottle;
run;

proc sql;
	title "PROC SQL GROUP";
	select model,mean(day1) as avg
		from bottle
		group by model
		order by model;
quit; 

proc sql;
	title "PROC SQL, COUNT";
	select model,day1, count(day1) as count
		from bottle
		where model<3
		group by model
		order by 1 desc;
quit;



		




















