
proc sql;
	create table set1 (keep=model machine day1)
		like sasuser.bottle;
quit;

proc sql;
	describe table set1;
quit;

proc sql;
	describe table sasuser.bottle;
quit;

proc sql;
	create table set2 as 
		select model,machine,day1
			from sasuser.bottle;
quit;

proc print data=set2;
run;

proc sql;
	create table set3 as 
		select * 
			from sasuser.bottle;
quit;

proc print data=set1;
run;

proc sql;
	describe table set1;
quit;

proc sql;
	insert into set1
		set model=4,
			machine=1
		set model=4;
quit;

proc sql;
	insert into set1 (model,machine)
	values (5,.)
	values (5,3)	;
quit;

proc print data=set1;
run;

proc sql;
	insert into set1 (model,machine)
		select model,machine
			from sasuser.bottle
			where model=2;
quit;









