
proc print data=sasuser.bottle;
run;

proc sql;
	title "proc sql CSS";
	select model,css(day1) as css
		from sasuser.bottle
		where model ne 4
		group by model
		order by model desc;
quit;

proc sql;
	title "Create a Table";
	create table set1 as 
		select model,machine,day1,day2 
			from sasuser.bottle
			where mod(model,2) ne 1
			order by 1,2;
quit;

proc print data=set1;
run;

proc sql;
	select model,avg(day1)
		from sasuser.bottle
		group by model
		order by 1;
quit;

proc sql;
	title "Use of Having Clause";
	select model,day1
		from sasuser.bottle
		group by model
		having avg(day1)>60
		order by model;
quit; 

proc print data=sasuser.bottle;
run;

proc sql;
	select avg(day1) as average
		from sasuser.bottle;
quit;

proc sql;
	select model,avg(day1) as average
		from sasuser.bottle
		group by model 
		order by model;
quit;

proc sql;
	title "Subqueries";
	title2 "> average";
	select model,avg(day1) as average format=dollar8.2
		from sasuser.bottle
		group by model
		having avg(day1)>
			(select avg(day1) from sasuser.bottle)
		order by model desc;
quit;

proc sql feedback;
	title "Select All Columns";
	select * 
		from sasuser.bottle;
quit;













 
