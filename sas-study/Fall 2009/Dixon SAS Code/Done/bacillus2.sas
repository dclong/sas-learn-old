options ls=75 formdlim='-' nonumber nodate;

data bacillus;
  	infile 'bacillus2.txt' ;
  	input trt $ pre post;

  	/* reference group coding: Placebo is the ref. group */
  	if trt = 'Ab1' then 
		a1 = 1;
    else 
		a1 =  0;
  
  /* a short cut way to the same thing */  
  	a2 = (trt = 'Ab2' );
  	a3 = (trt = 'Pl' );  /* to show what happens */
  
  
  /* effects coding */
  	b1 = (trt = 'Ab1' );
  	b2 = (trt = 'Ab2' );
  	if trt = 'Pl' then do;
    	b1 = -1;
    	b2 = -1;
    end;
run;
      
proc print;
  	var trt a1-a3 b1-b2;
  	title 'Leprosy study: indicator variables';
run;
     
proc glm;
  	class trt;
  	model post = trt /solution;
  	lsmeans trt /stderr;
  	title 'GLM solution';
run;
     
proc glm;
  	model post = a1 a2;
  	estimate 'Ab1 mean' intercept 1 a1 1;
  	estimate 'Ab2 mean' intercept 1 a2 1;
  	estimate 'Pl mean' intercept 1;
  	title 'Indicator variable regression / reference group coding';
run;

proc glm;
  	model post = b1 b2;
  	estimate 'Ab1 mean' intercept 1 b1 1;
  	estimate 'Ab2 mean' intercept 1 b2 1;
  	estimate 'Pl mean' intercept 1 b1 -1 b2 -1;
  	title 'Indicator variable regression / effects group coding';
run;

proc reg;
  	model post = a1 a2 a3 /noint;
  	title 'Indicator variable regression, cell means coding';
run;
     
proc reg;
  	model post = a1 a2 a3;
  	title 'Indicator variable regression, overparameterized model';
run;
     

             
                 
     
     
