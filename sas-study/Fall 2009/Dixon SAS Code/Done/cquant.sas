/* determine quantile of Chi-square distribution */
/*    given a probability and d.f. */

data cquant;
  	input p df;
  	c = cinv(p,df);
	cards;
	0.975 10
	0.975 20
run;

proc print;
  title 'Quantiles from Chi-square distribution';
run;
     
