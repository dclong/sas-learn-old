/* the following options line saves paper - */
/*     SAS puts information together to print fewer pages */
options formdlim = '-';

/* read the sparrow data set */
/*   the second column is not numeric. 
/*   The $ tells SAS to read the PRECEDING variable as a character variable */

data sparrow;
 	infile 'sparrow.txt';
 	input humerus fate $;
	if humerus ne .;             /* omit the first observation (header line) */
    lhumerus =  log(humerus);    /* log() is the natural log */
run;

/* Do the t-test using a more general procedure that provides residuals */
/* The model statement has the Y (response) variable on the left */
/*   the model is on the right.  For a t-test, the only component of the */
/*    is the variable that defines the groups */
/* PROC GLM assumes equal variances */

/* The output command creates a new data sets containing all the original */
/*   variables and the ones requested in the output command. */
/*   r = defines a variable to store the residuals */
/*   p = defines a variable to store the predicted values */

proc glm;
  	class fate;
  	model humerus = fate;
  	output out = resids r = resid p = pred;
  	title 'T test using PROC GLM';
run;
     
/* Diagnostics are based on the residuals */
/* You can plot residuals against the predicted values */
/*   to find outliers or unequal variance */  
proc plot;
  	plot resid*pred;
  	title 'Residual vs. predicted value plot';
run;
       
/* you can construct a normal probability plot in univariate */
proc univariate plot normal;
  	var resid;
  	qqplot / normal;
  	title 'Normal probability plot of residuals';
run;
          

/* you can test equal variances by constructing abs(resid) then */
/*  doing a t-test */
data resid2;
  	set resids;
  	absresid =  abs(resid);
run;

/* Levene's test is a T test on abs(resid) */
proc ttest;
  	class fate;
  	var absresid;
  	title 'T test on abs(resid), i.e. Levenes test';
run;
               
/* Wilcoxon  rank sum test */
/*  default is to use asymptotic normal distribution */
/*  good for n > 10 in each group */
 
proc npar1way wilcoxon;
  	class fate;
  	var humerus;
/*  exact wilcoxon;    /* uncomment to get exact p value */
                       /*  requires excessive cpu time when n > 20 */ 
                       /*  exact wilxocon /mc;  uses monte carlo approx */
  	exact wilcoxon /mc;
    title 'Wilcoxon rank sum test';
run;

/* randomization test using observed values as the scores */     

proc npar1way scores = data;
  	class fate;
  	var humerus;
  	exact scores = data /mc;    /* default is 10000 randomizations */
  	title 'Randomization test on observed values'; 
run;
            
















