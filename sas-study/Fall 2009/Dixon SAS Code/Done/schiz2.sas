/* plots for block data */
/*  using schizophrenia data as example */

data schiz2;
  	set schiz;
	trt =  'Unaff';
  	y = unaff;
  	output;
	trt = 'Aff';
  	y =  aff;
  	output;
  	keep pair trt y;
run;

/* simple plot of treatment values in each block */
/*   requires 'reorganized data' */
/*  label each point by the treatment code */

proc plot;
  	plot y*pair =  trt;
  	title 'Responses in each block';
run;
/* a better plot is to sort the blocks by the block mean */
/*   which makes it easier to spot multiplicative effects */

proc sort;
  	by pair;
run;
  
/* calculate the block means and store in a new data set */
proc means noprint;
  	by pair;
  	output out = means mean = blockmean;
run;
  
/* them merge those block means with the reorganized data */
/*  to add a new column (bmean) to the reorg data */
/*  requires that both are sorted by pair */
data schiz3;
  	merge schiz2 means;
  	by pair;
run;
  
/* print out the data set to see what the merge data */  
proc print data = schiz3;
run;
   
proc plot;
  plot y*blockmean =  trt;
  title 'Responses in each block, sorted by block mean';
run;
     
