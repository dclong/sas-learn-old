options noovp;      
/* enables overlay plots on vincent.  not necessary on pc's */
     
data births;
  	infile 'birth.txt';
  	input year denmark netherland canada usa;
  	y =  netherland;
  	x =  year - 1949;
  	year2 = year*year; 
  	xlx =  x*log(x);
run;

proc reg;
  	model y =  year year2;
run;
              
/* fit four loess smooths, with 4 diff amounts of smoothing */   
proc loess;
  	model y =  year /smooth = (0.3 0.6 1 2) dfmethod =exact ;  
  	ods output outputstatistics = fit;
  	title 'Loess nonparametric regression';
run;
  
proc print data = fit (obs = 10);
run;

/* and plot the values for each smoothing parameter */       
proc plot;
  	by smoothingparameter;
  	plot DepVar*year pred*year = '*' /overlay;
run;

/* collect information for Breusch-Pagan test */
/*   to test whether variance increasing/decreasing over time */

proc reg data = births;
  	model netherland =  year;
  	output out = resids r = resid p = yhat;
  	title 'Simple linear regression';
run;
  
proc plot;
  	plot resid*yhat;
run;

data resid2;
  	set resids;
  	esquare =  (45/0.00006542)*resid**2;
run;
  
/* Note: need to replace 45 by n, 0.00006542 by SSE */
  
/* regress squared residual on year, compare SSmodel /2 to Chi-sq */
proc reg;
  	model esquare =  year;
  	title 'Information for Breusch-Pagan test';
run;
                 
/* how to cut a continuous X variable into groups */
data births2;
  	set births;
  /* create a new data set, reading obs from births */
  	yeargroup =  floor((year - 1949) / 5);
  
  /* floor() rounds down to smaller integer */
  /*  so year =  1950 - > floor (1/5) =  0, */
  /*     year =  1951 - > floor (2/5) =  0  */
  /*  and so on until year =  1954 - > floor(5/5) =  1 */
  
  /* this creates groups of every five years.  
      Change 5 to get other sizes */
      
  /* or use a sequence of if then ; else; statements */
  
  	if year  < 1954 then 
		yeargroup =  0;
  	else 
		if year < 1959 then 
			yeargroup = 1;
  		else 
			if year  < 1964 then 
				yeargroup = 2;
  /* repeat to define all but the last group, then use */
  			else yeargroup =  9;
run;
  
  
         
        

