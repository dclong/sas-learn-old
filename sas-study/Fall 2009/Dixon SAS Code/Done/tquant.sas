/* determine quantile of t distribution given a probability and d.f. */

data tquant;
  	input p df;
  	t = tinv(p,df);
  	cards;
0.975 10
0.975 20
run;

proc print;
  	title 'Quantiles from t distribution';
run;
     
