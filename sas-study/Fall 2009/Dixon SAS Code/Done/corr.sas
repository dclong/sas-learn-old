data meat;
/* same as in meat.sas (following) */

proc corr;
 	var ph time logtime;
 	title 'Estimate and test correlation';
run;

/* can also get CI for correlation using Fisher Z transform */
/*  in version 9.1 and later */
/* turn off bias adjustment to recreate lecture approach */
/* recommend omit (i.e. adjust for bias) in general */
/*   help on proc corr / syntax / proc corr stmt  has details */

proc corr fisher (biasadj=no);
  	var ph time logtime;
  	title 'CI for correlation coefficients';
run;

/* and can use fisher Z transform to test */
/*  Ho: rho = non-zero constant */

proc corr fisher (rho0 = 0.6 biasadj=no); 
  	var ph time logtime;
  	title 'Fisher test of rho = 0.6';
run;

/* the Spearman correlation is the non-parametric correlation */
/*   The observations of X are converted to ranks */
/*   THe observations of Y are converted to ranks */
/*   The spearman correlation is the usual corr. between the ranks
*/
/*   It is resistant to outliers. */
/*   useful when anticipate monotonic nonlinear relationship */

proc corr spearman;
  	var ph time logtime;
  	title 'estimate and test spearman correlation';
run;
