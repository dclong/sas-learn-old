/* illustrations of various transformations in SAS */

data transform;
  	input y p;
  
/* the square root function is sqrt()
  sy =  sqrt(y);
  
/* the arcsin square root transformation of a proportion is */
  	ap =  asin(sqrt(p));
  
/* to raise a number to a power uses **  */
/*  e.g. y squared is: */
   	sqy =  y**2;
  

  
     
