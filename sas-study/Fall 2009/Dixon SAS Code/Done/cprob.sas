/* determine probability of less than specified value */
/*  for a Chi-square distribution with the given d.f. */
/* this is the inverse computation to that done by cquant.sas */

data cprob;
  	input c df;
  	p = probchi(c,df);
	cards;
	2.00 10
	2.00 20
	38.2 20
run;

proc print;
  	title 'Probabilities of observed < C from a Chi-square distribution';
run;
   
