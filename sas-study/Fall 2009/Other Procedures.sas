
proc reg aic sbc cp mse;
  	model lweight = lage llength nsex lchest lheadlen lheadwid lneck
    					/ selection = cp best = 5 b;
  	model lweight = lage llength nsex lchest lheadlen lheadwid lneck
    			/ selection = stepwise slstay = 0.20 slentry = 0.20;
run;

proc reg;
   	model logbrain = logbody loglitter loggest;
	test1: test loglitter = 0;
	test2: test loglitter = 0, loggest = 0;
	test3: test loglitter+loggest = 1;
   	title 'Regression of log brain size on log body and log litter';
   	title2' With test statements'; 
run;

proc reg;
  	model y =  x /dwprob;
  	title 'regression with Durbin-Watson statistic';
run;
proc autoreg;
  	model y =  x /dw = 1 dwprob;
run;
/* fit four loess smooths, with 4 diff amounts of smoothing */   
proc loess;
  	model y =  year /smooth = (0.3 0.6 1 2) dfmethod =exact ;  
  	ods output outputstatistics = fit;
  	title 'Loess nonparametric regression';
run;

proc print data = fit (obs = 10);
run;

proc plot;
  	by smoothingparameter;
  	plot DepVar*year pred*year = '*' /overlay;
run;
proc plot;
  plot y*blockmean =  trt;
  title 'Responses in each block, sorted by block mean';
run;

proc means noprint;
  	by code;
  	var percent;
  	output out = means mean = mean std = sd;
run;
proc means mean std stderr t prt clm;
  	var diff;
  	title 'Paired data: t-test and 95% confidence interval';
run;
proc glm;
   	model logbrain = logbody loglitter loggest;
   	contrast 'test1' loglitter 1;
   	contrast 'test2' loglitter 1, loggest 1;
   	estimate 'test3' loglitter 1 loggest 1;
	title 'Regression of log brain size on log body, log litter and log gest';
   	output out = resids r = resid p = yhat; 
run;

proc plot;
  plot bresid*gresid yhat*gresid = '*' /overlay;
run;

proc reg;
  	model logbrain = loggest logbody loglitter /vif influence ;
/* the vif option gives you the vif for each obs */
/* the influence option gives you h, dffits and dfbeta for each obs */    
  	output out = resids r = resid rstudent = rs 
                      p = yhat cookd = cookd dffits = dffit h = h;
                      
/* various diagnostics can be stored in the output dataset */
/*   the syntax for all is keyword =  variable name */
/*   rstudent =  gives you externally studentized residual */
/*   cookd =  gives you cooks distance */
/*   dffits = gives you dffits */
/*   h =  gives you Hii */
     
/* there are many other pieces of info that can be stored.  See the proc reg */
/*   documentation for all the details */                      
run;

proc plot;
   	plot rs*yhat = '*' $ i;
   	plot rs*resid;
   	plot rs*i =  '*';
   	plot cookd*i = '*' ;
   	plot dffit*i = '*' ;
   	plot h*i = '*' ;
run;
proc plot;
  	where ph = .;
  	plot yhat*logtime = '*' uciobs*logtime = 'u' lciobs*logtime = 'l' /overlay;
run;
proc corr fisher (biasadj=no);
  	var ph time logtime;
  	title 'CI for correlation coefficients';
run;

proc corr fisher (rho0 = 0.6 biasadj=no); 
  	var ph time logtime;
  	title 'Fisher test of rho = 0.6';
run;

proc corr spearman;
  	var ph time logtime;
  	title 'estimate and test spearman correlation';
run;
proc glm;
  	where code ne 1;
    class code;
    model percent = code;
    title 'Without judge 1';
run;
proc glm;
  	class amount type;
  	model gain = amount type amount*type;
     /* could also write amount | type */
    lsmeans amount amount*type /stderr;
  	estimate 'beef-pork' type 1 0 -1;
  	estimate 'high-low' amount 1 -1;
  	estimate 'high-low in beef' amount 1 -1 amount*type 1 0 0 -1 0 0;
  	estimate 'beef-pork in low' type 1 0 -1 amount*type 0 0 0 1 0 -1;
  	estimate 'high-low in beef vs high-low in pork' amount*type 1 0 -1 -1 0 1;
  	contrast 'av of beef,pork - cereal' type 1 -2 1;
  	contrast 'beef-pork' type 1 0 -1;
  	title '2 way factorial ANOVA';
run;
proc glm;
  	class both;
  	model gain = both;
  	lsmeans both /stderr;
  	contrast 'amount' both 1 1 1 -1 -1 -1;
  	contrast 'type'  both 1 -1  0 1 -1  0,
                     both 1  0 -1 1  0 -1;
  	contrast 'amount*type' both 1 -1  0 -1 1 0,
                           both 1  0 -1 -1 0 1;
 	estimate 'high-low in beef' both 1 0 0 -1 0 0;
    title '1-way anova approach to factorial ANOVA';
run;
proc glm;
  	class amount type;
  	model gain = amount type amount*type;
  	lsmeans amount*type / slice = amount;
  	title 'Slicing by amount';
run;
proc glm;
  	by amount;
  	class type;
  	model gain = type;
  	title 'Separate analyses by amount'; 
run;
proc glm;
  	class code;
  	model percent = code;
/* to get confidence intervals for estimates and model param */
/*   add /clparm to model statement, e.g.: */
/*  model percent =  code /clparm;   */


/*  The following request more information: */
/*   lsmeans command provides  means, s.e., and 95% ci for each group */

  	lsmeans code /stderr cl;
 
/*  output residuals and plot diagnostics to do a residual plot */

  	output out=resids p = yhat r = resid;

/* estimate and contrast statements for a-priori questions: */

  	estimate 'spock - rest ' code 6 -1 -1 -1 -1 -1 -1 /divisor = 6;
  	estimate 'BAD: spock - rest' code 6 -1 -1 -1 -1 -1 -1;
  
  	contrast 'spock - rest' code 6 -1 -1 -1 -1 -1 -1;
  	contrast 'among rest'   code 0  1 -1  0  0  0  0,
                            code 0  0  1 -1  0  0  0,
                            code 0  0  0  1 -1  0  0,
                            code 0  0  0  0  1 -1  0,
                            code 0  0  0  0  0  1 -1;

  	contrast 'among rest, 2' code 0 5 -1 -1 -1 -1 -1,
                             code 0 0  4 -1 -1 -1 -1,
                             code 0 0  0  3 -1 -1 -1,
                             code 0 0  0  0  2 -1 -1,
                             code 0 0  0  0  0  1 -1;
                           
/* multiple comparisons adjustments for post-hoc comparisons */
/*   adding cl to this (or any subsequent gives you simul. conf. int */
/*   for differences.  conf. int. for individ. means are NOT adjusted */

   	lsmeans code /stderr pdiff adjust = tukey cl;      
     /* tukey's mcp */
  
/* the following multiple comparisons adjustments are commented out */
/*   each produces output similar to the above command, */
/*   however, the indicated multiple comparisons procedure will be used */

/*   lsmeans code /stderr pdiff adjust = t;      
     /* No m.c.p., i.e. Fisher's lsd */
 
/*   lsmeans code /stderr pdiff adjust = bonferroni;      
     /* bonferroni mcp */

/*   lsmeans code /stderr pdiff adjust = scheffe;       
     /* scheffe mcp */
run;

proc glm data = meat;
  	model ph = logtime;
  	estimate 'ph at 5 hr' intercept 1 logtime 1.6094;
  	estimate 'ph at 2 hr' intercept 1 logtime 0.6931;
  	title 'regression on observed data';
run;

proc glm data = all;
  	model ph=logtime;
  	output out=set1 
    residual=r               /* residuals */
    predicted=yhat           /* predicted values */
    stdp = sepred            /* s.e. of predicted mean (the line) */
    stdi = stobs             /* s.e. on predicted observation */
    lclm = lcipred           /* lower 95% c.i. limit for pred. mean */
    uclm = ucipred           /* upper 95% c.i. limit for pred. mean */
    lcl = lciobs             /* lower 95% c.i. limit for pred. obs */
    ucl = uciobs             /* upper 95% c.i. limit for pred. obs */
    ;                        /* and here, finally, is the end of the output */
  	title 'regression on all data';
run;



proc import datafile = "sparrow.xls" out = sparrow;
run;



