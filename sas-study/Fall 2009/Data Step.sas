data bacillus;
  	infile 'bacillus2.txt' ;
  	input trt $ pre post;

  	/* reference group coding: Placebo is the ref. group */
  	if trt = 'Ab1' then 
		a1 = 1;
    else 
		a1 =  0;
  
  /* a short cut way to the same thing */  
  	a2 = (trt = 'Ab2' );
  	a3 = (trt = 'Pl' );  /* to show what happens */
  
  
  /* effects coding */
  	b1 = (trt = 'Ab1' );
  	b2 = (trt = 'Ab2' );
  	if trt = 'Pl' then do;
    	b1 = -1;
    	b2 = -1;
    end;
run;
data sparrow;
 	infile 'sparrow.txt' firstobs = 2;
 	input humerus fate $;
run;
data births2;
  	set births;
  /* create a new data set, reading obs from births */
  	yeargroup =  floor((year - 1949) / 5);
  
  /* floor() rounds down to smaller integer */
  /*  so year =  1950 - > floor (1/5) =  0, */
  /*     year =  1951 - > floor (2/5) =  0  */
  /*  and so on until year =  1954 - > floor(5/5) =  1 */
  
  /* this creates groups of every five years.  
      Change 5 to get other sizes */
      
  /* or use a sequence of if then ; else; statements */
  
  	if year  < 1954 then 
		yeargroup =  0;
  	else 
		if year < 1959 then 
			yeargroup = 1;
  		else 
			if year  < 1964 then 
				yeargroup = 2;
  /* repeat to define all but the last group, then use */
  			else yeargroup =  9;
run;

data brain;
  	infile 'brain.txt' firstobs = 2;   
  	input gest brain body litter species $;
  	loggest = log(gest);
  	logbrain = log(brain);
  	logbody = log(body);
  	loglitter = log(litter);
  	i =  _n_;    
run;
data rats;
  	infile 'ratweight.txt';
  	input amount $ type $ @;
/* create a variable encoding both amount and type */
/*  || is the string concatenate operator */
/*  SAS stores character variables as fixed length objects */
/*   8 characters is the default, */
/*   shorter strings are padded with trailing spaces, */
/*   trim() removes those trailing spaces   */
	both = trim(amount) || '/' || type;
  	do i =  1 to 10;
    	input gain @;
    	output;
    end;
run;

/* an alternative way to read these data */
/* without knowing there are 10 obs per treatment */
/*  this approach works with unequal numbers per treatment */

/* the missover option is crucial.  By default, if SAS is asked */
/*  to read beyond the end of a line, it goes to the next line */
/*  missover tells SAS to return a missing value  instead */

data rat2;
  	infile 'ratweight.txt' missover;
  	input amount $ type $ @ ;
  	both = trim(amount) || '/' || type;
 	input gain @;
  	do until (gain =  .);
    	output;
    	input gain @;
    end;
run;

/* the order of input and output are reversed from the first data step */
/*  this is because we read an observation before entering the loop */
/*  we stay in the loop so long as we have a valid value */
/* we leave the loop as soon as we get a missing value (end of line) */

/* Here's one way to store a SAS data set as a text file */
/* the if statement adds a header before the first data line */

data _null_;
  	file 'ratweight2.txt';
    set rats;
  	if _n_ =  1 then put 'amount type gain';
  	put amount type gain;
run;

/* proc export; is another way to output all sorts of formats */
/*  including .csv, .xls, and many database formats */


data rats;
  	infile 'ratweight.txt';
  	input amount $ type $ @;
  	both = trim(amount) || '/' || type;
    do i =  1 to 10;
    	input gain @;
    	output;
   	end;
run;

data wool;
  	infile 'wool.txt' missover;
  	input bale @;
  	do i =  1 to 4;
    	input clean @;
    	if clean ne . then output;
    end;
run; 

data wool;
   	infile 'wool.txt' missover;
	input bale @;
  	if bale < 3 then 
		farm = 'SE';
    else 
		if bale < 6 then 
			farm = 'SC';
    	else 
			farm = 'C';
		*endif;
	*endif;
  	do i =  1 to 4;
    	input clean @;
    	if clean ne . then output;
    end;
run;
