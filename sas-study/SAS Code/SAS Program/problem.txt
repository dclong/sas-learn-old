DATA STUDY;
INPUT A B @@;
CARDS;
2 3
;
A=A+B;
PROC PRINT;
RUN;


data allscore;
infile 'c:\research\try1.txt';
        /*read the data from the file*/
input scores @@;
cards;
run;

/* get useful data from dataset allscore*/
proc print data=allscore;

data temp1 temp2;
   retain flag 0;
   set allscore(firstobs=2);
   if scores=. then flag=flag+1;
   if flag=0 then output temp1;
   else output temp2;
run;

proc univariate data=temp1 normal;
var scores;
run;

/*renew the dataset*/
if varlen(temp2,score) then do;
data allscore;
   set temp2;
   keep scores;
run;
go to line;
end;
run;
