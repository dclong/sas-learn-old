
proc ttest;
  	class trt;
  	var y;
  	title 'Unpaired t-test';
run;

proc ttest;
  	class fate;
  	var humerus lhumerus;
  	title 'T-test of humerus and log transformed length';
run;

proc ttest data=set1;
	class trt; 
	format trt trt.;
run;

proc ttest data=owls;
   	class type;
   	var km91 km118 km140 km160 km177 km241 km338;
run;

proc multtest data=set1 permutation nsample=10000 
					seed=36607 pvals outsamp=pmt; 
	class trt;  
	contrast "intr vs extr" 1 -1;
    test mean(y);  
run; 

proc npar1way wilcoxon;
  	class fate;
  	var humerus;
/*  exact wilcoxon;    /* uncomment to get exact p value */
                       /*  requires excessive cpu time when n > 20 */ 
                       /*  exact wilxocon /mc;  uses monte carlo approx */
  	exact wilcoxon /mc;
    title 'Wilcoxon rank sum test';
run;

proc npar1way scores = data;
  	class fate;
  	var humerus;
  	exact scores = data /mc;    /* default is 10000 randomizations */
  	title 'Randomization test on observed values'; 
run;

proc npar1way data=set1 wilcoxon;
	class oil; 
	var y;
	exact wilcoxon / mc n=10000;
run;

proc npar1way data=set1 wilcoxon;
	class agent;
  	var time; 
run;

proc npar1way data=owls;
   	class type;
   	var km91 km118 km140 km160 km177 km241 km338;
run;
