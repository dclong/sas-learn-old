
proc logistic data=set1;
  	model RESULT = AGE SEX E1-E3 EMOTION ETREAT L1-L4 ATREAT ALCADD
        	HEALTH FINANCE M1-M3 PDRINK SIBS WORK WAGES JOBS DAGE
        		DFREQ STOP DRY DRUGS/ sle=.25 sls=.25 selection=s details
                   				maxiter=50 converge=.0001 covb itprint;
run;


proc logistic data=set1 nosimple covout outest=setp1;
  	model result =  ETREAT ATREAT E3 WORK/ itprint covb corrb ctable pprob=.4
                   maxiter=50 selection=none converge=.0001;
run;

/*  Use backward elimination to select a good subset of variables */

proc logistic data=set1;
  	model RESULT = AGE EMOTION ETREAT L1-L4 ATREAT
        	HEALTH  M1-M3 PDRINK SIBS WORK WAGES DAGE
        		DFREQ STOP DRUGS/  sls=.05 selection=b details
                   	maxiter=50 converge=.0001 covb itprint;
run;

proc logistic data=set1 nosimple covout outest=setp1;
  	model RESULT = AGE EMOTION ETREAT L2 ATREAT HEALTH PDRINK WAGES
                 DFREQ  /  covb corrb ctable pprob=.5
                         		maxiter=50 selection=none converge=.0001;
run;

proc logistic data=set1 nosimple covout outest=setp1;
  	model RESULT = E3 ETREAT ATREAT HEALTH WORK
                /  covb corrb ctable pprob=.5
                   		maxiter=50 selection=none converge=.0001;
run;

proc logistic data=set1 nosimple covout outest=setp1;
  	model RESULT = AGE ATREAT/  covb corrb ctable pprob=.5
                   maxiter=50 selection=none converge=.0001;
run;
