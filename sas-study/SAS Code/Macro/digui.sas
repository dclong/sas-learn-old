
%let n=1;
%macro finddefine(bg_code, s_brc_var, s_brc_code, e_brc_var ,e_brc_code);
 %put &n;
 %let n=%eval(&n+1);
 %let sqlobs=0;
 %if &n=10 %then %let sqlobs=1;
 %if ^&sqlobs %then %finddefine(&bg_code, &s_brc_var ,&s_brc_code ,&e_brc_var ,&e_brc_code);
%mend  finddefine;
%finddefine(bg_code, s_brc_var, s_brc_code, e_brc_var ,e_brc_code);


%let parent_brc=%eval(%sysfunc(substr(&s_brc_var,10,1))+1);
%macro create_label(); 
%let a="&debug_field"; 
%let b="&debug_cfield"; 
%let i=1; 
%let c=%sysfunc(scan(&a,&i," ")); 
%let c1=%sysfunc(scan(&b,&i,",")); 
label 
%do %while("&c"^=""); 
&c=" &c1" 
%let i=%eval(&i+1); 
%let c=%sysfunc(scan(&a,&i," ")); 
%let c1=%sysfunc(scan(&b,&i,",")); 
%end; 
; 
%mend ;

