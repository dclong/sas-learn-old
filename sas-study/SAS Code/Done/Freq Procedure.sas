
proc freq data=x2;
  	tables work*job / chisq;
  	weight y;
run;

PROC FREQ DATA=SET3;
  	TABLES COL2*COL1 / NOPERCENT NOCOL;
RUN;

proc freq data=set2 order=data;
  	tables parents*health / chisq;
  	weight y;
run;

proc freq data=set2;
  	tables work*attribute / chisq;
  	weight y;
run;


proc freq data=set1; 
	by run;
    table sex*gear / chisq cmh nopercent nocol expected;
    format sex sex. gear gear. run run.;
run;

proc freq data=set1;
	table response*year / chisq expected cellchi2;
  	weight count;
  	format response rowfmt.
           year colfmt.;
run;

proc freq data=set2;
	table response*year / chisq noprint;
  	weight count;
run;

PROC FREQ DATA=SET2;
  	TABLES ROW*COL / EXACT CHISQ;
    WEIGHT X;
RUN;

proc freq data=set1;
   	table type / testp = (.0625 .75 .1875);
   	weight y;
run;

/*Very strange!*/
proc freq data=set1;
  	tables batch*process*yield / cmh2 scores=rank noprint;
run;



proc freq data=set1; 
	by gear run;
  	table sex / binomial (p=.5);
run;

proc freq data=bottle;
	tables model/all;
run;
