
proc transpose data=VLoading out=VLoading2;
run;

proc transpose data=PLoading out=PLoading2;
     var x1-x6;
run;

proc transpose data=bottle out=set2 prefix=var;
	var day1-day5;
run;

proc transpose data=bottle out=tran name=wh prefix=var;
	id id;
run;

proc transpose data=bottle out=tran (drop=wh _label_) name=wh prefix=var;
	id id;
run;

proc transpose data=parms out=effects prefix=value name=effect;
  	var a b c d e ab ac ad ae bc bd be
            cd ce de abc abd abe acd ace ade
            bcd bce bde cde abcd abce abde 
            acde bcde abcde;
run;
