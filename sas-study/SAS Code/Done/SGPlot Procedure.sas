
ods html path="C:\Documents and Settings\dclong\My Documents\SAS Study\SAS Code" (url=none) body="gout1.htm";

proc sgplot data=brain;
	where brain ne .;
	histogram brain;
run;

ods html close;

ods printer file="C:\Documents and Settings\dclong\My Documents\SAS Study\SAS Code\gout1.ps" ps;

proc sgplot data=brain;
	where brain ne .;
	histogram brain;
run;

ods printer close;

ods printer file="C:\Documents and Settings\dclong\My Documents\SAS Study\SAS Code\gout1.pdf" pdf;

proc sgplot data=brain;
	where brain ne .;
	histogram brain;
run;

ods printer close;


proc sgplot data=brain;
	where brain ne .;
	histogram brain;
run;

proc sgplot data=brain;
	where brain ne .;
	histogram brain;
	density brain;
run;

proc sgplot data=bottle;
	vbar day1/group=machine;
run;

proc sgplot data=bottle;
	vbar model;
run;

proc sgplot data=bottle;
	vbar day1/group=model;
run;

proc sgplot data=bottle;
	vbar model/response=day1;
run;

proc sql;
	select model,sum(day1) as SUM1
		from bottle
		group by model
		order by model;
quit;

data set1;
	array a{5} 1-5;
run;

data dist;
	do x=0 to 1 by 0.001;
		pdf12=pdf("beta",x,1,2);
		pdf23=pdf("beta",x,2,3);
		pdf34=pdf("beta",x,3,4);
		pdf29=pdf("beta",x,2,9);
		output;
	end;
run;

proc print data=dist(obs=100);
run;

proc sgplot data=dist;
	series x=x y=pdf12;
	series x=x y=pdf23;
	series x=x y=pdf34;
	series x=x y=pdf29;
run;

proc sgplot data=dist;
	series x=x y=pdf12;
	series x=x y=pdf23;
	series x=x y=pdf34;
	series x=x y=pdf29;
	*xaxis type= grid ;
	yaxis label="Density"  values=(0 to 5 by 1);
run;

proc sgplot data=dist;
	series x=x y=pdf12/legendlabel="Beta(1,2)" markers lineattrs=(thickness=4);
	series x=x y=pdf23/legendlabel="Beta(2,3)" markers lineattrs=(thickness=2);
	series x=x y=pdf34/legendlabel="Beta(3,4)" markers lineattrs=(thickness=1);
	series x=x y=pdf29/legendlabel="Beta(2,9)" markers lineattrs=(thickness=0.5);
	*xaxis type= grid ;
	yaxis label="Density"  values=(0 to 5 by 1);
run;

proc sgplot data=dist;
	series x=x y=pdf12;
	series x=x y=pdf23;
	series x=x y=pdf34;
	series x=x y=pdf29;
	*xaxis type= grid ;
	refline 1 2 3 /transparency=0.5 lable=("Mean1" "Mean2" "Mean2");
	yaxis label="Density"  values=(0 to 5 by 1);
run;

proc sgplot data=dist;
	series x=x y=pdf12;
	series x=x y=pdf23;
	series x=x y=pdf34;
	series x=x y=pdf29;
	*xaxis type= grid ;
	yaxis label="Density"  values=(0 to 5 by 1);
	inset "Practice Plotting"/position=topright border;
run;

proc sgplot data=dist;
	series x=x y=pdf12;
	series x=x y=pdf23;
	series x=x y=pdf34;
	series x=x y=pdf29;
	*xaxis type= grid ;
	inset "Practice Plotting"/position=topright border;
	yaxis label="Density"  values=(0 to 5 by 1);
run;

proc sgplot data=dist;
	series x=x y=pdf12;
	series x=x y=pdf23;
	series x=x y=pdf34;
	series x=x y=pdf29;
	*xaxis type= grid ;
	inset "Practice Plotting"/position=topright border;
	yaxis label="Density"  values=(0 to 5 by 1);
	title "Some Plotting Practice";
run;

proc sgplot data=bottle;
	series x=model y=day1;
run;

proc sgpanel data=bottle;
	panelby model;
	series x=machine y=day1;
run;

proc sgpanel data=bottle;
	panelby model;
	reg x=machine y=day1;
run;

proc sgplot data=brain;
	scatter x=brain y=body;
run;

proc sgplot data=brain;
	scatter x=brain y=body;
	ellipse x=brain y=body;
run;

proc sgplot data=brain;
	vbox brain;
run;

proc sgplot data=sashelp.prdsale;
  	yaxis label="Sales";
  	vbar country / response=predict;
  	vbar country / response=actual barwidth=0.5 transparency=0.2;
run;

proc sgplot data=bottle;
	scatter x=machine y=day1/group=model;
run;

proc sgplot data=sashelp.class;
  	reg x=height y=weight / CLM CLI;
run;


proc sgplot data=sashelp.classfit;
  title "Fit and Confidence Band from Precomputed Data";
  band x=height lower=lower upper=upper /
       legendlabel="95% CLI" name="band1"; 
  
 
    band x=height lower=lowermean upper=uppermean / 
       fillattrs=GraphConfidence2
       legendlabel="95% CLM" name="band2";
  scatter x=height y=weight;
  series x=height y=predict / lineattrs=GraphPrediction
         legendlabel="Predicted Fit" name="series";
 
  
 
    keylegend "series" "band1" "band2" / location=inside position=bottomright;
run;
 
proc sgplot data=sashelp.class(where=(age<16));
  dot age / response=height stat=mean
            limitstat=stddev numstd=1;
run;

proc sgplot data=sashelp.heart;
  title "Cholesterol Distribution";
  histogram cholesterol;
 
    density cholesterol;
  density cholesterol / type=kernel;
 
    keylegend / location=inside position=topright;
run;
 
proc sgplot data=sashelp.heart;
  title "Cholesterol Distribution by Weight Class";
  hbox cholesterol / category=weight_status;
run;

proc sgplot data=sashelp.stocks (where=(date >= "01jan2000"d
                                 and date <= "01jan2001"d
                                 and stock = "IBM"));
   title "Stock Volume vs. Close";
   vbar date / response=volume;
   vline date / response=close y2axis;
run;









