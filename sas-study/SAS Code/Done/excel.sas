/* various ways to read files from excel */

/* 1) store the file as .csv (comma delimited) */
/*    read using a data step  dsd option on infile crucial */
/*    may also need lrecl=XX  if many fields (replace XX by 512 or 1024) */
data datacsv;
  	infile 'c:/philip/stat500/data/bacillus2.csv' dsd;
  	input trt $ pre post;
run;

/* 2) store the file as .csv (comma delimited) */
/*    read using proc import */
proc import file='c:/philip/stat500/data/bacillus2.csv' out=fromcsv;
run;

/* 3) store the file as .xls (Excel workbook) */
/*    read using proc import */
proc import file='c:/philip/stat500/data/bacillus2.xls' out=fromxls;
run;

options formdlim='-';

proc print data=datacsv (obs=10);
  	title 'First 10 obs of csv/data';
run;

proc print data=fromcsv (obs=10);
  	title 'First 10 obs of csv/import';
run;

proc print data=fromxls (obs=10);
  	title 'First 10 obs of xls/import';
run;

proc import datafile = "sparrow.xls" out = sparrow;
run;

/*it seems that we cannot use file short for datafile name*/

proc import datafile='C:\Documents and Settings\dclong\My Documents\SAS Study\Data\book1.xls' 
				out=fromxls dbms=xls table=sheet2;
run;

libname dblib excel path="C:\Documents and Settings\dclong\My Documents\SAS Study\Data\book1.xls";

proc access dbms=xls;
	create work.book1.access;
	path="C:\Documents and Settings\dclong\My Documents\SAS Study\Data\book1.xls";
	list all;
	getnames=yes;
run;

proc access dbms=xls accdesc=book1;
	create work.book1.view;
	select all;
run;

