
PROC CORR DATA=set1;
     VAR X1-X4;
RUN;

proc corr data=scores;
     var prin1 prin2 prin3 x1-x10 age;
run;

proc corr data=set1 Fisher(biasadj=no); 
	var x y; 
run;

proc corr fisher (biasadj=no);
  	var ph time logtime;
  	title 'CI for correlation coefficients';
run;

proc corr fisher (rho0 = 0.6 biasadj=no); 
  	var ph time logtime;
  	title 'Fisher test of rho = 0.6';
run;

proc corr spearman;
  	var ph time logtime;
  	title 'estimate and test spearman correlation';
run;

proc corr data=sasuser.bottle fisher (rho0 = 0.6 biasadj=no); 
  	var day1-day4;
  	title 'Fisher test of rho = 0.6';
run;

