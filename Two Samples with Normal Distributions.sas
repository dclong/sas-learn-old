*******************Chuanlong Du*******************************************;
/*
Functions:
This program deals with two samples with normal distribution.
1. Test whether the covariance matrices are homogeneous
2. Do the two-sample Hotelling Tsquare (F) test using pooled covariance matrix
3. Do the two-sample Hotelling Tsquare (F) test using approximation
4. Print information about the confidence ellipse
5. Construct simutaneous confidence intervals using Tsquare method
6. Construct simutaneous confidence intervals using Bonferroni method 
Parameters:
Parameter X1 is the observation matrix for population 1
Parameter X2 is the observation matrix for population 2
Parameter DMu0 is used for the test H0: Mu1-Mu2=DMu0
Parameter alpha is the significant level
Parameter C is used for the test H0: C*(Mu1-Mu2)=d
Parameter d is used for the test H0: C*(Mu1-Mu2)=d
Parameter ConMat is used to construct the simutaneous confidence intervals.
For any a which is a row vector of ConMat, simutaneous C.I. for a(Mu1-Mu2) is constructed. 
Waring: 
when you use this program, you should be aware of that the confidence 
ellipse is for C*(Mu1-Mu2) while the simutaneous confidence intervals are for ConMat*(Mu1-Mu2). 
They are different!!! But When C is an identical matrix, ConMat*(Mu1-Mu2) is base on 
the confidence ellipse of Mu1-Mu2. 
*/
proc iml;
**********************Parameter Part I***************************************;
     use grain1;
	 read all into X1;
	 print X1;
	 use grain2;
	 read all into X2;
	 print X2;
	 DMu0={500 50 30};
	 alpha=0.05;
	 C={};
	 d=C*DMu0`;
	 ConMat={};
******************************************************************************;
**********************Parameter Part II***************************************;
	 /*If X is given, there's no need to intial n, p and S. 
	 However, if X is not given, these parameters should be intialed*/
	 n1=nrow(x1);*size of sample 1;
	 n2=nrow(x2);*size of sample 2;
	 *calculate basic statistics for sample 1;
	 sum1=X1[+,];
	 Xbar1=sum1/n1;
	 Xcent1=X1-repeat(Xbar1,n1,1);
	 S1=Xcent1`*Xcent1/(n1-1);
	 *calculate basic statistics for sample 2;
	 sum2=x2[+,];
	 Xbar2=sum2/n2;
	 Xcent2=X2-repeat(Xbar2,n2,1);
	 S2=Xcent2`*Xcent2/(n2-1);
/*
     n1=25;
     Xbar1={14 24 22};
	 S1={30 10 14,10 15 4,14 4 37};
	 n2=17;
	 Xbar2={15 20 24};
	 S2={26 12 10,12 17 8,10 8 43};
*/
*****************Body Part of Function*****************************************;
	 p=nrow(C);
	 n=n1+n2-1;
	 title f=arial c=red h=6 "Two Samples with Normal Distribution";
*--------Test whether the two covariance matrices are homogeneous---------------;
	 M=(n-1)*log(det(Spool))-(n1-1)*log(det(S1))-(n2-1)*log(det(S2));
	 df=1/2*(p+1)*p;
	 Cinverse=1-(2*p**2+3*p-1)/6/(p+1)*(1/(n1-1)+1/(n2-1)-1/(n-1));
	 ChiStat=M*Cinverse;
	 Pvalue=1-probchi(Chistat,df);
	 print "The information of the approximate Chisqaure test for homogeneous variance is:";
	 print M Cinverse ChiStat df Pvalue;
*--------use pooled covariance matrix to do the Hotelling Tsquare (F) test------;
	          *for the null hypthesis H0: C*(Mu1-Mu2)=d;
	 Spool=((n1-1)*S1+(n2-1)*S2)/(n-1);
	 Snew=C*Spool*C`;
	 HTsquare=n1*n2/(n1+n2)*(C*(Xbar1-Xbar2)`-d)`*inv(Snew)*(C*(Xbar1-Xbar2)`-d);
	 df1=p;
	 df2=n-p;
	 HotellingF=HTsquare*df2/p/(n-1);
	 Pvalue=1-probf(HotellingF,df1,df2);
	 print "The information of Hotelling Tsquare (F) test For H0: C*(Mu1-Mu2)is:";
	 print HTsquare HotellingF df1 df2 Pvalue;
*----------------------------------------------------------------------------*;
	 *calculate length and directions of the axes of the confidence ellipse;
	 Svalues=j(1,p);*a vector used to store the eigen values;
	 Svectors=j(p,p);*a matrix used to store the eigen vectors;
	 call eigen(Svalues,Svectors,Snew);
	 constant=sqrt(p*(n-1)/(n-p)*finv(1-alpha,p,n-p)*(1/n1+1/n2));
	 AxisLen=sqrt(Svalues)*constant;
	 print "The length of the axes of the ellipse and their directions are:";
	 print AxisLen Svectors;
*-------------------------------------------------------------------------------------------------------------------;
	 *construct the confidence region for the mean vector;
	 print "The confidence region (ellipse) for the C*(Mu1-Mu2) is:";
	 print "n1*n2/(n1+n2)*(C*(Mu1-Mu2)-C*(Xbar1-Xbar2))'(C*Spool*C`)^(-1)(C*(Mu1-Mu2)-C*(Xbar1-Xbar2))<=" HTsquare;
*-------------------------------------------------------------------------------------------------------------------;
     p=ncol(X);
     constant=sqrt(p*(n-1)/(n-p)*finv(1-alpha,p,n-p)*(1/n1+1/n2));
	 *construct simutaneous confidence intervals using Tsquare method;
	     *for a*(mu1-mu2) where a can be any row of ConMat;
	 start Tconf(a);
	 ADxbar=a*(Xbar1`-Xbar2`);
	 Width=constant*sqrt(a*Snew*a`);
	 UpperLimit=ADxbar+Width;
	 LowerLimit=ADxbar-width;
	 print "The Tsquare confidence interval for a'mu is:";
	 print ADxbar LowerLimit UpperLimit;
	 finish;
	 *use the above function to construct Tsquare simutaneous intervals;
	 m=nrow(ConMat);
	 do i=1 to m;
	    run Tconf(ConMat[i,]);
	 end;
*-------------------------------------------------------------------------------------;
	 *construct simutaneous confidence intervals using Bonferroni method;
	     *for a*(mu1-mu2) where a can be any row of ConMat;
	 start BonConf(a);
	 ADxbar=a*(Xbar1`-Xbar2`);
	 Width=tinv(1-alpha/m/2,n-1)*sqrt(a*Spool*a`*(1/n1+1/n2));
	 UpperLimit=ADxbar+Width;
	 LowerLimit=ADxbar-width;
	 print "The Bonferroni confidence interval is:";
	 print ADxbar LowerLimit UpperLimit;
	 finish;
	 *use the above function to construct Bonferroni simutaneous intervals;
	 do i=1 to m;
	    run BonConf(ConMat[i,]);
	 end;
*=====================================================================================;
	 *calculate some basic statistics;
	 p=nrow(C);
	 NewXbar1=C*Xbar1`;
	 NewXbar2=C*Xbar2`;
	 NewS1=C*S1*C`;
	 NewS2=C*S2*C`;
*-------------------------------------------------------------------------------------;
*----------Use Chisquare Approximation to do the test H0: C*(Mu1-Mu2)=d---------------;
	 Snew=NewS1/n1+NewS2/n2;
	 ChiStat=(NewXbar1-NewXbar2-d)`*inv(Snew)*(NewXbar1-NewXbar2-d);
	 df=p;
	 Pvalue=1-probchi(chistat,df);
	 print "The information of approximate Chisquare test for H0: C*(Mu1-Mu2)=d is:";
	 print ChiStat df Pvalue;
*------------------------------------------------------------------------------------;
	 *calculate length and directions of the axes of the confidence ellipse;
	 Svalues=j(1,p);*a vector used to store the eigen values;
	 Svectors=j(p,p);*a matrix used to store the eigen vectors;
	 call eigen(Svalues,Svectors,Snew);
	 constant=sqrt(cinv(1-alpha,p));
	 AxisLen=sqrt(Svalues)*constant;
	 print "The length of the axes of the ellipse and their directions are:";
	 print AxisLen Svectors;
*-------------------------------------------------------------------------------------------------------------------;
	 *construct the confidence region for the mean vector;
	 print "The confidence region (ellipse) for the C*(Mu1-Mu2) is:";
	 print "(C*(Mu1-Mu2)-C*(Xbar1-Xbar2))'(C*(S1/n1+S2/n2)*C`)^(-1)*(C*(Mu1-Mu2)-C*(Xbar1-Xbar2))<=" ChiStat;
*-------------------------------------------------------------------------------------------------------------------;
     p=ncol(X);
	 constant=sqrt(cinv(1-alpha,p));
	 *construct simutaneous confidence intervals using approximate Chisquare test;
	 start ChisqConf(a);
	 ADxbar=a*(Xbar1`-Xbar2`);
	 Width=constant*sqrt(a*Snew*a`);
	 UpperLimit=ADxbar+Width;
	 LowerLimit=ADxbar-width;
	 print "The Chisquare approximate confidence interval is:";
	 print ADxbar LowerLimit UpperLimit;
	 finish;
	 *use the above function to construct Bonferroni simutaneous intervals;
	 do i=1 to m;
	    run ChisqConf(ConMat[i,]);
	 end;
*=================================================================================;
*---------------Use Approximate Hotelling Tsquare (F) approximation to do--------; 
         *---------------------the test H0: C*(Mu1-Mu2)=d------------------------;
	 p=nrow(C);
     SaveInv=inv(Snew);
     HTsquare=(NewXbar1-NewXbar2-d)`*SaveInv*(NewXbar1-NewXbar2-d);
	 *calculate the degrees of freedom for the denominator of F statistic;
	 Mat1=NewS1*SaveInv/n1;
	 Mat2=NewS2*SaveInv/n2;
	 Divide1=trace(Mat1**2)+(trace(Mat1))**2;
	 divide1=divide1/n1;
	 divide2=trace(mat2**2)+(trace(mat2))**2;
	 divide2=divide2/n2;
	 divide=divide1+divide2;
	 nu=(p+p**2)/divide;
	 df1=p;
	 df2=nu-p+1;
	 HotellingF=HTsquare*df2/p/nu;
	 Pvalue=1-probf(HotellingF,df1,df2);
	 print "The information of approximate Hotelling Tsqaure (F) test is:";
	 print HTsquare HotellingF df1 df2 pvalue;
*-----------------------------------------------------------------------------------;
	 *calculate length and directions of the axes of the confidence ellipse;
	 Svalues=j(1,p);*a vector used to store the eigen values;
	 Svectors=j(p,p);*a matrix used to store the eigen vectors;
	 call eigen(Svalues,Svectors,Snew);
	 constant=sqrt(p*nu/(nu+1-p)*finv(1-alpha,df1,df2));
	 AxisLen=sqrt(Svalues)*constant;
	 print "The length of the axes of the ellipse and their directions are:";
	 print AxisLen Svectors;
*-------------------------------------------------------------------------------------------------------------------;
	 *construct the confidence region for the mean vector;
	 print "The confidence region (ellipse) for the C*(Mu1-Mu2) is:";
	 print "(C*(Mu1-Mu2)-C*(Xbar1-Xbar2))'(C*(S1/n1+S2/n2)*C`)^(-1)*(C*(Mu1-Mu2)-C*(Xbar1-Xbar2))<=" HTsquare;
*-------------------------------------------------------------------------------------------------------------------;
     p=ncol(X);
	 constant=sqrt(p*nu/(nu+1-p)*finv(1-alpha,df1,df2));
	 Snew=S1/n1+S2/n2;
	 *construct simutaneous confidence intervals using approximate Hotelling Tsquare (F) test;
	 start ATconf(a);
	 ADxbar=a*(Xbar1`-Xbar2`);
	 Width=constant*sqrt(a*Snew*a`);
	 UpperLimit=ADxbar+Width;
	 LowerLimit=ADxbar-width;
	 print "The Chisquare approximate confidence interval is:";
	 print ADxbar LowerLimit UpperLimit;
	 finish;
	 *use the above function to construct Bonferroni simutaneous intervals;
	 do i=1 to m;
	    run ATConf(ConMat[i,]);
	 end;
	 *construct simutaneous confidence intervals using approximate Bonferroni method;
	 start ABonConf(a);
	 *calculate the approximate degrees of freedom;
	 NewS1=a*S1*a`/n1;
	 NewS2=a*S2*a`/n2;;
     Snew=NewS1+NewS2;
	 Mat1=NewS1/Snew;
	 Mat2=NewS2/Snew;
	 Divide1=Mat1**2/n1;
	 divide2=mat2**2/n2;
	 nu=1/(divide1+divide2);*the degrees of freedom;
	 *claculate the limits of the interval;
	 ADxbar=a*(Xbar1`-Xbar2`);
	 Width=tinv(1-alpha/m/2,nu)*Snew;
	 UpperLimit=ADxbar+Width;
	 LowerLimit=ADxbar-width;
	 print "The Chisquare approximate confidence interval is:";
	 print nu ADxbar LowerLimit UpperLimit;
	 finish;
	 *use the above function to construct Bonferroni simutaneous intervals;
	 do i=1 to m;
	    run ABonConf(ConMat[i,]);
	 end;
quit;

/*
