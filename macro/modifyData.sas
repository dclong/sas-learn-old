data allscore;
     retain flag 0;
     infile "c:\research\try1.txt";
     input score;
     if score=. then
           flag=flag+1;
     else
           output;
run;

proc print data=allscore;
run;

option mprint;

%macro depart;
    %local i;
    data 
       %do i=1 %to 3;
          ID&i
       %end;
    ;
       set allscore;
       select(flag);
          %do i=1 %to 3;
              when(&i) output ID&i;
          %end;
       end;
    run;
%mend depart;

%depart;

%macro dis;
    %local i;
    %do i=1 %to 3;
        proc print data=ID&i;
        run;
    %end;
%mend dis;

%dis;

%macro modify;
    %local big;
    %local i;
    proc sql;
    %do i=1 %to 3;
        select max(score) into :big from ID&i;
        %if &big>500 %then
             update ID&i
                set score=score+500 where score=0;
        ;
    %end;
    run;
%mend modify;

%modify;

%dis;














