data allscore;
    retain flag 0;
    infile "c:\research\score_copy.txt";
    input score @@;
    if score=. then
       flag=flag+1;
    else
       output allscore;
run;

%macro getdata;
    %local i;
    data
         %do i=1 %to 9207;
            ID&i 
         %end;
    ;
         set allscore;
         select(flag);
            %do i=1 %to 9207;
                when(&i) output ID&i;
            %end;
            otherwise;
         end;
    run;
%mend getdata;

%getdata;      

ods listing file="c:\research\test.txt";

%macro write;
    %local i;
    %do i=1 %to 9207;
       ods listing select "tests for normality";
       proc univariate data=ID&i normal;
       var score;
       run;
    %end;
%mend write;

%write;


%macro getdata;
    %local i;
    %local flag;
    %let flag=0;
    data
         %do i=0 %to 3;
            ID&i 
         %end;
    ;
         infile "c:\research\try1.txt";
         input score @@;
         if _ERROR_=1 then
             %let flag=%eval(&flag+1);
         else
             output ID&flag;
%mend getdata;
%getdata;
run;

*ods listing "c:\research\test1.txt";

%macro write;
    %local i;
    %do i=1 %to 3;
       proc print data=Id&i;
       run;
       ods listing select "tests for normality";
       proc univariate data=ID&i normal;
       var score;
       run;
    %end;
%mend write;
%write;
