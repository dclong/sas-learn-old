****************************Chuanlong Du*********************************;
/*
Functions:
This program is used to calculate the conditional normal distribution
Declaration:
Parameter mu is the mean vector of the joint distribution
Parameter Sigma is the covariance matrix of the joint distribution
Parameter var is the subscript of the variables whose conditional distribution to be calcualted
Parameter cond is the subscript of the give part and their corresponding values
*/
proc iml;
*********************Body of the Function***************************************;
     start CondDist(mu,Sigma,var,cond);
	 mu1=mu[var];
	 Sigma11=Sigma[var,var];
	 mu2=mu[cond[1,]];
	 Sigma22=Sigma[cond[1,],cond[1,]];
	 Sigma12=Sigma[var,cond[1,]];
	 Sigma21=Sigma12`;
	 InvSigma22=inv(Sigma22);
	 CondMu=mu1+sigma12*InvSigma22*(cond[2,]`-mu2);
	 CondSigma=sigma11-sigma12*InvSigma22*Sigma21;
	 title f=arial c=red h=6 "Conditional Distribution";
	 print CondMu,CondSigma;
	 finish;
*************************Intial the parameters*********************************;
	 mu={2 -3 0 1};
	 sigma={1 1 1 0,1 3 2 1,1 2 4 2,0 1 2 4};
	 x1={1 3};
	 x2={2 4,-2 1.5};
**************************Run the Function*************************************;
	 run CondDist(mu,sigma,x1,x2);
quit;
